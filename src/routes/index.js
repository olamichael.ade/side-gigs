// import libs
import React from 'react';
import { Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import createBrowserHistory from 'history/createBrowserHistory';

// import components
import routes from './routes';
import PrivateRoute from './Private';
import PublicRoute from './Public';

// import base layout container
import { App } from 'containers';

const history = createBrowserHistory();

const Routes = () => (
  <ConnectedRouter history={history}>
    <App>
      <Switch>
        {routes.map((route, i) => {
          if (route.auth) {
            return <PrivateRoute key={i} {...route} />;
          }
          return <PublicRoute key={i} {...route} />;
        })}
      </Switch>
    </App>
  </ConnectedRouter>
);

export default Routes;
