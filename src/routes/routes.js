// import modular routes
import authRoutes from '../modules/auth/routes';
import leagueRoutes from '../modules/leagues/routes';
import walletRoutes from '../modules/wallet/routes';
import h2hRoutes from '../modules/h2h/routes';
import profileRoutes from '../modules/profile/routes';
import notificationRoutes from '../modules/notification/routes';
import homeRoutes from '../modules/home/routes';
import privacyPolicyRoutes from '../modules/privacy/routes';
import fplIdRoute from '../modules/myFplId/routes';
import contactRoute from '../modules/contact/routes';

export default [
  ...authRoutes,
  ...leagueRoutes,
  ...walletRoutes,
  ...h2hRoutes,
  ...profileRoutes,
  ...notificationRoutes,
  ...homeRoutes,
  ...privacyPolicyRoutes,
  ...fplIdRoute,
  ...contactRoute
];
