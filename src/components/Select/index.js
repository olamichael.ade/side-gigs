import React from 'react';
import { FormComponent } from 'HOCS';
import { Select as WrapperSelect } from 'antd';

function Select(props) {
  return <WrapperSelect {...props} />;
}

const mapInputToProps = input => ({
  ...input
  // onBlur: (proxy, event) => {
  //   // proxy.persist();
  //   if (proxy.target.value !== '') {
  //     input.onBlur(proxy, event);
  //   }
  // }
});

Select.form = FormComponent(Select, mapInputToProps);
Select.Option = WrapperSelect.Option;

export default Select;
