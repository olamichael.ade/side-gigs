import React from 'react';
import { FormComponent } from 'HOCS';
import moment from 'moment';
import { DatePicker as WrapperDatePicker } from 'antd';

const dateFormat = 'DD/MM/YYYY';
const today = moment();

const renderDatePicker = ({
  input,
  placeholder,
  defaultValue,
  meta: { touched, error }
}) => (
  <div>
    <WrapperDatePicker
      {...input}
      dateForm="MM/DD/YYYY"
      selected={input.value ? moment(input.value) : null}
    />
    {touched && error && <span>{error}</span>}
  </div>
);

export default renderDatePicker;
