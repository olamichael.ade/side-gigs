import React from 'react';

const AppError = () => (
  <div className="error-screen">
    <div className="error-container">
      <h1 className="error-title">{`Sorry, something went wrong.`}</h1>
      <p>{`We're working on it and we'll get it fixed as soon as we can.`}</p>
    </div>
  </div>
);

export default AppError;
