import React from 'react';
import { Button, Text } from 'components';
import { FACEBOOK_PROVIDER } from 'utils/constants';

const SocialAuth = props => {
  const { onClick, label } = props;

  const handleFacebookAuth = () => {
    const FB = window.FB;

    return FB.login(
      response => {
        console.log(response);
        const { authResponse } = response;
        if (authResponse) {
          const { accessToken } = authResponse;
          onClick({ access_token: accessToken, provider: FACEBOOK_PROVIDER });
        } else {
          console.log('User cancelled login or did not fully authorize.');
        }
      },
      { scope: 'email' }
    );
  };

  return (
    <Button className="auth-social-button" onClick={handleFacebookAuth}>
      <Text color="white">{label}</Text>
    </Button>
  );
};

export default SocialAuth;
