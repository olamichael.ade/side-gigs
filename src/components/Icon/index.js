import React from 'react';
import PropTypes from 'prop-types';
import { Icon as AntIcon } from 'antd';
import './index.scss';

// import svg icons or png images
import {
  OK,
  Money,
  Handshake,
  Badge,
  CreateGameIcon,
  CreateIcon,
  JoinIcon,
  FundIcon
} from './icons';

// Define Icon types (font awesome icon) classes
const ICON_CLOSE = 'close';
const ICON_PLUS = 'plus';
const ICON_CARET_UP = 'caret-up';
const ICON_CARET_DOWN = 'caret-down';
const ICON_SETTINGS = 'setting';
const ICON_DELETE = 'delete';
const ICON_VIEW = 'profile';
const ICON_DOWN_CIRCLE = 'down-circle';
const ICON_LOCK = 'lock';
const ICON_UNLOCK = 'unlock';
const ICON_FIRE = '<Icon type="fire" theme="outlined" />';

// Define icon types
const OK_ICON = 'OK_ICON';
const MONEY_ICON = 'MONEY_ICON';
const HANDSHAKE_ICON = 'HANDSHAKE_ICON';
const BADGE_ICON = 'BADGE_ICON';
const CREATE_ICON = 'CREATE_ICON';
const CREATE_GAME_ICON = 'CREATE_GAME_ICON';
const FUND_ICON = 'FUND_ICON';
const JOIN_ICON = 'JOIN_ICON';

function Icon(props) {
  const {
    weight,
    size,
    iconClass,
    color,
    iconType,
    onClick,
    title,
    theme
  } = props;

  let iconTypeClass;
  let weightClass;
  let colorClass;
  let isExternalIcon = false;
  let pngIcon = false;

  let sizeClass;
  switch (size) {
    case 'small':
      sizeClass = 'sizeSmall';
      break;
    case 'medium':
      sizeClass = 'sizeMedium';
      break;
    case 'medium1':
      sizeClass = 'sizeMedium1';
      break;
    case 'small4':
      sizeClass = 'sizeSmall4';
      break;
    case 'med2':
      sizeClass = 'sizeMed2';
      break;
    case 'large1':
      sizeClass = 'sizeLarge1';
      break;
    case 'large2':
      sizeClass = 'sizeLarge2';
      break;
    case 'extraLarge':
      sizeClass = 'sizeExtraLarge';
      break;
    default:
      sizeClass = ''; // 'defaultSize
  }

  switch (color) {
    case 'orange':
      colorClass = 'orange';
      break;
    case 'green':
      colorClass = 'green';
      break;
    case 'black':
      colorClass = 'black';
      break;
    case 'blue':
      colorClass = 'blue';
      break;
    case 'red':
      colorClass = 'red';
      break;
    case 'grey':
      colorClass = 'grey';
      break;
    case 'white':
      colorClass = 'white';
      break;
    case 'disabled':
      colorClass = 'disbaledColor';
      break;
    default:
      colorClass = colorClass || 'colorDefault';
  }

  switch (weight) {
    case 'med2':
      weightClass = 'icon-weight-med2';
      break;
    default:
      weightClass = '';
  }

  switch (iconType) {
    case ICON_CLOSE:
      iconTypeClass = ICON_CLOSE;
      isExternalIcon = true;
      break;
    case ICON_PLUS:
      iconTypeClass = ICON_PLUS;
      isExternalIcon = true;
      break;
    case ICON_CARET_UP:
      iconTypeClass = ICON_CARET_UP;
      isExternalIcon = true;
      break;
    case ICON_CARET_DOWN:
      iconTypeClass = ICON_CARET_DOWN;
      isExternalIcon = true;
      break;
    case ICON_DELETE:
      iconTypeClass = ICON_DELETE;
      isExternalIcon = true;
      break;
    case ICON_SETTINGS:
      iconTypeClass = ICON_SETTINGS;
      isExternalIcon = true;
      break;
    case ICON_VIEW:
      iconTypeClass = ICON_VIEW;
      isExternalIcon = true;
      break;
    case ICON_DOWN_CIRCLE:
      iconTypeClass = ICON_DOWN_CIRCLE;
      isExternalIcon = true;
      break;
    case OK_ICON:
      iconTypeClass = OK;
      pngIcon = true;
      break;
    case MONEY_ICON:
      iconTypeClass = Money;
      pngIcon = true;
      break;
    case HANDSHAKE_ICON:
      iconTypeClass = Handshake;
      pngIcon = true;
      break;
    case BADGE_ICON:
      iconTypeClass = Badge;
      pngIcon = true;
      break;
    case CREATE_ICON:
      iconTypeClass = CreateIcon;
      pngIcon = true;
      break;
    case CREATE_GAME_ICON:
      iconTypeClass = CreateGameIcon;
      pngIcon = true;
      break;
    case FUND_ICON:
      iconTypeClass = FundIcon;
      pngIcon = true;
      break;
    case JOIN_ICON:
      iconTypeClass = JoinIcon;
      pngIcon = true;
      break;
    case ICON_LOCK:
      iconTypeClass = ICON_LOCK;
      isExternalIcon = true;
      break;
    case ICON_UNLOCK:
      iconTypeClass = ICON_UNLOCK;
      isExternalIcon = true;
      break;
    case ICON_FIRE:
      iconTypeClass = ICON_FIRE;
      isExternalIcon = true;
      break;
    default:
      sizeClass = '';
  }

  let finaliconTypeClass = `${weightClass} ${sizeClass} ${colorClass}`;

  if (!isExternalIcon) {
    finaliconTypeClass = `${iconTypeClass}  ${finaliconTypeClass}`;
  }

  if (iconClass) {
    finaliconTypeClass = `${iconClass} ${finaliconTypeClass}`;
  }

  if (pngIcon) {
    const IconComp = iconTypeClass;
    return (
      <IconComp
        className={`${sizeClass} ${iconClass}`}
        color={color}
        onClick={onClick}
      />
    );
  }

  if (isExternalIcon) {
    return (
      <AntIcon
        type={iconTypeClass}
        className={finaliconTypeClass}
        onClick={onClick}
        theme={theme}
      />
    );
  }
  return <i className={finaliconTypeClass} title={title} alt={title} />;
}

Icon.ICON_CLOSE = ICON_CLOSE;
Icon.OK_ICON = OK_ICON;
Icon.ICON_PLUS = ICON_PLUS;
Icon.ICON_CARET_UP = ICON_CARET_UP;
Icon.ICON_CARET_DOWN = ICON_CARET_DOWN;
Icon.ICON_SETTINGS = ICON_SETTINGS;
Icon.ICON_DELETE = ICON_DELETE;
Icon.ICON_VIEW = ICON_VIEW;
Icon.ICON_DOWN_CIRCLE = ICON_DOWN_CIRCLE;
Icon.MONEY_ICON = MONEY_ICON;
Icon.HANDSHAKE_ICON = HANDSHAKE_ICON;
Icon.BADGE_ICON = BADGE_ICON;
Icon.CREATE_ICON = CREATE_ICON;
Icon.FUND_ICON = FUND_ICON;
Icon.CREATE_GAME_ICON = CREATE_GAME_ICON;
Icon.JOIN_ICON = JOIN_ICON;
Icon.ICON_LOCK = ICON_LOCK;
Icon.ICON_UNLOCK = ICON_UNLOCK;

Icon.propTypes = {
  weight: PropTypes.string,
  size: PropTypes.string,
  classIcon: PropTypes.string,
  iconType: PropTypes.string,
  color: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func,
  theme: PropTypes.string
};

export default Icon;
