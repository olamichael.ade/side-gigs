import React from 'react';
import PropTypes from 'prop-types';
export function CreateGameIcon(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786199/ffl/create-game.png)`,
        backgroundSize: 'cover'
      }}
    />
  );
}
CreateGameIcon.propTypes = {
  className: PropTypes.string
};
