import React from 'react';
import PropTypes from 'prop-types';

export function Money(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786204/ffl/ffl-money.png)`,
        backgroundSize: 'cover'
      }}
    />
  );
}

Money.propTypes = {
  className: PropTypes.string
};
