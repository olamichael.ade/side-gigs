import React from 'react';
import PropTypes from 'prop-types';

export function Logo(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(/images/logo.png)`,
        backgroundRepeat: 'no-repeat'
      }}
    />
  );
}

Logo.propTypes = {
  className: PropTypes.string
};
