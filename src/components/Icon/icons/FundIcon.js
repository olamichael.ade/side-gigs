import React from 'react';
import PropTypes from 'prop-types';
export function FundIcon(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786205/ffl/fund-wallet.png)`,
        backgroundSize: 'cover'
      }}
    />
  );
}
FundIcon.propTypes = {
  className: PropTypes.string
};
