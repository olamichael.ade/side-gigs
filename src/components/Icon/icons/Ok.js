import React from 'react';
import PropTypes from 'prop-types';

export function OK(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786205/ffl/ok.png)`,
        backgroundSize: 'cover'
      }}
    />
  );
}

OK.propTypes = {
  className: PropTypes.string
};
