import React from 'react';
import PropTypes from 'prop-types';

export function Handshake(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786203/ffl/ffl-one-on-one.png)`,
        backgroundSize: 'cover'
      }}
    />
  );
}

Handshake.propTypes = {
  className: PropTypes.string
};
