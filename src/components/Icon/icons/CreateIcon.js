import React from 'react';
import PropTypes from 'prop-types';
export function CreateIcon(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786197/ffl/create-account.png)`,
        backgroundSize: 'cover'
      }}
    />
  );
}
CreateIcon.propTypes = {
  className: PropTypes.string
};
