import React from 'react';
import PropTypes from 'prop-types';

export function Badge(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786197/ffl/ffl-badge.png)`,
        backgroundSize: '90px 100px',
        backgroundRepeat: 'no-repeat'
      }}
    />
  );
}

Badge.propTypes = {
  className: PropTypes.string
};
