import React from 'react';
import PropTypes from 'prop-types';
export function JoinIcon(props) {
  return (
    <div
      className={props.className}
      style={{
        background: `url(https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786205/ffl/play-game.png)`,
        backgroundSize: 'cover'
      }}
    />
  );
}
JoinIcon.propTypes = {
  className: PropTypes.string
};
