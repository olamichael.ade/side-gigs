import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import './Topbar.scss';

const Topbar = props => {
  return <div className="topbar-container">{props.children}</div>;
};

Topbar.propTypes = {
  children: PropTypes.node
};

export default withRouter(Topbar);
