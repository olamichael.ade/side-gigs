import React from 'react';
import { Button as AntButton } from 'antd';
import './Button.scss';

const Button = props => {
  let { color, weight, size, className, ...rest } = props;

  let btnColor;
  switch (color) {
    case 'purple':
      btnColor = 'btn-color-purple';
      break;
    case 'black':
      btnColor = 'btn-color-black';
      break;
    case 'red':
      btnColor = 'btn-color-red';
      break;
    default:
      btnColor = '';
  }

  let style = {
    width: '170px',
    borderRadius: '20px',
    padding: size === 'large' ? '10px' : '',
    height: '40px'
  };

  className = `${className} ${btnColor}`;

  return <AntButton className={className} {...rest} style={style} />;
};

export default Button;
