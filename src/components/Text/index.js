import React from 'react';
import PropTypes from 'prop-types';
import './Text.scss';

const Text = props => {
  const { classText, size, weight, children, color } = props;

  let textSize;
  switch (size) {
    case 'xlarge':
      textSize = 'text-xlarge';
      break;
    case 'small2':
      textSize = 'text-small2';
      break;
    case 'large':
      textSize = 'text-large';
      break;
    case 'large2':
      textSize = 'text-large2';
      break;
    case 'large3':
      textSize = 'text-large3';
      break;
    case 'med2':
      textSize = 'text-med2';
      break;
    case 'large30':
      textSize = 'text-30';
      break;
    case 'large32':
      textSize = 'text-32';
      break;
    case 'med24':
      textSize = 'text-24';
      break;
    default:
      textSize = 'text-normal';
  }

  let textWeight;
  switch (weight) {
    case 'light':
      textWeight = 'text-weight-light';
      break;
    case 'med':
      textWeight = 'text-weight-med';
      break;
    case 'med2':
      textWeight = 'text-weight-med2';
      break;
    case 'light2':
      textWeight = 'text-weight-light2';
      break;
    default:
      textWeight = 'text-weight-normal';
  }

  let textColor;
  switch (color) {
    case 'white':
      textColor = 'text-color-white';
      break;
    case 'black':
      textColor = 'text-color-black ';
      break;
    case 'black1':
      textColor = 'text-color-black-1';
      break;
    case 'green':
      textColor = 'text-color-green';
      break;
    default:
      textColor = '';
  }

  let className = `${textSize} ${textColor} ${textWeight}`;

  if (classText) {
    className = `${className} ${classText}`;
  }

  return <span className={className}>{children}</span>;
};

Text.propTypes = {
  classText: PropTypes.string,
  size: PropTypes.string,
  weight: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.number,
    PropTypes.array
  ])
};

export default Text;
