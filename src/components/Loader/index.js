import React from 'react';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import './index.scss';

const displayName = 'CommonLoader';

const propTypes = {
  error: PropTypes.object,
  pastDelay: PropTypes.bool
};

const LoadingComponent = ({ error, pastDelay }) => {
  // Handle the loading state
  if (pastDelay) {
    return (
      <div className="loader-container">
        <Spin />
      </div>
    );
  } else if (error) {
    // Handle the error state
    return (
      <div className="loader-container">
        Sorry, there was a problem loading the page.
      </div>
    );
  } else {
    return null;
  }
};

LoadingComponent.displayName = displayName;
LoadingComponent.propTypes = propTypes;

export default LoadingComponent;
