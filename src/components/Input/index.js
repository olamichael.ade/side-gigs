import React from 'react';
import { FormComponent } from 'HOCS';
import { Input as WrappedInput } from 'antd';

function Input(props) {
  return <WrappedInput {...props} />;
}

const mapInputToProps = input => ({
  ...input,
  onBlur: (proxy, event) => {
    proxy.persist();
    if (proxy.target.value !== '') {
      input.onBlur(proxy, event);
    }
  }
});

Input.form = FormComponent(Input, mapInputToProps);

export default Input;
