import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const displayName = 'Navbar';

const propTypes = {
  dark: PropTypes.bool
};

const Navbar = ({ dark }) => {
  return (
    <div className="logo-container">
      <div className={dark ? 'logo' : 'white-logo'} />
    </div>
  );
};

Navbar.displayName = displayName;
Navbar.propTypes = propTypes;

export default Navbar;
