import React from 'react';
import ReactDOM from 'react-dom';
// import Loadable from 'react-loadable';
import { Provider } from 'react-redux';
import store from './store';
import Routes from './routes';
import { unregister as unregisterServiceWorker } from './registerServiceWorker';

// For now bloody CRA doesnt support import outside the src folder
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/index.scss';

import { authCheck } from './modules/auth/store/actions';

store.dispatch(authCheck());

const AppBundle = (
  <Provider store={store}>
    <Routes />
  </Provider>
);

// window.onload = () => {
//   Loadable.preloadReady().then(() => {
//     ReactDOM.hydrate(AppBundle, document.getElementById('root'));
//   });
// };

ReactDOM.render(AppBundle, document.getElementById('root'));

unregisterServiceWorker();
