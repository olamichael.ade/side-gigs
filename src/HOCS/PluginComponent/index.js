import React, { Component } from 'react';
import { FB_APPID } from 'utils/constants';

export function PluginComponent(WrappedComponent) {
  return class SocialPlugin extends Component {
    componentDidMount() {
      window.fbAsyncInit = function() {
        window.FB.init({
          appId: FB_APPID,
          autoLogAppEvents: true,
          xfbml: true,
          version: 'v2.11'
        });
      };

      (function(d, s, id) {
        var js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');

      const script = document.createElement('script');

      script.src = 'https://apis.google.com/js/client:platform.js?onload=start';
      script.async = true;
      script.defer = true;

      document.body.appendChild(script);

      window.start = function() {
        window.gapi.load('auth2', function() {});
      };
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}
