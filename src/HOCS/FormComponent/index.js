import React from 'react';
import omit from 'lodash/omit';
import './FormComponent.scss';

/**
 *
 * @param {*} WrappedComponent Can be Input, Select etc etc
 * @param {*} mapInputToProps Object of props we want to pass to input
 * Here we make configurations for how a form component should look like
 */
export const FormComponent = (WrappedComponent, mapInputToProps) => {
  const FormClass = props => {
    const {
      input,
      formLayout = {},
      loading = false,
      hintText,
      onWarn,
      border = true,
      meta
    } = props;

    const { touched, error, invalid, warning } = meta || {};

    const {
      marginBottom = 14,
      marginTop = 0,
      marginLeft = 0,
      marginRight = 0
    } = formLayout;

    let classNameInput = 'input';
    let classNameMsg = 'msg-hint';
    let msg = hintText;

    if (invalid && (touched || error.forceShowError) && !loading) {
      if (border) {
        classNameInput += ` input-error`;
      }

      classNameMsg = 'msg-error';
      msg = error.value || error;
    }

    if (warning && touched) {
      if (onWarn) {
        onWarn(warning);
      }
    }

    const mappedInput = mapInputToProps ? mapInputToProps(input) : input;
    const componentProps = omit({ ...mappedInput, ...props }, [
      'meta',
      'input',
      'formLayout'
    ]);
    const style = {
      margin: `${marginTop}px ${marginRight}px ${marginBottom}px ${marginLeft}px`
    };

    return (
      <div className="form-component-wrapper" style={style}>
        <WrappedComponent
          {...componentProps}
          className={`${props.className}  ${classNameInput}`}
        />
        <div className={classNameMsg}>{msg}</div>
      </div>
    );
  };

  return FormClass;
};
