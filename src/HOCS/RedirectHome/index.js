import React, { Component } from 'react';

export function RedirectHome(WrappedComponent) {
  return class Redirect extends Component {
    componentWillMount() {
      this.redirectToDashboard(this.props);
    }

    componentWillReceiveProps(nextProps) {
      this.redirectToDashboard(nextProps);
    }

    gotoRoute = path => {
      return this.props.history.push(path);
    };

    redirectToDashboard(props) {
      const { isAuthenticated } = props;
      if (isAuthenticated) this.props.history.push('/leagues/hot-leagues');
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}
