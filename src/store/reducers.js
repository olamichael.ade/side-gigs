import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

// import module reducers
import authReducer from 'modules/auth/store/reducer';
import leaguesReducer from 'modules/leagues/store/reducer';
import walletReducer from 'modules/wallet/store/reducer';
import h2hReducer from 'modules/h2h/store/reducer';
import profileReducer from 'modules/profile/store/reducer';
import notificationReducer from 'modules/notification/store/reducer';
import contactReducer from 'modules/contact/store/reducer';
import globalsReducer from 'modules/global/store/reducer';

export default combineReducers({
  router: routerReducer,
  form: formReducer,
  auth: authReducer,
  leagues: leaguesReducer,
  wallet: walletReducer,
  h2h: h2hReducer,
  profile: profileReducer,
  notification: notificationReducer,
  contact: contactReducer,
  globals: globalsReducer
});
