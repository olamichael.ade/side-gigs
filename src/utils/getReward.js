const rewards = {
  'type-1': [
    { position: 1, reward: 50 },
    { position: 2, reward: 30 },
    { position: 3, reward: 20 }
  ],
  'type-2': [
    { position: 1, reward: 50 },
    { position: 2, reward: 35 },
    { position: 3, reward: 15 }
  ],
  'type-3': [
    { position: 1, reward: 50 },
    { position: 2, reward: 40 },
    { position: 3, reward: 10 }
  ],
  'type-4': [
    { position: 1, reward: 50 },
    { position: 2, reward: 45 },
    { position: 3, reward: 5 }
  ],
  'type-5': [
    { position: 1, reward: 55 },
    { position: 2, reward: 45 },
    { position: 3, reward: 0 }
  ],
  'type-6': [
    { position: 1, reward: 60 },
    { position: 2, reward: 20 },
    { position: 3, reward: 20 }
  ],
  'type-7': [
    { position: 1, reward: 60 },
    { position: 2, reward: 25 },
    { position: 3, reward: 15 }
  ],
  'type-8': [
    { position: 1, reward: 60 },
    { position: 2, reward: 30 },
    { position: 3, reward: 10 }
  ],
  'type-9': [
    { position: 1, reward: 60 },
    { position: 2, reward: 40 },
    { position: 3, reward: 0 }
  ],
  'type-10': [
    { position: 1, reward: 70 },
    { position: 2, reward: 15 },
    { position: 3, reward: 15 }
  ],
  'type-11': [
    { position: 1, reward: 70 },
    { position: 2, reward: 20 },
    { position: 3, reward: 10 }
  ],
  'type-12': [
    { position: 1, reward: 70 },
    { position: 2, reward: 30 },
    { position: 3, reward: 0 }
  ],
  'type-13': [
    { position: 1, reward: 80 },
    { position: 2, reward: 15 },
    { position: 3, reward: 5 }
  ],
  'type-14': [
    { position: 1, reward: 80 },
    { position: 2, reward: 20 },
    { position: 3, reward: 0 }
  ],
  'type-15': [
    { position: 1, reward: 90 },
    { position: 2, reward: 10 },
    { position: 3, reward: 0 }
  ],
  'type-16': [
    { position: 1, reward: 100 },
    { position: 2, reward: 0 },
    { position: 3, reward: 0 }
  ]
};

export const getRewardObject = rewardType => {
  return rewards[rewardType];
};

export const computeReward = cred => {
  return [
    {
      position: 1,
      reward: parseInt(cred.firstPositionPrice, 10)
    },
    {
      position: 2,
      reward: parseInt(cred.secondPositionPrice, 10)
    },
    {
      position: 3,
      reward: parseInt(cred.thirdPositionPrice, 10)
    }
  ];
};
