import request from './request';
import { CHARGE_CARD_URL, BANKS_URL } from './constants';

class Api {
  static registerUser(credentials) {
    return request('/auth/signup', 'POST', {
      body: JSON.stringify(credentials)
    });
  }

  static loginUser(credentials) {
    return request('/auth/login', 'POST', {
      body: JSON.stringify(credentials)
    });
  }

  static verifyTeamId(teamId) {
    return request(`/fpl/team/${teamId}`, 'GET');
  }

  static getUser() {
    return request(`/users/me`, 'GET_AUTH');
  }

  static getLeagues() {
    return request(`/leagues`, 'GET_AUTH');
  }

  static getUsersLeagues(id) {
    return request(`/users/${id}/leagues`, 'GET_AUTH');
  }

  static getOneLeague(code) {
    return request(`/leagues/code/${code}`, 'GET_AUTH');
  }

  static findLeagueById(id) {
    return request(`/leagues/${id}`, 'GET_AUTH');
  }

  static socialAuth(data) {
    return request('/auth/facebook', 'POST', {
      body: JSON.stringify(data)
    });
  }

  static getAccountTransactions() {
    return request('/transactions', 'GET_AUTH');
  }

  static getCards() {
    return request('/cards', 'GET_AUTH');
  }

  static fundWallet(userId, data) {
    return request(`/users/${userId}/fund`, 'POST_AUTH', {
      body: JSON.stringify(data)
    });
  }

  static saveCard(card) {
    return request(`/cards`, 'POST_AUTH', {
      body: JSON.stringify(card)
    });
  }

  static deleteCard(cardId) {
    return request(`/cards/${cardId}`, 'DELETE_AUTH');
  }

  static chargeCard(data) {
    return request(
      CHARGE_CARD_URL,
      'POST',
      {
        body: JSON.stringify(data)
      },
      null,
      true
    );
  }

  static createLeague(credentials) {
    return request('/leagues', 'POST_AUTH', {
      body: JSON.stringify(credentials)
    });
  }

  static getGameWeeks() {
    return request('/fpl/gameweeks', 'GET_AUTH');
  }

  static getBanks() {
    return request(BANKS_URL, 'GET', null, null, true);
  }

  static getAccounts() {
    return request('/accounts', 'GET_AUTH');
  }

  static deleteAccount(accountId) {
    return request(`/accounts/${accountId}/delete`, 'DELETE_AUTH');
  }

  static saveAccount(data) {
    return request(`/accounts`, 'POST_AUTH', {
      body: JSON.stringify(data)
    });
  }

  static payout(userId, data) {
    return request(`/users/${userId}/payout`, 'POST_AUTH', {
      body: JSON.stringify(data)
    });
  }

  static resetPassword(creds) {
    return request(`/auth/password/update`, 'POST', {
      body: JSON.stringify(creds)
    });
  }

  static requestPasswordReset(creds) {
    return request(`/auth/password/reset`, 'POST', {
      body: JSON.stringify(creds)
    });
  }

  static joinLeague(leagueId) {
    return request(`/leagues/${leagueId}/join`, 'POST_AUTH');
  }

  static createChallenge(data) {
    return request(`/h2h`, 'POST_AUTH', {
      body: JSON.stringify(data)
    });
  }

  static getCurrentWeek() {
    return request('/fpl/gameweeks/current', 'GET_AUTH');
  }

  static updateInfo(userInfo) {
    return request(`/users/me`, 'PUT_AUTH', {
      body: JSON.stringify(userInfo)
    });
  }

  static updatePassword(info) {
    return request(`/users/me/change-password`, 'PUT_AUTH', {
      body: JSON.stringify(info)
    });
  }
  static searchUser(name) {
    return request(`/users/search?name=${name}`, 'GET_AUTH');
  }

  static getOpenChallenge() {
    return request('/h2h-open', 'GET');
  }

  static fetchChallengeType(ct) {
    let challengeType = `/myh2h${ct ? `?type=${ct}` : ''}`;

    if (ct === 'all') challengeType = '/h2h-all';

    return request(challengeType, 'GET_AUTH');
  }

  static getH2h(id) {
    return request(`/h2h/${id}`, 'GET_AUTH');
  }

  static acceptChallenge(id) {
    return request(`/h2h/${id}/accept`, 'PATCH_AUTH');
  }

  static rejectChallenge(id) {
    return request(`/h2h/${id}/reject`, 'PATCH_AUTH');
  }

  static loadInvitations() {
    return request('/h2h-invites', 'GET_AUTH');
  }

  static loadJoinedLeagues() {
    return request('/leagues/joined-leagues', 'GET_AUTH');
  }

  static loadLeagueStanding(leagueId) {
    return request(`/leagues/${leagueId}/standing`, 'GET_AUTH');
  }

  static sendMessage(data) {
    return request('/messages/contactus', 'POST', {
      body: JSON.stringify(data)
    });
  }

  static loadTrendingLeagues() {
    return request('/leagues/trending', 'GET');
  }

  static updateH2h(h2h) {
    const { _id } = h2h;
    return request(`/h2h/${_id}`, 'PATCH_AUTH', {
      body: JSON.stringify(h2h)
    });
  }

  static getPromoLeagues() {
    return request('/promo-leagues', 'GET');
  }
}

export default Api;
