import moment from 'moment';
import { TOKEN_KEY, AUTH_PATHS } from './constants';

const DATE_FORMAT = 'YYYY-MM-DD HH:MM:SS';

export const capitalize = str =>
  str && str.replace(/\b\w/g, l => l.toUpperCase());

export const setToken = token => {
  if (!process.browser) {
    return false;
  }

  window.sessionStorage.setItem(TOKEN_KEY, token);
  return true;
};

export const getToken = () => window.sessionStorage[TOKEN_KEY] || '';

export const isAuthRoute = pathname => AUTH_PATHS.includes(pathname);

export const parseDateTime = str => {
  const isValid = moment(str).isValid();

  return isValid ? moment(str).format(DATE_FORMAT) : str;
};

export const getDayMonth = str => {
  const dateObj = new Date(str);
  const month = dateObj.getUTCMonth() + 1;
  const day = dateObj.getUTCDate();
  const year = dateObj.getUTCFullYear();
  return `${day}/${month}/${year}`;
};
export const currencyFormatter = str => {
  const value = str || 0;
  return value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
};
export const pad = (n, width, z) => {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

export const ccFormatter = number => {
  if (!number) return '';
  // xxxx xxxx xxxx xxxx xxxx
  const splitter = /.{1,4}/g;
  number = number.substring(0, 19);
  return (
    number
      .substring(0, 15)
      .match(splitter)
      .join(' ') + number.substring(15)
  );
};

export const credtCardNormalizer = (value, previousValue) => {
  if (!value) {
    return value;
  }
  const onlyNums = value.replace(/[^\d]/g, '');

  if (onlyNums.length <= 4) {
    return onlyNums;
  }
  if (onlyNums.length <= 8) {
    return onlyNums.slice(0, 4) + ' ' + onlyNums.slice(4);
  }
  if (onlyNums.length <= 12) {
    return (
      onlyNums.slice(0, 4) +
      ' ' +
      onlyNums.slice(4, 8) +
      ' ' +
      onlyNums.slice(8)
    );
  }
  return (
    onlyNums.slice(0, 4) +
    ' ' +
    onlyNums.slice(4, 8) +
    ' ' +
    onlyNums.slice(8, 12) +
    ' ' +
    onlyNums.slice(12)
  );
};

export const expiryDateNormalizer = value => {
  if (!value) {
    return value;
  }
  const onlyNums = value.replace(/[^\d]/g, '');

  if (onlyNums.length <= 2) {
    return onlyNums;
  }
  return onlyNums.slice(0, 2) + '/' + onlyNums.slice(2);
};

/**
 * Remove dashes added by the formatter. We want to store credit card number as plain numbers
 */
export const ccParser = number => (number ? number.replace(/-/g, '') : '');

export const parseClosingDate = str => moment(str).format('Do MMM, YYYY');
