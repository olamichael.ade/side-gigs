import React from 'react';
import { LinkedText, Text } from 'components';
import { strings } from './constants';

// Form Input validators
const isRequired = value =>
  value && value.length ? undefined : strings.REQUIRED_FIELD;

const isEmail = value => {
  return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? strings.EMAIL_REQUIRED
    : undefined;
};

const minParticiants = value =>
  value && value < 2 ? strings.MINIMUM_PARTICIPANT : undefined;

const isFieldRequired = msg => value => (value ? undefined : msg);

const passwordRequired = isFieldRequired(strings.PASSWORD_REQUIRED);
const emailRequired = isFieldRequired(strings.EMAIL_REQUIRED);
const leagueNameRequired = isFieldRequired(strings.LEAGUE_NAME_REQUIRED);

const amountRequired = isFieldRequired(strings.AMOUNT_REQUIRED);
const feeRequired = isFieldRequired(strings.FEE_REQUIRED);
const challengeRequired = isFieldRequired(strings.CHALLLENGE_TITLE_REQUIRED);

const minAmountRequired = maxAmount => value => {
  if (value && Number(value) > maxAmount) return strings.AMT_EXCEEDS_BALANCE;

  if (value && Number(value) <= 500) return 'Minimum amount is N500';

  return undefined;
};

const minWithdrawalAmount = maxAmount => value => {
  if (!value) return 'Participation fee is required.';

  return value.length && Number(value) <= maxAmount ? (
    undefined
  ) : (
    <div key="help-text">
      <Text key="head-text">{strings.MIN_WITHDRAWAL_AMOUNT}</Text>.{' '}
      <Text key="click">Click </Text>
      <LinkedText key="link-text" to="/wallet/fund">
        here
      </LinkedText>{' '}
      <Text key="tail-text">to fund wallet</Text>
    </div>
  );
};

const minAmount = value => {
  return Number(value) <= 500 ? strings.MIN_AMOUNT : undefined;
};

export const errors = {
  isEmail,
  passwordRequired,
  emailRequired,
  isRequired,
  amountRequired,
  minWithdrawalAmount,
  leagueNameRequired,
  feeRequired,
  challengeRequired,
  minParticiants,
  minAmount,
  minAmountRequired
};
