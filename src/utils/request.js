import 'whatwg-fetch';
import { isUndefined } from 'lodash';
import { errorCodes } from './constants';
import { getToken } from './utils';

const {
  HTTP_200_OK,
  HTTP_300_MULTIPLE_CHOICES,
  HTTP_204_NO_CONTENT
} = errorCodes;

const POST_OPTIONS = {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
};

const PUT_OPTIONS = {
  method: 'PUT',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
};

const PATCH_OPTIONS = {
  method: 'PATCH',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
};

const OPTIONS_OPTIONS = {
  method: 'OPTIONS',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
};

const AuthOptions = method => ({
  method: method,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${getToken()}`
  }
});

const GET_OPTIONS = {
  method: 'GET'
};

function checkRequestStatus(response) {
  if (response.status === HTTP_204_NO_CONTENT) return {};

  const json = response.json();

  if (
    response.status >= HTTP_200_OK &&
    response.status < HTTP_300_MULTIPLE_CHOICES
  ) {
    return json;
  }

  return json.then(err => {
    const error = new Error(response.status);
    error.body = err;
    error.errorStatus = response.status;
    error.errorMessage = err.errors;
    error.message = err.message;
    throw error;
  });
}

/**
 * @param {string} url The URL we want to request
 * @param {object} [options] The options we wan to pass to fetch api
 * @return {object} The response data
 */

export default function(url, method, options, errorHandler, isExternalApi) {
  const API_URL = process.env.REACT_APP_API;

  const ROOT_URL = isExternalApi ? url : `${API_URL}${url}`;
  let errHandler = errorHandler;
  if (isUndefined(errHandler)) {
    errHandler = err => {
      throw err;
    };
  }

  let bakedOptions;
  const meth = typeof method === 'string' ? method : '';
  switch ((meth || '').toUpperCase()) {
    case 'PUT':
      bakedOptions = PUT_OPTIONS;
      break;
    case 'POST':
      bakedOptions = POST_OPTIONS;
      break;
    case 'PATCH':
      bakedOptions = PATCH_OPTIONS;
      break;
    case 'OPTIONS':
      bakedOptions = OPTIONS_OPTIONS;
      break;
    case 'GET_AUTH':
      bakedOptions = AuthOptions('GET');
      break;
    case 'POST_AUTH':
      bakedOptions = AuthOptions('POST');
      break;
    case 'PATCH_AUTH':
      bakedOptions = AuthOptions('PATCH');
      break;
    case 'PUT_AUTH':
      bakedOptions = AuthOptions('PUT');
      break;
    case 'DELETE_AUTH':
      bakedOptions = AuthOptions('DELETE');
      break;
    default:
      bakedOptions = GET_OPTIONS;
  }
  return fetch(ROOT_URL, {
    ...bakedOptions,
    ...options
  })
    .then(checkRequestStatus)
    .catch(errHandler);
}
