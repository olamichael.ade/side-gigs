// export const isDevMode = process.env.NODE_ENV === 'development';
export const FB_APPID = process.env.REACT_APP_FB_APPID;

export const SECKEY = process.env.REACT_APP_SECKEY;

export const PBFKEY = process.env.REACT_APP_PBFKEY;

export const CHARGE_CARD_URL = process.env.REACT_APP_CHARGE_CARD_URL;

export const BANKS_URL = process.env.REACT_APP_BANKS_URL;
export const CLOUD_NAME = process.env.REACT_APP_CLOUD_NAME;

export const TOKEN_KEY = `FFL_USER_${process.env.NODE_ENV}`;
export const FACEBOOK_PROVIDER = 'facebook';

export const HOME = '/';
export const LOGIN = '/login';
export const SIGNUP = '/signup';
export const FORGOT_PASSWORD = '/forgot-password';
export const RESET_PASSWORD = '/reset-password';

export const AUTH_PATHS = [
  HOME,
  LOGIN,
  SIGNUP,
  FORGOT_PASSWORD,
  RESET_PASSWORD
];

// Auth strings
export const LOGIN_ERROR = 'Login Failed! Please try again.';

// Error Codes
const HTTP_200_OK = 200;
const HTTP_300_MULTIPLE_CHOICES = 300;
const HTTP_400_BAD_REQUEST = 400;
const HTTP_401_UNAUTHORIZED = 401;
const HTTP_403_FORBIDDEN = 403;
const HTTP_404_NOT_FOUND = 404;
const HTTP_422_UNKNOWN = 422;
const HTTP_500_INTERNAL_SERVER_ERROR = 500;
const HTTP_503_SERVICE_UNAVAILABLE = 503;
const HTTP_204_NO_CONTENT = 204;

// Validation strings
const EMAIL_REQUIRED = 'Please enter a valid email';
const PASSWORD_REQUIRED = 'Please enter your password';
const REQUIRED_FIELD = 'Required Field';
const AMOUNT_REQUIRED = 'Please enter amount greater than 0';
const MIN_WITHDRAWAL_AMOUNT = 'Amount exceeds your current balance';
const AMT_EXCEEDS_BALANCE = 'Amount exceeds your current balance';
const MIN_AMOUNT = 'Minimum participation fee is N500';
const LEAGUE_NAME_REQUIRED = 'League Name cannot be empty';
const CHALLLENGE_TITLE_REQUIRED = 'Challenge title cannot be empty';
const FEE_REQUIRED = 'Participation Fee is required';
const MINIMUM_PARTICIPANT =
  'Minimum participants cannot be lower than two people';

export const errorCodes = {
  HTTP_404_NOT_FOUND,
  HTTP_503_SERVICE_UNAVAILABLE,
  HTTP_401_UNAUTHORIZED,
  HTTP_403_FORBIDDEN,
  HTTP_300_MULTIPLE_CHOICES,
  HTTP_200_OK,
  HTTP_500_INTERNAL_SERVER_ERROR,
  HTTP_400_BAD_REQUEST,
  HTTP_422_UNKNOWN,
  HTTP_204_NO_CONTENT
};

export const cardTypes = {
  MASTERCARD: 'Mastercard',
  VISA: 'Visa',
  VERVE: 'Verve'
};

export const strings = {
  EMAIL_REQUIRED,
  PASSWORD_REQUIRED,
  REQUIRED_FIELD,
  AMOUNT_REQUIRED,
  MIN_WITHDRAWAL_AMOUNT,
  LEAGUE_NAME_REQUIRED,
  CHALLLENGE_TITLE_REQUIRED,
  FEE_REQUIRED,
  MINIMUM_PARTICIPANT,
  MIN_AMOUNT,
  AMT_EXCEEDS_BALANCE
};
