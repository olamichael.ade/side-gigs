import React from 'react';
import T from 'prop-types';
import { Text } from 'components';
import { ResetForm } from '../ResetForm';
import '../../../../pages/Auth.scss';

const PasswordReset = ({
  resetPassword,
  passwordMisMatch,
  isSending,
  history,
  error,
  message
}) => {
  const resetPasswordAndLogin = creds => {
    if (creds.confirm_password !== creds.password) {
      return passwordMisMatch();
    }
    return resetPassword(creds).then(reset => {
      if (reset && reset.ok) history.push('/login');
    });
  };

  return (
    <div>
      <div className="auth-header">
        <Text classText="auth-header-text" color="white" weight="med">
          Reset Password?
        </Text>
      </div>
      {error &&
        message && <div className="auth-message-box error">{message}</div>}
      <div className="form-subtitle-container">
        <Text classText="auth-subheader-text" color="white">
          Enter your new password to stay protected.
        </Text>
      </div>
      <ResetForm loading={isSending} onSubmit={resetPasswordAndLogin} />
    </div>
  );
};

PasswordReset.propTypes = {
  resetPassword: T.func,
  isVerifying: T.bool,
  isComplete: T.bool
};

export default PasswordReset;
