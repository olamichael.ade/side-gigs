import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
import Input from 'components/Input';
// import { errors } from 'utils/validators';
import { RESET_PASSWORD_FORM } from 'modules/auth/constants';
import '../../../../pages/Auth.scss';

const FormInput = Input.form;

const WrappedForm = props => {
  const { onSubmit, loading, handleSubmit, invalid } = props;

  const inputLayout = {
    marginBottom: 20
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="login-form">
      <div>
        <label>
          <Text color="white">New Password</Text>
        </label>
        <Field
          className="form-input"
          name="password"
          placeholder=""
          type="password"
          component={FormInput}
          formLayout={inputLayout}
        />
      </div>
      <div>
        <label>
          <Text color="white">Confirm Password</Text>
        </label>
        <Field
          className="form-input"
          name="confirm_password"
          placeholder=""
          type="password"
          component={FormInput}
          formLayout={inputLayout}
        />
      </div>
      <div>
        <label>
          <Text color="white">Verification Code</Text>
        </label>
        <Field
          className="form-input"
          name="shortCode"
          placeholder=""
          type="text"
          required
          component={FormInput}
          formLayout={inputLayout}
        />
      </div>
      <div className="auth-btn-container">
        <Button
          loading={loading}
          className="auth-form-button"
          disabled={invalid}
          htmlType="submit"
        >
          <Text color="white">{loading ? 'Sending...' : 'CONFIRM'}</Text>
        </Button>
      </div>
    </form>
  );
};

WrappedForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

export const ResetForm = reduxForm({ form: RESET_PASSWORD_FORM })(WrappedForm);
