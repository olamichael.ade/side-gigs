import { withRouter } from 'react-router';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { resetPassword, passwordMisMatch } from 'modules/auth/store/actions';
import PasswordReset from './components/PasswordReset';

const mapStateToProps = state => {
  const {
    auth: { error, errorMessage, isSending }
  } = state;

  return {
    error,
    message: errorMessage,
    isSending
  };
};

export default compose(
  connect(
    mapStateToProps,
    { resetPassword, passwordMisMatch }
  ),
  withRouter
)(PasswordReset);
