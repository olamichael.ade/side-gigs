import React from 'react';
import { Text, LinkedText, SocialAuth } from 'components';
import { LoginForm } from '../LoginForm';
import './login.scss';

const Login = props => {
  const { loading, requestLogin, socialAuth, error, message, history } = props;
  const { state = {} } = history.location || {};

  const handleLogin = creds => {
    requestLogin(creds).then(() => {
      const { pathname, search } = state.from || {};
      if (pathname) {
        history.push(`${pathname}${search || ''}`);
      }
    });
  };

  return (
    <div className="login-container">
      <div className="box">
        <div className="auth-header">
          <Text classText="auth-header-text" color="black" weight="med">
            Welcome back
          </Text>
        </div>
        {error && <div className="auth-message-box error">{message}</div>}
        <LoginForm loading={loading} onSubmit={creds => handleLogin(creds)} />
        <div className="auth-alt-container">
          <div className="alt-container">
            <Text classText="social-alt-text" color="black">
              Or continue with
            </Text>
          </div>
          <SocialAuth onClick={cred => socialAuth(cred)} label="Facebook" />
          <div className="alt-container cta-section">
            <LinkedText className="signup-link" to="/forgot-password">
              Forgot Password?
            </LinkedText>
          </div>
        </div>
      </div>
      <div className="auth-footer">
        <Text classText="auth-footer-text" color="white">
          I am new here,{' '}
          <LinkedText className="signup-link" to="/signup">
            Sign up
          </LinkedText>
        </Text>
      </div>
    </div>
  );
};

export default Login;
