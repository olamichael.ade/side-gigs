import { withRouter } from 'react-router';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { RedirectHome, PluginComponent } from 'HOCS';
import { requestLogin, socialAuth } from 'modules/auth/store/actions';
import Login from './components/Login';

const mapStateToProps = state => {
  const { auth } = state;
  return {
    loading: auth.loading,
    loaded: auth.loaded,
    error: auth.error,
    message: auth.errorMessage,
    isAuthenticated: auth.isAuthenticated
  };
};

export default compose(
  connect(
    mapStateToProps,
    { requestLogin, socialAuth }
  ),
  withRouter,
  RedirectHome,
  PluginComponent
)(Login);
