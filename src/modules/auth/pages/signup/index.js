import { withRouter } from 'react-router';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { PluginComponent, RedirectHome } from 'HOCS';
import { registerUser, socialAuth, setFplId } from 'modules/auth/store/actions';

import Signup from './components/Signup';

const mapStateToProps = state => {
  const { auth } = state;

  const { fplUser } = auth;
  return {
    loading: auth.loading,
    loaded: auth.loaded,
    isVerifying: auth.isVerifying,
    error: auth.error,
    errorMessage: auth.errorMessage || auth.message,
    isAuthenticated: auth.isAuthenticated,
    fplUser,
    initialValues: {
      firstName: fplUser.firstName || '',
      lastName: fplUser.lastName || ''
    }
  };
};

export default compose(
  connect(
    mapStateToProps,
    { registerUser, socialAuth, setFplId }
  ),
  withRouter,
  RedirectHome,
  PluginComponent
)(Signup);
