import React from 'react';
import { Text, LinkedText, SocialAuth } from 'components';
import { SignupForm } from '../SignupForm';
import { IdForm } from '../IdForm';
import './signup.scss';

const Signup = props => {
  const {
    loading,
    initialValues,
    fplUser,
    registerUser,
    socialAuth,
    error,
    errorMessage,
    setFplId,
    isVerifying
  } = props;

  const handleVerify = teamId => {
    setFplId(teamId);
  };

  return (
    <div className="signup-container">
      <div className="box">
        <div className="auth-header">
          <Text classText="auth-header-text" color="black" weight="med">
            Create account
          </Text>
        </div>
        {error &&
          errorMessage && (
            <div className="auth-message-box error">{errorMessage}</div>
          )}
        {!fplUser || !fplUser.id ? (
          <IdForm
            loading={isVerifying}
            onSubmit={handleVerify}
            hasError={error}
            message={errorMessage}
          />
        ) : (
          <SignupForm
            initialValues={initialValues}
            loading={loading}
            onSubmit={creds => registerUser(creds)}
          />
        )}
        {fplUser &&
          fplUser.id && (
            <div className="auth-alt-container">
              <div className="alt-container">
                <Text classText="social-alt-text" color="black">
                  Or continue with
                </Text>
              </div>

              <SocialAuth onClick={cred => socialAuth(cred)} label="Facebook" />
            </div>
          )}
      </div>
      <div className="auth-footer">
        <Text classText="auth-footer-text" color="white">
          I have an account,{' '}
          <LinkedText className="signup-link" to="/login">
            Sign in
          </LinkedText>
        </Text>
      </div>
    </div>
  );
};

export default Signup;
