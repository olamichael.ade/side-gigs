import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
import Input from 'components/Input';
import { errors } from 'utils/validators';
import { SIGN_UP_FORM } from 'modules/auth/constants';
import '../../../../pages/Auth.scss';

const FormInput = Input.form;

const WrappedSignupForm = props => {
  const { onSubmit, loading, handleSubmit, invalid } = props;

  const inputLayout = {
    marginBottom: 20
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="login-form">
      <div className="text-left">
        <label>
          <Text color="black">First name</Text>
        </label>
        <Field
          className="form-input"
          name="firstName"
          type="text"
          placeholder=""
          component={FormInput}
          formLayout={inputLayout}
          validate={errors.isRequired}
        />
      </div>
      <div className="text-left">
        <label>
          <Text color="black">Last name</Text>
        </label>
        <Field
          className="form-input"
          name="lastName"
          type="text"
          placeholder=""
          component={FormInput}
          formLayout={inputLayout}
          validate={errors.isRequired}
        />
      </div>
      <div className="text-left">
        <label>
          <Text color="black">Email</Text>
        </label>
        <Field
          className="form-input"
          name="email"
          placeholder=""
          type="text"
          component={FormInput}
          formLayout={inputLayout}
          validate={[errors.emailRequired, errors.isEmail]}
        />
      </div>
      <div className="text-left">
        <label>
          <Text color="black">Phone Number</Text>
        </label>
        <Field
          className="form-input"
          name="phone"
          placeholder="Enter your phone number"
          type="number"
          component={FormInput}
          formLayout={inputLayout}
        />
      </div>
      <div className="text-left">
        <label>
          <Text color="black">Password</Text>
        </label>
        <Field
          className="form-input"
          name="password"
          type="password"
          placeholder=""
          component={FormInput}
          formLayout={inputLayout}
          validate={errors.passwordRequired}
        />
      </div>
      <div className="auth-btn-container">
        <Button
          loading={loading}
          className="auth-form-button"
          disabled={invalid}
          htmlType="submit"
        >
          <Text color="white">{loading ? 'Signing up...' : 'Sign Up'}</Text>
        </Button>
      </div>
    </form>
  );
};

WrappedSignupForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

export const SignupForm = reduxForm({
  form: SIGN_UP_FORM,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(WrappedSignupForm);
