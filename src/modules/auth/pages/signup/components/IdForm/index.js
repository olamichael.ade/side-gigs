import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text, LinkedText } from 'components';
import Input from 'components/Input';
import { errors } from 'utils/validators';
import { FPL_ID_FORM } from 'modules/auth/constants';
import './idform.scss';

const FormInput = Input.form;

const WrappedIdForm = props => {
  const { onSubmit, handleSubmit, invalid, loading } = props;

  const inputLayout = {
    marginBottom: 20
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="login-form">
      <Field
        className="form-input without-bg"
        name="teamId"
        placeholder="Enter your FPL ID"
        type="text"
        component={FormInput}
        formLayout={inputLayout}
        validate={[errors.isRequired]}
      />
      <div className="how-to-get-text">
        <LinkedText className="signup-link" to="/get-fpl-id">
          How to get my FPL ID
        </LinkedText>
      </div>
      <div className="auth-btn-container">
        <Button
          loading={loading}
          className="auth-form-button"
          disabled={invalid}
          htmlType="submit"
        >
          <Text color="white">{loading ? 'Verifying...' : 'PROCEED'}</Text>
        </Button>
      </div>
    </form>
  );
};

WrappedIdForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string,
  hasError: PropTypes.bool,
  message: PropTypes.string
};

export const IdForm = reduxForm({ form: FPL_ID_FORM })(WrappedIdForm);
