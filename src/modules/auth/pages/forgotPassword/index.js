import { withRouter } from 'react-router';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { requestPasswordReset } from 'modules/auth/store/actions';
import ForgotPassword from './components/ForgotPassword';

const mapStateToProps = state => {
  const {
    auth: { error, errorMessage, isVerifying, isComplete }
  } = state;

  return {
    error,
    message: errorMessage,
    isComplete,
    isVerifying
  };
};

export default compose(
  connect(
    mapStateToProps,
    { requestPasswordReset }
  ),
  withRouter
)(ForgotPassword);
