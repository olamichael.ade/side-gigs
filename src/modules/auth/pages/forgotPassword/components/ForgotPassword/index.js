import React from 'react';
import T from 'prop-types';
import { Text } from 'components';
import { ForgotPasswordForm } from '../ForgotPasswordForm';
import '../../../../pages/Auth.scss';

const ForgotPassword = ({
  requestPasswordReset,
  isVerifying,
  isComplete,
  error,
  message,
  history
}) => {
  const requestAndRedirect = creds => {
    requestPasswordReset(creds).then(response => {
      if (response.isSent) {
        const path = '/reset-password';
        history.push(path);
      }
    });
  };
  return (
    <div>
      <div className="auth-header">
        <Text classText="auth-header-text" color="white" weight="med">
          Forgot Password?
        </Text>
      </div>
      {error && <div className="auth-message-box error">{message}</div>}
      <div className="form-subtitle-container">
        <Text classText="auth-subheader-text" color="white">
          Enter your email address and we'll email you a verification code.
        </Text>
      </div>
      <ForgotPasswordForm loading={isVerifying} onSubmit={requestAndRedirect} />
    </div>
  );
};

ForgotPassword.propTypes = {
  requestPasswordReset: T.func,
  isVerifying: T.bool,
  isComplete: T.bool
};

export default ForgotPassword;
