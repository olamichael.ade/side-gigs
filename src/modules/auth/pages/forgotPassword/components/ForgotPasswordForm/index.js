import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
import Input from 'components/Input';
import { errors } from 'utils/validators';
import { FORGOT_PASSWORD_FORM } from 'modules/auth/constants';
import '../../../../pages/Auth.scss';

const FormInput = Input.form;

const WrappedForgotPasswordForm = props => {
  const { onSubmit, loading, handleSubmit, invalid } = props;

  const inputLayout = {
    marginBottom: 20
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="login-form">
      <div>
        <label>
          <Text color="white">Email</Text>
        </label>
        <Field
          className="form-input"
          name="email"
          placeholder=""
          type="text"
          component={FormInput}
          formLayout={inputLayout}
          validate={[errors.emailRequired, errors.isEmail]}
        />
      </div>
      <div className="auth-btn-container">
        <Button
          loading={loading}
          className="auth-form-button"
          disabled={invalid}
          htmlType="submit"
        >
          <Text color="white">
            {loading ? 'Sending...' : 'SEND VERIFICATION CODE'}
          </Text>
        </Button>
      </div>
    </form>
  );
};

WrappedForgotPasswordForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

export const ForgotPasswordForm = reduxForm({ form: FORGOT_PASSWORD_FORM })(
  WrappedForgotPasswordForm
);
