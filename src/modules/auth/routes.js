import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/login',
    exact: true,
    auth: false, // Check if its an authenticated route
    component: Loadable({
      loader: () => import(/* webpackChunkName: "login" */ './pages/login'),
      loading: Loader,
      modules: ['login']
    })
  },
  {
    path: '/signup',
    exact: true,
    auth: false, // Check if its an authenticated route
    component: Loadable({
      loader: () => import(/* webpackChunkName: "signup" */ './pages/signup'),
      loading: Loader,
      modules: ['signup']
    })
  },
  {
    path: '/forgot-password',
    exact: true,
    auth: false, // Check if its an authenticated route
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "forgotPassword" */ './pages/forgotPassword'),
      loading: Loader,
      modules: ['forgotPassword']
    })
  },
  {
    path: '/reset-password',
    exact: true,
    auth: false, // Check if its an authenticated route
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "resetPassword" */ './pages/resetPassword'),
      loading: Loader,
      modules: ['resetPassword']
    })
  }
];

export default routes;
