import Api from 'utils/Api';
import {
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILED,
  AUTH_CHECK,
  LOGOUT_USER,
  SET_FPL_USER,
  VERIFY_FPL_ID,
  VERIFY_FPL_COMPLETE,
  VERIFY_FPL_FAILED,
  AUTH_RESET_PASSWORD_REQUEST,
  AUTH_RESET_PASSWORD_REQUEST_SUCCESS,
  AUTH_RESET_PASSWORD_REQUEST_FAILURE,
  AUTH_UPDATE_PASSWORD,
  AUTH_UPDATE_PASSWORD_SUCCESS,
  AUTH_UPDATE_PASSWORD_FAILURE,
  PASSWORD_MISMATCH
} from './actionTypes';
import { setToken } from 'utils/utils';
import { loadJoinedLeagues } from 'modules/leagues/store/actions';

function authCheck() {
  return dispatch => dispatch({ type: AUTH_CHECK });
}

function updateUser() {
  return async dispatch => {
    try {
      const { data } = await Api.getUser();
      // If App is unable to load user. we want to destroy the current session and force a reload.

      if (!data) {
        return dispatch({
          type: LOGOUT_USER
        });
      }
      //
      dispatch({ type: AUTH_LOGIN_SUCCESS, user: data });
    } catch (e) {
      return dispatch({
        type: AUTH_LOGIN_FAILED,
        message: 'Could not get user'
      });
    }
  };
}

function loadUser() {
  return async dispatch => {
    try {
      const { data } = await Api.getUser();
      // If App is unable to load user. we want to destroy the current session and force a reload.

      if (!data) {
        return dispatch({
          type: LOGOUT_USER
        });
      }
      //
      dispatch({ type: AUTH_LOGIN_SUCCESS, user: data });
      dispatch(loadJoinedLeagues());
    } catch (e) {
      return dispatch({
        type: AUTH_LOGIN_FAILED,
        message: 'Could not get user'
      });
    }
  };
}

function loginUser(token) {
  setToken(token);
  return dispatch => dispatch(loadUser());
}

function logoutUser() {
  return dispatch => dispatch({ type: LOGOUT_USER });
}

function requestLogin(credentials) {
  return dispatch => {
    dispatch({ type: AUTH_LOGIN });
    return Api.loginUser(credentials)
      .then(({ token }) => dispatch(loginUser(token)))
      .catch(() => {
        dispatch({
          type: AUTH_LOGIN_FAILED,
          message: 'Invalid Email/Password'
        });
      });
  };
}

function getPayload(params) {
  const { email, firstName, lastName, id, password, phone } = params;

  return {
    team_id: id,
    email,
    name: `${firstName} ${lastName}`,
    password,
    phone
  };
}

function registerUser(creds) {
  return (dispatch, getState) => {
    const {
      auth: { fplUser }
    } = getState();
    const payload = getPayload({ ...fplUser, ...creds });
    dispatch({ type: AUTH_LOGIN });
    return Api.registerUser(payload)
      .then(({ token }) => {
        setToken(token);
        dispatch(loginUser(token));
      })
      .catch(err => {
        dispatch({
          type: AUTH_LOGIN_FAILED,
          message: err.body.message || 'Invalid Credentials'
        });
      });
  };
}

function setFplId(data) {
  return dispatch => {
    dispatch({ type: VERIFY_FPL_ID });
    return Api.verifyTeamId(data.teamId)
      .then(res => {
        const {
          data: { id, player_first_name, player_last_name }
        } = res;
        dispatch({
          type: SET_FPL_USER,
          fplUser: {
            id,
            firstName: player_first_name,
            lastName: player_last_name
          }
        });
        dispatch({ type: VERIFY_FPL_COMPLETE });
      })
      .catch(err => {
        dispatch({ type: VERIFY_FPL_FAILED, message: 'Invalid FPL ID' });
      });
  };
}

function socialAuth(data) {
  return (dispatch, getState) => {
    const { auth } = getState();

    const payload = { ...data, teamId: auth.fplUser.id };
    dispatch({ type: AUTH_LOGIN });
    Api.socialAuth(payload)
      .then(({ token }) => {
        dispatch(loginUser(token));
        dispatch({ type: AUTH_LOGIN_SUCCESS });
      })
      .catch(err => {
        return dispatch({
          type: AUTH_LOGIN_FAILED,
          message: 'Failed to authenticate with FB'
        });
      });
  };
}

function resetPassword(creds) {
  return dispatch => {
    dispatch({ type: AUTH_UPDATE_PASSWORD });
    return Api.resetPassword(creds)
      .then(res => {
        dispatch({ type: AUTH_UPDATE_PASSWORD_SUCCESS, message: res.message });
        // Attempt login???? or redirect users to the home page to start inputing the teamID all over.
        // this looks like a long process.
        // will try to look for a way to handle this better.
        // when I am motivated.
        // 😄

        return { ok: true };
      })
      .catch(err => {
        dispatch({ type: AUTH_UPDATE_PASSWORD_FAILURE, message: err.message });
      });
  };
}

function requestPasswordReset(creds) {
  return dispatch => {
    dispatch({ type: AUTH_RESET_PASSWORD_REQUEST });
    return Api.requestPasswordReset(creds)
      .then(res => {
        dispatch({
          type: AUTH_RESET_PASSWORD_REQUEST_SUCCESS
        });
        return { isSent: true };
      })
      .catch(err => {
        dispatch({
          type: AUTH_RESET_PASSWORD_REQUEST_FAILURE,
          message:
            err.message === '404'
              ? 'No user associated with the email address found'
              : 'Error reseting your password. Please try again.'
        });
      });
  };
}
function passwordMisMatch() {
  return dispatch => dispatch({ type: PASSWORD_MISMATCH });
}

export {
  authCheck,
  requestLogin,
  loadUser,
  logoutUser,
  setFplId,
  registerUser,
  socialAuth,
  resetPassword,
  requestPasswordReset,
  passwordMisMatch,
  updateUser
};
