import {
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILED,
  AUTH_CHECK,
  LOGOUT_USER,
  SET_FPL_USER,
  VERIFY_FPL_ID,
  VERIFY_FPL_COMPLETE,
  VERIFY_FPL_FAILED,
  AUTH_RESET_PASSWORD_REQUEST,
  AUTH_RESET_PASSWORD_REQUEST_FAILURE,
  AUTH_RESET_PASSWORD_REQUEST_SUCCESS,
  AUTH_UPDATE_PASSWORD,
  AUTH_UPDATE_PASSWORD_SUCCESS,
  AUTH_UPDATE_PASSWORD_FAILURE,
  PASSWORD_MISMATCH
} from './actionTypes';
import { TOKEN_KEY } from 'utils/constants';

const initialState = {
  user: null,
  loading: false,
  isAuthenticated: false,
  loaded: false,
  error: false,
  errorMessage: '',
  success: false,
  isVerifying: false,
  fplUser: {},
  isComplete: false,
  isSending: false
};

function checkAuth(state) {
  return {
    ...state,
    isAuthenticated: !!sessionStorage.getItem(TOKEN_KEY)
  };
}

function isRequest(state) {
  return {
    ...state,
    loading: true,
    loaded: false,
    error: false,
    errorMessage: ''
  };
}

function isRequestSuccess(state, user) {
  return {
    ...state,
    loading: false,
    loaded: true,
    error: false,
    errorMessage: '',
    user,
    isAuthenticated: true,
    isSuccess: false
  };
}

function isRequestFailed(state, payload) {
  return {
    ...state,
    loading: false,
    loaded: false,
    error: true,
    errorMessage: payload.message
  };
}

function logout(state) {
  sessionStorage.removeItem(TOKEN_KEY);
  return {
    ...state,
    isAuthenticated: false,
    user: null
  };
}

function updateState(state, key, payload) {
  return Object.assign({}, state, { [key]: payload });
}

function authReducer(state = initialState, action) {
  switch (action.type) {
    case AUTH_LOGIN:
      return isRequest(state);
    case AUTH_LOGIN_SUCCESS:
      return isRequestSuccess(state, action.user);
    case AUTH_LOGIN_FAILED:
      return isRequestFailed(state, action);
    case AUTH_CHECK:
      return checkAuth(state);
    case LOGOUT_USER:
      return logout(state);
    case VERIFY_FPL_ID:
      return Object.assign({}, state, {
        isVerifying: true,
        error: false,
        message: ''
      });
    case VERIFY_FPL_COMPLETE:
      return updateState(state, 'isVerifying', false);
    case VERIFY_FPL_FAILED:
      const { message } = action;
      return { ...state, error: true, isVerifying: false, message };
    case SET_FPL_USER:
      const { fplUser } = action;
      return updateState(state, 'fplUser', fplUser);
    case AUTH_RESET_PASSWORD_REQUEST:
      return { ...state, isVerifying: true };
    case AUTH_RESET_PASSWORD_REQUEST_FAILURE:
      return {
        ...state,
        error: true,
        errorMessage: action.message,
        isVerifying: false
      };
    case AUTH_RESET_PASSWORD_REQUEST_SUCCESS:
      return { ...state, error: false, isVerifying: false };
    case AUTH_UPDATE_PASSWORD:
      return { ...state, error: false, isSending: true };
    case AUTH_UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        isSuccess: true,
        message: 'Password Updated Successfully',
        isSending: false
      };
    case AUTH_UPDATE_PASSWORD_FAILURE:
      return {
        ...state,
        error: true,
        isSending: false,
        errorMessage: action.message,
        message: action.message
      };
    case PASSWORD_MISMATCH:
      return { ...state, errorMessage: "Password don't match", error: true };

    default:
      return { ...state };
  }
}

export default authReducer;
