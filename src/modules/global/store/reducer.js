import {
  LOAD_PROMO_LEAGUES,
  LOAD_PROMO_FAILED,
  LOAD_PROMO_SUCCESS,
  REFRESH_PROMO_LEAGUES
} from './actionType';

const initialState = {
  promoLeagues: [],
  loading: false,
  loaded: false
};

function globalReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_PROMO_LEAGUES:
      return { ...state, loading: true };
    case LOAD_PROMO_SUCCESS:
      const { promoLeagues } = action;
      return { ...state, promoLeagues, loading: false, loaded: true };
    case LOAD_PROMO_FAILED:
      return { ...state, loading: false, loaded: true };
    case REFRESH_PROMO_LEAGUES:
      return { ...initialState };
    default:
      return { ...state };
  }
}

export default globalReducer;
