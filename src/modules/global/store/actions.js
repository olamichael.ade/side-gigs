import Api from 'utils/Api';
import {
  LOAD_PROMO_LEAGUES,
  LOAD_PROMO_SUCCESS,
  LOAD_PROMO_FAILED,
  REFRESH_PROMO_LEAGUES
} from './actionType';

function getPromoLeagues() {
  return dispatch => {
    dispatch({ type: LOAD_PROMO_LEAGUES });
    Api.getPromoLeagues()
      .then(res => {
        dispatch({ type: LOAD_PROMO_SUCCESS, promoLeagues: res });
      })
      .catch(err => {
        console.log('Could not get promotional leagues.');
        dispatch({ type: LOAD_PROMO_FAILED });
      });
  };
}

function refreshPromoLeagues() {
  return dispatch => dispatch({ type: REFRESH_PROMO_LEAGUES });
}

export { getPromoLeagues, refreshPromoLeagues };
