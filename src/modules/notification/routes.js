import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/notification',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "notificationDashboard" */ './pages/Dashboard'),
      loading: Loader,
      modules: ['notificationDashboard']
    })
  }
];

export default routes;
