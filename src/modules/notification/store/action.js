import Api from 'utils/Api';
import {
  REFRESH,
  AJAX_COMPLETE,
  AJAX_REQUEST,
  LOAD_NOTIFICATION
} from './actionTypes';

function refresh() {
  return dispatch => dispatch({ type: REFRESH });
}

function loadInvitations() {
  return dispatch => {
    dispatch({ type: AJAX_REQUEST });
    Api.loadInvitations()
      .then(res => {
        dispatch({ type: AJAX_COMPLETE });
        dispatch({ type: LOAD_NOTIFICATION, invitations: res.data });
      })
      .catch(() => {
        dispatch({ type: AJAX_COMPLETE });
        console.log('Could not get h2h challenge invitations');
      });
  };
}

export { loadInvitations, refresh };
