import {
  LOAD_NOTIFICATION,
  AJAX_COMPLETE,
  AJAX_REQUEST,
  REFRESH
} from './actionTypes';

const initialState = {
  loading: true,
  loaded: false,
  invitations: []
};

function notificationReducer(state = initialState, action) {
  switch (action.type) {
    case AJAX_REQUEST: {
      return { ...state, loading: true };
    }
    case AJAX_COMPLETE: {
      return { ...state, loading: false, loaded: true };
    }
    case LOAD_NOTIFICATION: {
      const { invitations } = action;
      return { ...state, invitations };
    }
    case REFRESH: {
      return { ...initialState };
    }
    default:
      return { ...state };
  }
}

export default notificationReducer;
