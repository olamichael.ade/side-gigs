export const LOAD_NOTIFICATION = 'h2h/LOAD_NOTIFICATION';
export const AJAX_REQUEST = 'h2h/AJAX_REQUEST';
export const AJAX_COMPLETE = 'h2h/AJAX_COMPLETE';
export const REFRESH = 'h2h/REFRESH';
