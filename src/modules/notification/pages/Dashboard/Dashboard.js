import React from 'react';
import { Text, Topbar } from 'components';
import Tabs from '../../components/Tabs';

import './index.scss';

const NotificationDashboard = props => {
  const { invitations } = props;
  return (
    <div>
      <Topbar>
        <Text classText="header-title" weight="med2" color="black">
          Notifications
        </Text>
      </Topbar>
      <Tabs invitations={invitations} />
    </div>
  );
};

export default NotificationDashboard;
