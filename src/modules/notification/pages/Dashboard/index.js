import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { LoadComponent, RefreshComponent } from 'HOCS';
import { loadInvitations, refresh } from '../../store/action';
import Dashboard from './Dashboard';

const mapStateToProps = state => {
  const { notification } = state;

  return {
    loading: notification.loading,
    loaded: notification.loaded,
    invitations: notification.invitations
  };
};

export default compose(
  connect(
    mapStateToProps,
    { load: loadInvitations, refresh }
  ),
  LoadComponent,
  RefreshComponent,
  withRouter
)(Dashboard);
