import React, { Component } from 'react';
import { Tabs, Table } from 'antd';
import { Text } from 'components';
import Column from './InvitationColumn';
import './Tabs.scss';

const TabPane = Tabs.TabPane;

class WrappedTabs extends Component {
  callback = key => {
    console.log(key);
  };

  render() {
    const { invitations, loading } = this.props;

    return (
      <Tabs
        className="notification-tab-container"
        defaultActiveKey="1"
        onChange={this.callback}
      >
        <TabPane tab="H2H Invitations" key="1">
          <section className="invitation-h2h-table">
            <div className="table-title-container">
              <Text classText="iv-table-title" weight="med">
                Private Challenges
              </Text>
            </div>
            <div>
              <Table
                loading={loading}
                rowKey={record => record._id}
                dataSource={invitations}
                columns={Column}
                locale={{ emptyText: 'No Invitations' }}
              />
            </div>
          </section>
        </TabPane>
      </Tabs>
    );
  }
}

export default WrappedTabs;
