import React from 'react';
import { Link } from 'react-router-dom';
import { Text, Icon } from 'components';
import './InvitationColumn.scss';

const { ICON_VIEW } = Icon;

export default [
  {
    title: '',
    align: 'left',
    dataIndex: 'title',
    className: 'jc-col',
    render: (text, record) => (
      <Text classText="jc-title" color="black">
        {record.invitedContender.name}
      </Text>
    )
  },
  {
    title: <Text classText="jc-col-header">Game Duration</Text>,
    align: 'left',
    dataIndex: 'start',
    className: 'jc-col gw',
    render: (text, record) => (
      <span>
        <Text classText="jc-value-text gw">{`Gameweek ${record.start} - ${
          record.end
        }`}</Text>
        <Text classText="jc-value-text-mobile">{`GW ${record.start} - ${
          record.end
        }`}</Text>
      </span>
    )
  },
  {
    title: <Text classText="jc-col-header">Fee</Text>,
    align: 'right',
    dataIndex: 'fee',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.fee || 0}</Text>
  },
  {
    title: <Text classText="jc-col-header">Prize</Text>,
    align: 'right',
    dataIndex: 'prize',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.prize || 0}</Text>
  },
  {
    title: <Text classText="jc-col-header">View</Text>,
    align: 'center',
    dataIndex: 'control',
    className: 'jc-col',
    render: (t, r) => {
      return (
        <div className="ctrl-container">
          <Link to={`/h2h/${r._id}`}>
            <Icon iconType={ICON_VIEW} />
          </Link>
        </div>
      );
    }
  }
];
