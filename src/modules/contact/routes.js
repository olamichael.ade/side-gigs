import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/contact-us',
    exact: true,
    auth: false,
    component: Loadable({
      loader: () => import(/* webpackChunkName: "contactUs" */ './index'),
      loading: Loader,
      modules: ['contactUs']
    })
  }
];

export default routes;
