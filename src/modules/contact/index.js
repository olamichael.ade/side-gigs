import { connect } from 'react-redux';
import { contactUs } from './store/actions';
import Contact from './component/Contact';

const mapStateToProps = state => {
  const { contact } = state;
  return {
    loading: contact.loading,
    error: contact.error,
    success: contact.success,
    errorMessage: contact.errorMessage
  };
};

export default connect(
  mapStateToProps,
  { contactUs }
)(Contact);
