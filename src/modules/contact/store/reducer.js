import { IS_SENDING, IS_COMPLETE, IS_FAILED } from './actionTypes';

const initialState = {
  loading: false,
  error: false,
  success: false,
  message: false
};

const contactReducer = (state = initialState, action) => {
  switch (action.type) {
    case IS_SENDING:
      return { ...state, loading: true };
    case IS_COMPLETE:
      return {
        ...state,
        loading: false,
        success: true,
        message: action.message
      };
    case IS_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.message
      };
    default:
      return { ...state };
  }
};

export default contactReducer;
