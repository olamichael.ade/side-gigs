import Api from 'utils/Api';
import { IS_SENDING, IS_COMPLETE, IS_FAILED } from './actionTypes';

function contactUs(data) {
  return dispatch => {
    dispatch({ type: IS_SENDING });
    Api.sendMessage(data)
      .then(res => {
        dispatch({
          type: IS_COMPLETE,
          message: 'Thank you for contacting us!'
        });
      })
      .catch(err => {
        console.log('Could Not send message: ', JSON.stringify(err));
        dispatch({ type: IS_FAILED, message: 'Could not send message' });
      });
  };
}

export { contactUs };
