import React from 'react';
import PropTypes from 'prop-types';
import { FacebookIcon, TwitterIcon, WhatsappIcon } from 'react-share';
import { Text, Icon } from 'components';
import ContactForm from './ContactForm';

import './index.scss';

const { OK_ICON } = Icon;

const SuccessPage = (
  <div className="contact-completed-container">
    <div className="container-body">
      <div className="icon-container">
        <Icon iconType={OK_ICON} iconClass="ok-icon" />
      </div>
      <div className="completed-text-container">
        <Text color="black" classText="success-text">
          Thank you for contacting us. We'll get back to you shortly
        </Text>
      </div>
    </div>
  </div>
);

const Contact = props => {
  const { contactUs, loading, success } = props;

  return (
    <div className="contact-us-container">
      <div className="content">
        <div className="title-container">
          <Text color="white" classText="page-header">
            How Can We Help You?
          </Text>
        </div>
        <div className="box">
          {success ? (
            SuccessPage
          ) : (
            <ContactForm
              loading={loading}
              onSubmit={message => contactUs(message)}
            />
          )}
        </div>
        <div className="comms-container">
          <p className="comms-text">OR</p>
          <div className="comms-icon-container">
            <a
              className="share-icon"
              href="https://twitter.com/fansfantasylg"
              target="__blank"
            >
              <TwitterIcon size={32} round />
            </a>
            <a
              className="share-icon"
              href="https://web.facebook.com/fansfantasyleague/"
              target="__blank"
            >
              <FacebookIcon size={32} round />
            </a>
            <a
              className="share-icon"
              href=" https://chat.whatsapp.com/EzODJJRu0IZFQeiikiiDjk"
              target="__blank"
            >
              <WhatsappIcon size={32} round />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

Contact.propTypes = {
  contactUs: PropTypes.func,
  loading: PropTypes.bool,
  error: PropTypes.bool,
  errorMessage: PropTypes.string
};

export default Contact;
