import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Input } from 'antd';
import { Button, Text } from 'components';
import InputComponent from 'components/Input';
import { errors } from 'utils/validators';
import './index.scss';

const FormInput = InputComponent.form;
const { TextArea } = Input;

const WrappedContactForm = props => {
  const { onSubmit, loading, handleSubmit, invalid } = props;

  const inputLayout = {
    marginBottom: 20
  };

  const renderTextArea = ({ input: { onChange } }) => {
    return <TextArea placeholder="Message" rows={4} onChange={onChange} />;
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="contact-form">
      <div className="form-input-container">
        <label>
          <Text color="black">Your Email</Text>
        </label>
        <Field
          className="form-input"
          name="email"
          placeholder="Enter Your Email"
          component={FormInput}
          formLayout={inputLayout}
          validate={[errors.emailRequired, errors.isEmail]}
        />
      </div>
      <div className="form-input-container">
        <label>
          <Text color="black">Subject Line</Text>
        </label>
        <Field
          className="form-input"
          name="subject"
          placeholder="Subject"
          component={FormInput}
          formLayout={inputLayout}
          validate={errors.isRequired}
        />
      </div>
      <div className="form-input-container">
        <label>
          <Text color="black">Message</Text>
        </label>
        <Field
          className="form-input"
          name="message"
          component={renderTextArea}
          formLayout={inputLayout}
        />
      </div>
      <div className="contact-btn-container">
        <Button
          color="purple"
          loading={loading}
          className="contact-form-button"
          disabled={invalid}
          htmlType="submit"
        >
          <Text color="white">Send Message</Text>
        </Button>
      </div>
    </form>
  );
};

WrappedContactForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

export default reduxForm({ form: 'CONTACT_US_FORM' })(WrappedContactForm);
