import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/profile',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import(/* webpackChunkName: "profile" */ './pages/Profile'),
      loading: Loader,
      modules: ['profile']
    })
  }
];

export default routes;
