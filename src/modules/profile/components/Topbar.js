import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'components';
import './Topbar.scss';

const Topbar = props => {
  const { title } = props;

  return (
    <div className="topbar-container">
      <Text classText="top-title" weight="med2" color="black">
        {title}
      </Text>
    </div>
  );
};

Topbar.propTypes = {
  title: PropTypes.string,
  rightComponent: PropTypes.node
};

export default Topbar;
