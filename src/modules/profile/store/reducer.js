import {
  REFRESH,
  UPDATE_USER_INFO,
  UPDATE_USER_INFO_SUCCESS,
  UPDATE_USER_INFO_FAILURE,
  UPDATE_USER_PASSWORD,
  UPDATE_USER_PASSWORD_SUCCESS,
  UPDATE_USER_PASSWORD_FAILURE,
  UNKNOWN_ACTION,
  PASSWORD_MISMATCH,
  CLEAR_MESSAGE
} from './actionTypes';

const initialState = {
  error: false,
  success: false,
  errorMessage: '',
  isUpdating: false,
  message: '',
  isUpdated: false
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case REFRESH: {
      return { ...initialState };
    }
    case UPDATE_USER_INFO:
    case UPDATE_USER_PASSWORD:
      return { ...state, isUpdating: true };

    case UPDATE_USER_INFO_SUCCESS:
    case UPDATE_USER_PASSWORD_SUCCESS: {
      const { message } = action;
      return {
        ...state,
        message,
        isUpdating: false,
        isUpdated: true,
        error: false,
        success: true
      };
    }
    case UPDATE_USER_INFO_FAILURE:
    case UPDATE_USER_PASSWORD_FAILURE: {
      const { message } = action;
      return {
        ...state,
        message,
        isUpdated: false,
        isUpdating: false,
        error: true
      };
    }
    case UNKNOWN_ACTION:
      return { ...state, message: 'Unknown request' };
    case PASSWORD_MISMATCH:
      return { ...state, message: "Password don't match", error: true };
    case CLEAR_MESSAGE:
      return { ...state, message: '', error: false, success: false };
    default:
      return state;
  }
}

export default reducer;
