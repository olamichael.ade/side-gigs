import Api from 'utils/Api';
import {
  REFRESH,
  UPDATE_USER_INFO,
  UPDATE_USER_INFO_FAILURE,
  UPDATE_USER_INFO_SUCCESS,
  UNKNOWN_ACTION,
  PASSWORD_MISMATCH,
  CLEAR_MESSAGE
} from './actionTypes';
import { updateUser } from 'modules/auth/store/actions';

function refresh() {
  return { type: REFRESH };
}

function updateInfo(updateType, creds) {
  let method;
  return (dispatch, getState) => {
    const {
      auth: { user }
    } = getState();

    if (updateType === 'password') {
      method = 'updatePassword';
      creds.email = user.email;
    } else if (updateType === 'info') method = 'updateInfo';
    else dispatch({ type: UNKNOWN_ACTION });

    dispatch({ type: UPDATE_USER_INFO });

    Api[method](creds)
      .then(response => {
        dispatch({
          type: UPDATE_USER_INFO_SUCCESS,
          data: response,
          message: 'Successfully updated'
        });
        dispatch(updateUser());
      })
      .catch(err => {
        dispatch({ type: UPDATE_USER_INFO_FAILURE, message: err.body.message });
      });
  };
}

function clearMessage() {
  return dispatch => dispatch({ type: CLEAR_MESSAGE });
}

function passwordMisMatch() {
  return dispatch => dispatch({ type: PASSWORD_MISMATCH });
}
export { updateInfo, refresh, passwordMisMatch, clearMessage };
