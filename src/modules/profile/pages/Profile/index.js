import { compose } from 'redux';
import { connect } from 'react-redux';
import { RefreshComponent } from 'HOCS';
import {
  refresh,
  updateInfo,
  passwordMisMatch,
  clearMessage
} from '../../store/actions';
import Profile from './profile';

const mapStateToProps = state => {
  const { auth, profile } = state;
  return {
    editableUserInfo: {
      name: auth && auth.user && auth.user.name,
      email: auth && auth.user && auth.user.email,
      phone: auth && auth.user && auth.user.phone
    },
    isUpdating: profile.isUpdating,
    error: profile.error,
    message: profile.message,
    isUpdated: profile.isUpdated,
    success: profile.success
  };
};

export default compose(
  connect(
    mapStateToProps,
    { refresh, updateInfo, passwordMisMatch, clearMessage }
  ),
  RefreshComponent
)(Profile);
