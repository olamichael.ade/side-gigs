import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
import Input from 'components/Input';
import { errors } from 'utils/validators';
import { PASSWORD_FORM } from '../../../constants';

import './profile-settings.scss';

const FormInput = Input.form;

const WrappedPasswordForm = props => {
  const { invalid, handleSubmit, onSubmit, isUpdating } = props;
  const inputLayout = {
    marginBottom: 20
  };

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="login-form password-form"
    >
      <div className="form-heading">Edit Password</div>
      <div className="wrapper form-row">
        <label className="text-label">
          <Text color="black">Current Password</Text>
        </label>
        <Field
          className="input-field"
          name="oldPassword"
          placeholder="old password"
          type="password"
          component={FormInput}
          formLayout={inputLayout}
          validate={[errors.isRequired]}
        />
      </div>
      <div className="wrapper form-row">
        <label className="text-label ">
          <Text color="black">New Password</Text>
        </label>
        <Field
          className="input-field"
          name="newPassword"
          placeholder="enter your new password"
          type="password"
          component={FormInput}
          formLayout={inputLayout}
          validate={[errors.isRequired]}
        />
      </div>
      <div className="wrapper form-row">
        <label className="text-label ">
          <Text color="black">Confirm Password</Text>
        </label>
        <Field
          className="input-field"
          name="confirmPassword"
          placeholder="re-enter your new password"
          type="password"
          component={FormInput}
          formLayout={inputLayout}
        />
      </div>
      <div className="auth-btn-container">
        <Button
          color="purple"
          className="form-btn"
          disabled={invalid}
          htmlType="submit"
          size="large"
        >
          <Text classText="btn-text" color="white">
            {isUpdating ? 'Updating...' : 'UPDATE'}
          </Text>
        </Button>
      </div>
    </form>
  );
};

WrappedPasswordForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

export const PasswordForm = reduxForm({
  form: PASSWORD_FORM,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(WrappedPasswordForm);
