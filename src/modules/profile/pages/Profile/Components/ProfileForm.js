import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
import Input from 'components/Input';

import { errors } from 'utils/validators';

import { PROFILE_FORM } from 'modules/profile/constants';
import './profile-settings.scss';

const FormInput = Input.form;

const WrappedProfileForm = props => {
  const { invalid, handleSubmit, onSubmit, isUpdating } = props;
  const inputLayout = {
    marginBottom: 20
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="login-form profile-form">
      <div className="form-heading">Profile Details</div>
      <div className="wrapper form-row">
        <label className="text-label">
          <Text color="black">Name</Text>
        </label>
        <Field
          className="input-field"
          name="name"
          placeholder="Name"
          type="text"
          component={FormInput}
          formLayout={inputLayout}
          validate={[errors.leagueNameRequired]}
        />
      </div>
      <div className="wrapper form-row">
        <label className="text-label ">
          <Text color="black">Email</Text>
        </label>
        <Field
          className="input-field"
          name="email"
          placeholder="Email"
          type="text"
          component={FormInput}
          formLayout={inputLayout}
        />
      </div>
      <div className="wrapper form-row">
        <label className="text-label ">
          <Text color="black">Phone Number</Text>
        </label>
        <Field
          className="input-field"
          name="phone"
          placeholder="Phone number"
          type="text"
          component={FormInput}
          formLayout={inputLayout}
          required
        />
      </div>
      <div className="auth-btn-container">
        <Button
          color="purple"
          className="form-btn"
          disabled={invalid}
          htmlType="submit"
          size="large"
        >
          <Text classText="btn-text" color="white">
            {isUpdating ? 'Updating...' : 'UPDATE'}
          </Text>
        </Button>
      </div>
    </form>
  );
};

WrappedProfileForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

export const ProfileForm = reduxForm({
  form: PROFILE_FORM,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(WrappedProfileForm);
