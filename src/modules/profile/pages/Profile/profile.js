import React, { Component } from 'react';
import { ProfileForm, PasswordForm } from './Components';
import Topbar from '../../components/Topbar';
import { Alert } from 'antd';
import './profile.scss';

class Profile extends Component {
  handleSubmit = items => {
    return this.props.updateInfo('info', items);
  };

  handlePasswordSubmit = passwordInfo => {
    if (passwordInfo.newPassword !== passwordInfo.confirmPassword) {
      return this.props.passwordMisMatch();
    }
    return this.props.updateInfo('password', passwordInfo);
  };

  render() {
    const {
      error,
      clearMessage,
      message,
      editableUserInfo,
      success
    } = this.props;
    return (
      <div className="main-body">
        <div className="create-league-wrapper">
          <Topbar title="Profile Settings" />
          {(error || success) && (
            <div>
              <Alert
                message={message}
                type={success ? 'success' : 'error'}
                className="profile-message-box"
                closable
                onClose={clearMessage}
              />
            </div>
          )}
          <ProfileForm
            initialValues={editableUserInfo}
            onSubmit={this.handleSubmit}
            {...this.props}
          />
          <PasswordForm onSubmit={this.handlePasswordSubmit} {...this.props} />
        </div>
      </div>
    );
  }
}

export default Profile;
