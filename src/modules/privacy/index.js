import React from 'react';
import { Text } from 'components';
import './index.scss';

const PrivacyPolicy = () => {
  return (
    <div className="policy-container">
      <div className="box">
        <h2>Privacy Policy</h2>
        <h5>Last Update Posted: August 27, 2018</h5>

        <section>
          <h4>1. Introduction</h4>
          <p>
            <Text weight="med">1.1 </Text>
            <span>
              FanLigue recognizes that people who use FanLigue’s Service value
              their privacy. This Privacy Policy details important information
              regarding the collection, use and disclosure of User information
              collected on FanLigue’s fantasy sports website located at
              www.fansfantasyleague.com (the "Site"), the related mobile app,
              and any other features, tools, materials, or other services
              (including co-branded or affiliated services) offered from time to
              time by FanLigue or its affiliate companies (the “Service”).
              FanLigue provides this Privacy Policy to help you make an informed
              decision about whether to use or continue using the Service.
            </span>
          </p>
          <p>
            <span>1.2 </span>
            <span>
              This Privacy Policy should be read in conjunction with our Terms
              of Use. By accessing the Service, you are consenting to the
              information collection and use practices described in this Privacy
              Policy.
            </span>
          </p>
          <p>
            <span>1.3 </span>
            <span>
              Your use of the Service and any information you provide through
              the Service remains subject to the terms of this Privacy Policy
              and our Terms of Use, as each may be updated from time to time.
            </span>
          </p>
          <p>
            <span>1.4 </span>
            <span>
              Any questions, comments or complaints that you might have should
              be emailed to fansfantasyleague@gmail.com.
            </span>
          </p>
        </section>
        <section>
          <h4>2. Information we collect</h4>
          <p>
            By using our Service and by submitting information to us through
            your use of our Service then this Privacy Policy will apply. You
            provide certain personal information to FanLigue when choosing to
            participate in the various activities on the Service such as
            registering for an account, participating in contests, posting
            messages, taking advantage of promotions, responding to surveys or
            subscribing to newsletters or other mailing lists. The personal
            information we collect from you generally may include:
          </p>
          <p>
            <Text weight="med">2.1.1 </Text>
            <Text>Your name</Text>
          </p>
          <p>
            <Text weight="med">2.1.2 </Text>
            <Text>Your Email address;</Text>
          </p>
          <p>
            <Text weight="med">2.1.3 </Text>
            <Text>Contacts you choose to submit;</Text>
          </p>
          <p>
            <Text weight="med">2.1.4 </Text>
            <Text>Your preferences;</Text>
          </p>
          <p>
            <Text weight="med">2.1.5 </Text>
            <Text>Response to surveys;</Text>
          </p>
          <p>
            <Text weight="med">2.1.6 </Text>
            <Text>Communications sent to us by you;</Text>
          </p>
          <p>
            <Text weight="med">2.1.7 </Text>
            <Text>
              your subscriptions to newsletters and services offered by us; and
            </Text>
          </p>
          <p>
            <Text weight="med">2.1.8 </Text>
            <Text>
              any other information you submit to FanLigue when choosing to
              participate in various activities on the Service.
            </Text>
          </p>
          <p>
            <span>2.2 </span>
            <span>
              In addition to the above, we may need to verify your identity in
              order for you to use some aspects of the Service. For purposes of
              verification, we may also collect the following personal
              information from you (for compliance reasons, provision of this
              information, when requested, is a mandatory condition of using our
              Service):
            </span>
          </p>
          <p>
            <span>2.2.1 </span>
            <span>other identification documents;</span>
          </p>
          <p>
            <span>2.2.2 </span>
            <span>
              other information as may be required to verify you in accordance
              with applicable laws and regulations.
            </span>
          </p>
          <p>
            <span>2.3 </span>
            <span>
              When you use our mobile app, we also may collect mobile device
              information like operating system and hardware type, numbers or
              codes that are unique to your particular device (such as IDFA or
              an Android Ad ID), device information, default device language,
              the location of your device (at a GPS level), and app usage
              information. This data also may be linked to your other
              information, including your location data.
            </span>
          </p>
          <p>
            <span>2.4 </span>
            <span>
              In addition, if you choose to log in, access or otherwise connect
              to FanLigue, or contact FanLigue, through a social networking
              service (such as Facebook), we may collect your user ID and user
              name associated with that social networking service, as well as
              any information you make public using that social networking
              service. We may also collect information you have authorized the
              social networking service to share with us (such as your user ID,
              public profile information, email address, birthday, friends list,
              and pages you have "liked")
            </span>
          </p>
          <p>
            <span>2.5 </span>
            <span>
              Cookies Information: When you visit the FanLigue.com website, we
              may send one or more cookies - small files - to your computer or
              other device, which may enable us or others to uniquely identify
              your browser. FanLigue uses both session cookies and persistent
              cookies. A persistent cookie remains after you close your browser.
              Persistent cookies may be used by your browser on subsequent
              visits to the site. Persistent cookies can be removed by following
              your web browser help file directions. Session cookies are
              temporary and typically disappear after you close your browser.
              You may be able to reset your web browser to refuse all cookies or
              to indicate when a cookie is being sent. However, some features of
              the Site or Service may not function properly if the ability to
              accept cookies is disabled.
            </span>
          </p>
          <p>
            <span>2.6 </span>
            <span>
              Log File Information: When you use our Service, our servers may
              automatically record certain information that your device sends
              whenever you visit any website and use certain apps. These server
              logs may include information such as your web or app request,
              Internet Protocol ("IP") address, browser type, browser language,
              referring/exit pages and URLs, platform type, number of clicks,
              domain names, landing pages, pages viewed and the order of those
              pages, the amount of time spent on particular pages, the date and
              time of your request, and one or more cookies that may uniquely
              identify your browser.
            </span>
          </p>
          <p>
            <span>2.7 </span>
            <span>
              Clear GIFs Information: When you use our Service, we may employ
              technology such as "clear GIFs" (a.k.a. Web Beacons) which are
              used to track the online usage patterns of our users. In addition,
              we may also use clear GIFs in HTML-based emails sent to our users
              to track which emails are opened by specific users.
            </span>
          </p>
          <p>
            <span>2.8 </span>
            <span>
              FanLigue may also collect information about you from third
              parties, such as marketing partners, identity verification
              services, anti-fraud services and other service providers.
            </span>
          </p>
        </section>
      </div>
    </div>
  );
};

export default PrivacyPolicy;
