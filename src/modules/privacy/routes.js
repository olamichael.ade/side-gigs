import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/privacy-policy',
    exact: true,
    auth: false,
    component: Loadable({
      loader: () => import(/* webpackChunkName: "privacyPolicy" */ './index'),
      loading: Loader,
      modules: ['privacyPolicy']
    })
  }
];

export default routes;
