import React, { Component } from 'react';
import T from 'prop-types';
import { Icon } from 'antd';
import { Text, Button, LinkedText } from 'components';
import Topbar from 'modules/wallet/components/Topbar';
import CardDetails from 'modules/wallet/components/CardDetails';
import AddCardForm from 'modules/wallet/components/AddCardForm';
import './Dashboard.scss';

class Dashboard extends Component {
  onDelete = id => {
    return this.props.deleteCard(id);
  };

  gotoPrevious = e => {
    e.preventDefault();

    this.props.history.goBack();
  };

  renderEmpty = () => {
    return (
      <div className="cd-container selected">You have no saved cards.</div>
    );
  };

  handleSuccess = () => {
    return this.props.history.push('/wallet');
  };

  renderCards = () => {
    const {
      cards,
      fundWalletFromCard,
      isProcessingPayment,
      selectedCard,
      selectCard,
      isDeleting,
      amount
    } = this.props;

    return (
      <div>
        {cards.map(card => (
          <CardDetails
            key={`card-${card._id}`}
            isSelected={selectedCard === card._id}
            {...card}
            onSelect={selectCard}
            onDelete={this.onDelete}
            isDeleting={isDeleting}
            hasAmount={Number(amount) > 0}
          />
        ))}
        <div className="card-button-container">
          <Button
            loading={isProcessingPayment}
            color="purple"
            className="form-btn"
            disabled={!selectedCard || isProcessingPayment}
            onClick={() => fundWalletFromCard(this.handleSuccess)}
          >
            {isProcessingPayment ? 'Processing...' : 'FUND WALLET'}
          </Button>
        </div>
      </div>
    );
  };

  renderBack = () => {
    return (
      <LinkedText to="/#" onClick={this.gotoPrevious}>
        <Icon type="arrow-left" />
        Back
      </LinkedText>
    );
  };

  render() {
    const { cards, fundWallet, isPaying } = this.props;
    return (
      <div className="wallet-container">
        <Topbar title="Fund Wallet" rightComponent={this.renderBack()} />
        {cards.length > 0 && (
          <div className="help-text-container">
            <Text className="help-text" color="black">
              Pay with one of your saved cards
            </Text>
          </div>
        )}
        <div className="fw-body">
          <div className="w-section w-card-list">
            {cards.length > 0 ? this.renderCards() : this.renderEmpty()}
          </div>
          <div className="w-section w-card-add">
            <div className="form-add-wrapper">
              <div className="form-title">
                <Text weight="med2" classText="form-title-text">
                  Add new card
                </Text>
              </div>
              <AddCardForm
                loading={isPaying}
                onSubmit={card => fundWallet(card, this.handleSuccess)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  isPaying: T.bool,
  cards: T.arrayOf(T.object),
  fundWallet: T.func
};

export default Dashboard;
