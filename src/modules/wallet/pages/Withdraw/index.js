import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { LoadComponent, RefreshComponent } from 'HOCS';
import {
  loadAccounts,
  refresh,
  selectAccount,
  deleteAccount,
  processPayout,
  payToAccount
} from '../../store/actions';
import Dashboard from './components/Dashboard';

const mapStateToProps = state => {
  const { auth, wallet } = state;

  return {
    user: auth.user,
    loading: wallet.loading,
    loaded: wallet.loaded,
    accounts: wallet.accounts,
    banks: wallet.banks,
    isPaying: wallet.isPaying,
    selectedAccount: wallet.selectedAccount,
    isDeleting: wallet.isDeleting,
    isProcessingPayment: wallet.isProcessingPayment,
    amount: wallet.amount
  };
};

export default compose(
  connect(
    mapStateToProps,
    {
      load: loadAccounts,
      refresh,
      selectAccount,
      deleteAccount,
      processPayout,
      payToAccount
    }
  ),
  LoadComponent,
  RefreshComponent,
  withRouter
)(Dashboard);
