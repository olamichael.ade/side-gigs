import React, { Component } from 'react';
import { Icon } from 'antd';
import { Text, Button, LinkedText } from 'components';
import Topbar from 'modules/wallet/components/Topbar';
import CardDetails from 'modules/wallet/components/CardDetails';
import AddAccountForm from 'modules/wallet/components/AddAccountForm';
import './Dashboard.scss';

class Dashboard extends Component {
  onDelete = id => {
    return this.props.deleteAccount(id);
  };

  gotoPrevious = e => {
    e.preventDefault();

    this.props.history.goBack();
  };

  handleSuccess = () => {
    return this.props.history.push('/wallet');
  };

  renderEmpty = () => {
    return (
      <div className="cd-container selected">You have no saved account(s).</div>
    );
  };

  renderCards = () => {
    const {
      accounts,
      isPaying,
      selectedAccount,
      selectAccount,
      isDeleting,
      payToAccount,
      isProcessingPayment,
      amount
    } = this.props;

    return (
      <div>
        {accounts.map(account => (
          <CardDetails
            type="account"
            key={`card-${account._id}`}
            isSelected={selectedAccount === account._id}
            {...account}
            onSelect={selectAccount}
            onDelete={this.onDelete}
            isDeleting={isDeleting}
            hasAmount={Number(amount) > 0}
          />
        ))}
        <div className="card-button-container">
          <Button
            loading={isProcessingPayment}
            color="purple"
            className="form-btn"
            disabled={!selectedAccount || isProcessingPayment}
            onClick={() => payToAccount(selectedAccount, this.handleSuccess)}
          >
            {isPaying ? 'Processing...' : 'WITHDRAW FUND'}
          </Button>
        </div>
      </div>
    );
  };

  renderBack = () => {
    return (
      <LinkedText to="/#" onClick={this.gotoPrevious}>
        <Icon type="arrow-left" />
        Back
      </LinkedText>
    );
  };

  render() {
    const { accounts, banks, processPayout, amount } = this.props;
    return (
      <div className="wallet-container">
        <Topbar title="Withdraw" rightComponent={this.renderBack()} />
        {accounts.length > 0 && (
          <div className="help-text-container">
            <Text className="help-text" color="black">
              Pay to one of your saved accounts
            </Text>
          </div>
        )}
        <div className="fw-body">
          <div className="w-section w-card-list">
            {accounts.length > 0 ? this.renderCards() : this.renderEmpty()}
          </div>
          <div className="w-section w-card-add">
            <div className="form-add-wrapper">
              <div className="form-title">
                <Text weight="med2" classText="form-title-text">
                  Add a new account
                </Text>
              </div>
              <AddAccountForm
                banks={banks}
                onSubmit={account => processPayout(account)}
                amount={amount}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
