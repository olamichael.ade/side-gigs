import React from 'react';
import { Icon } from 'antd';
import Text from 'components/Text';
import { parseDateTime } from 'utils/utils';

import './Column.scss';

const transactionTypes = {
  withdrawal: 'Withdrawal',
  top_up: 'Top Up',
  won_league: 'Won League',
  won_h2h: 'Won H2H',
  draw_h2h: 'Draw on H2H',
  create_h2h: 'Create H2H',
  accepted_h2h: 'Accepted H2H',
  join_league: 'Joined League',
  create_league: 'Create League'
};

export default [
  {
    title: 'Transaction Type',
    align: 'left',
    dataIndex: '_id',
    render: (text, record) => (
      <span className="tc-type">
        <Text weight="med2">{transactionTypes[record.type]}</Text>
        <Text>{parseDateTime(record.created_at)}</Text>
      </span>
    )
  },
  {
    title: 'Amount',
    align: 'right',
    dataIndex: 'date',
    rowKey: 'date',
    render: (t, r) => (
      <span>
        <Text classText={`${r.isCredit ? 'is-credit' : 'is-debit'}`}>{`${
          r.isCredit ? '+' : '-'
        }${r.currency} ${Number(r.amount).toLocaleString()}`}</Text>
        {r.state === 'pending' ? (
          <Icon type="exclamation-circle-o" className="col-icon pending" />
        ) : (
          <Icon type="check-circle-o" className="col-icon success" />
        )}
      </span>
    )
  }
];
