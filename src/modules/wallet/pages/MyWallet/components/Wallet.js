import React from 'react';
import { Table } from 'antd';
import { withRouter } from 'react-router';
import { Text } from 'components';
import Topbar from 'modules/wallet/components/Topbar';
import Card from 'modules/wallet/components/Card';
import { CardForm } from 'modules/wallet/components/CardForm';
import Column from './Column';
import { errors } from 'utils/validators';
import './Wallet.scss';

const Wallet = props => {
  const { transactions, setAmount, history, user } = props;

  const { currency, wallet = 0 } = user || {};

  const userBalance = (
    <Text classText="rc-label-value">
      <Text classText="rc-label-web">Available Balance: </Text>
      <Text classText="rc-label-mobile">Av Balance: </Text>
      <Text classText="rc-value">{`${currency} ${wallet.toLocaleString()}`}</Text>
    </Text>
  );

  const proceedToPayment = type => history.push(`/wallet/${type}`);
  const validators = errors.minAmountRequired(wallet);
  return (
    <div className="wallet-container">
      <Topbar title="My Wallet" rightComponent={userBalance} />
      <div className="wallet-body">
        <div className="w-card-container">
          <Card
            title="Fund Wallet"
            subTitle="Choose the amount you would like to fund your wallet with."
          >
            <CardForm
              validator={errors.amountRequired}
              form="FUND_WALLET_FORM"
              onSubmit={amount => {
                setAmount(amount);
                return proceedToPayment('fund');
              }}
              buttonLabel="FUND WALLET"
            />
          </Card>

          <Card
            title="Withdraw funds"
            subTitle="Withdraw funds from your wallet into your primary bank account."
          >
            <CardForm
              validator={[validators, errors.amountRequired]}
              form="WITHDRAWAL_FORM"
              onSubmit={amount => {
                setAmount(amount);
                return proceedToPayment('withdraw');
              }}
              buttonLabel="WITHDRAW FUND"
            />
          </Card>
        </div>
        <div className="w-table-container">
          <Table
            rowKey={record => record._id}
            dataSource={transactions}
            columns={Column}
            locale={{ emptyText: 'No Transactions' }}
          />
        </div>
      </div>
    </div>
  );
};

export default withRouter(Wallet);
