import { compose } from 'redux';
import { connect } from 'react-redux';
import { LoadComponent, RefreshComponent } from 'HOCS';
import { load, setAmount, refresh } from '../../store/actions';
import Wallet from './components/Wallet';

const mapStateToProps = state => {
  const { auth, wallet } = state;

  return {
    user: auth.user,
    loading: wallet.loading,
    loaded: wallet.loaded,
    transactions: wallet.transactions
  };
};

export default compose(
  connect(
    mapStateToProps,
    { load, setAmount, refresh }
  ),
  LoadComponent,
  RefreshComponent
)(Wallet);
