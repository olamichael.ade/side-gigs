import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Modal } from 'antd';
import Text from 'components/Text';

import { cardTypes } from 'utils/constants';
import { pad } from 'utils/utils';

import './CardDetails.scss';

const confirm = Modal.confirm;
const modalStyle = {
  top: 40
};

const maskAccount = number => {
  const maskedNumber = [
    number.substr(0, 4),
    number.substring(4, 10).replace(/\w/gi, '*')
  ];

  return maskedNumber.join('');
};

const CardDetails = props => {
  const {
    _id,
    name,
    type,
    number,
    onSelect,
    onDelete,
    isSelected,
    isDeleting,
    bank,
    hasAmount
  } = props;

  const isAccount = type === 'account';

  function showConfirm(id) {
    confirm({
      title: `Do you want to delete this ${isAccount ? 'account' : 'card'}?`,
      style: { modalStyle },
      confirmLoading: isDeleting,
      onOk() {
        return onDelete(_id);
      },
      okText: 'YES, DELETE',
      onCancel() {}
    });
  }

  const cardType = cardTypes[type] || type;

  const maskNumber = () => {
    if (number) {
      return type !== 'account' ? pad(number, 16, '*') : maskAccount(number);
    }

    return '';
  };
  console.log(bank);

  const isMasterCard = cardType === 'Mastercard';

  const selector = isMasterCard || isAccount ? onSelect : () => {};

  return (
    <div
      className={`cd-container ${isSelected ? 'selected' : ''} ${
        hasAmount ? 'active' : ''
      }`}
      onClick={() => selector(_id)}
    >
      <div className="cd-content">
        <Text color="black" classText="cd-value">
          {name}
        </Text>
        <Text color="black" classText="cd-value">
          {maskNumber()}
        </Text>
        <Text>{`${type === 'account' ? bank : cardType}`}</Text>
      </div>
      <div className="card-control">
        {_id && (
          <div onClick={() => showConfirm()}>
            <Icon type="close" style={{ fontSize: 25, color: '#848484' }} />
          </div>
        )}
      </div>
    </div>
  );
};

CardDetails.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  type: PropTypes.string,
  number: PropTypes.string,
  onSelect: PropTypes.func,
  onDelete: PropTypes.func,
  isSelected: PropTypes.bool
};

export default CardDetails;
