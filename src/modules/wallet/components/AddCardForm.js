import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
import Input from 'components/Input';

import { credtCardNormalizer, ccParser } from 'utils/utils';

import './AddCardForm.scss';

const FormInput = Input.form;

const WrappedAddForm = props => {
  const { onSubmit, handleSubmit, invalid, loading } = props;

  const inputLayout = {
    marginBottom: 20
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="">
      <div className="labelled-input">
        <label>
          <Text color="black">Name on Card</Text>
        </label>
        <div>
          <Field
            required
            className="field-input"
            name="name"
            type="text"
            component={FormInput}
            formLayout={inputLayout}
          />
        </div>
      </div>
      <div className="labelled-input">
        <label>
          <Text color="black">Card number</Text>
        </label>
        <div>
          <Field
            required
            className="field-input"
            name="number"
            type="tel"
            maxLength={24}
            placeholder="xxxx xxxx xxxx xxxx xxxx"
            component={FormInput}
            normalize={credtCardNormalizer}
            formLayout={inputLayout}
            parse={ccParser}
          />
        </div>
      </div>
      <div className="remember_me">
        <label htmlFor="rememberMe">
          <Text color="black">Save Card</Text>
        </label>
        <div>
          <Field
            name="rememberMe"
            id="remember_me"
            component="input"
            type="checkbox"
          />
        </div>
      </div>
      <div className="card-form-btn">
        <Button
          loading={loading}
          color="purple"
          className="form-btn"
          disabled={invalid}
          htmlType="submit"
        >
          <Text classText="btn-text" color="white">
            ADD & PAY
          </Text>
        </Button>
      </div>
    </form>
  );
};

WrappedAddForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

export default reduxForm({ form: 'ADD_CARD_FORM' })(WrappedAddForm);
