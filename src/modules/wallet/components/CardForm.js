import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
import Input from 'components/Input';

import './CardForm.scss';

const FormInput = Input.form;

const WrappedCardForm = props => {
  const { onSubmit, handleSubmit, invalid, buttonLabel, validator } = props;

  const inputLayout = {
    marginBottom: 20
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="card-form">
      <Field
        className="form-input"
        name="amount"
        placeholder="Amount"
        type="number"
        component={FormInput}
        formLayout={inputLayout}
        validate={validator}
        warn={validator}
      />
      <div className="card-form-btn">
        <Button
          color="purple"
          className="wallet-btn"
          disabled={invalid}
          htmlType="submit"
        >
          <Text classText="btn-text" color="white">
            {buttonLabel}
          </Text>
        </Button>
      </div>
    </form>
  );
};

WrappedCardForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

export const CardForm = reduxForm({ form: 'CARD_FORM' })(WrappedCardForm);
