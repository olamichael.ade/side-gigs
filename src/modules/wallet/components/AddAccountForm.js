import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
import Input from 'components/Input';

import './AddCardForm.scss';

const FormInput = Input.form;

const Option = Select.Option;

const WrappedAddForm = props => {
  const { onSubmit, handleSubmit, invalid, banks = [], amount } = props;

  const inputLayout = {
    marginBottom: 20
  };

  const renderSelectField = ({ input: { onChange } }) => {
    return (
      <Select
        showSearch
        style={{ width: '100%' }}
        placeholder="Select a bank"
        optionFilterProp="children"
        onChange={onChange}
        defaultValue=""
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
      >
        <Option value="">Select bank</Option>
        {banks.map(bank => (
          <Option key={`bank-${bank.code}`} value={bank.code}>
            {bank.name}
          </Option>
        ))}
      </Select>
    );
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="">
      {amount !== 0 ? (
        <div className="labelled-input read-only">
          <div>
            <label>
              <Text color="black" classText="amount-label">
                Amount:
              </Text>
            </label>
            <Text>NGN {(Number(amount) + 45).toLocaleString()}</Text>
          </div>
          <Text size="small2">
            *The total amount charged includes a NGN 45 processing fee
          </Text>
        </div>
      ) : (
        <div className="labelled-input">
          <label>
            <Text color="black">Amount</Text>
          </label>
          <div>
            <Field
              required
              className="field-input"
              name="amount"
              type="number"
              component={FormInput}
              formLayout={{ marginBottom: 0 }}
            />
          </div>
          <div className="hint-component">
            <Text size="small2">
              *The total amount charged will include a NGN 45 processing fee
            </Text>
          </div>
        </div>
      )}
      <div className="labelled-input">
        <label>
          <Text color="black">Account Name</Text>
        </label>
        <div>
          <Field
            required
            className="field-input"
            name="accountName"
            type="text"
            component={FormInput}
            formLayout={inputLayout}
          />
        </div>
      </div>
      <div className="labelled-input">
        <label>
          <Text color="black">Account Number</Text>
        </label>
        <div>
          <Field
            required
            className="field-input"
            name="accountNumber"
            type="text"
            maxLength={10}
            component={FormInput}
            formLayout={inputLayout}
          />
        </div>
      </div>
      <div className="labelled-input">
        <label>
          <Text color="black">Bank</Text>
        </label>
        <div>
          <Field
            required
            className="field-input"
            name="code"
            component={renderSelectField}
            formLayout={inputLayout}
          />
        </div>
      </div>
      <div className="remember_me">
        <label htmlFor="rememberMe">
          <Text color="black">Save Account</Text>
        </label>
        <div>
          <Field
            name="rememberMe"
            id="remember_me"
            component="input"
            type="checkbox"
          />
        </div>
      </div>
      <div className="card-form-btn">
        <Button
          color="purple"
          className="form-btn"
          disabled={invalid}
          htmlType="submit"
        >
          <Text classText="btn-text" color="white">
            WITHDRAW
          </Text>
        </Button>
      </div>
    </form>
  );
};

WrappedAddForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string,
  amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default reduxForm({ form: 'ADD_ACCOUNT_FORM' })(WrappedAddForm);
