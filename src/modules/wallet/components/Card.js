import React from 'react';
import PropTypes from 'prop-types';
import Text from 'components/Text';

import './Card.scss';

const Card = props => {
  const { title, subTitle, children } = props;
  return (
    <div className="card-container">
      <Text classText="card-title" weight="med2">
        {title}
      </Text>
      <Text classText="card-subtitle" color="black">
        {subTitle}
      </Text>
      {children}
    </div>
  );
};

Card.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  children: PropTypes.node
};

export default Card;
