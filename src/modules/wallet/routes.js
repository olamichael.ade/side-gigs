import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/wallet',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "myWallet" */ './pages/MyWallet'),
      loading: Loader,
      modules: ['myWallet']
    })
  },
  {
    path: '/wallet/fund',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "fundWallet" */ './pages/FundWallet'),
      loading: Loader,
      modules: ['fundWallet']
    })
  },
  {
    path: '/wallet/withdraw',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "withdrawFunds" */ './pages/Withdraw'),
      loading: Loader,
      modules: ['withdrawFunds']
    })
  }
];

export default routes;
