import {
  REQUEST,
  REQUEST_FAILED,
  REQUEST_SUCCESS,
  LOAD_TRANSACTIONS,
  LOAD_USER_CARDS,
  SET_AMOUNT,
  REFRESH,
  SELECT_CARD,
  CARD_SAVED_SUCCESS,
  DELETE_CARD,
  DELETE_CARD_REQUEST,
  LOAD_BANKS,
  SELECT_ACCOUNT,
  DELETE_ACCOUNT_REQUEST,
  LOAD_USER_ACCOUNTS,
  DELETE_ACCOUNT,
  WALLET_TOPUP,
  WALLET_TOPUP_CANCELLED,
  WALLET_TOPUP_SUCCESS,
  WALLET_TOPUP_FROM_SAVED_CARD,
  WALLET_TOPUP_FROM_SAVED_ACC,
  WALLET_TOPUP_FAILED,
  UPDATE_USER_ACCOUNTS
} from './actionTypes';

const initialState = {
  loading: false,
  loaded: false,
  error: false,
  errorMessage: null,
  isProcessing: false,
  loadingTransactions: false,
  amount: 0,
  transactions: [],
  cards: [],
  selectedCard: null,
  isPaying: false,
  isDeleting: false,
  accounts: [],
  selectedAccount: null,
  isProcessingPayment: false
};

function isRequest(state) {
  return {
    ...state,
    loading: true
  };
}

function isRequestSuccess(state) {
  return {
    ...state,
    loading: false,
    loaded: true
  };
}

function isRequestFailed(state, payload) {
  return {
    ...state,
    loading: false,
    error: true,
    errorMessage: payload.message
  };
}

/**
 *
 * @param {*} state state objet
 * @param {*} key string
 * @param {*} payload { [key]: value }
 */
function updateState(state, payload) {
  return {
    ...state,
    ...payload
  };
}

function refresh(state) {
  return {
    ...state,
    loading: false,
    loaded: false
  };
}

function deleteCard(state, cardId) {
  const cards = state.cards.filter(card => card._id !== cardId);

  return {
    ...state,
    cards,
    isDeleting: false
  };
}

function deleteAccount(state, accountId) {
  const accounts = state.accounts.filter(account => account._id !== accountId);

  return {
    ...state,
    accounts,
    isDeleting: false
  };
}

function filterNigerianBanks(banks) {
  return banks.filter(bank => bank.country === 'NG');
}

function walletReducer(state = initialState, action) {
  switch (action.type) {
    case REFRESH:
      return refresh(state);
    case REQUEST:
      return isRequest(state);
    case REQUEST_SUCCESS:
      return isRequestSuccess(state);
    case REQUEST_FAILED:
      return isRequestFailed(state, action.payload);
    case LOAD_TRANSACTIONS:
    case LOAD_USER_CARDS:
    case SET_AMOUNT:
    case SELECT_CARD:
    case SELECT_ACCOUNT:
    case LOAD_USER_ACCOUNTS:
      return updateState(state, action.payload);
    case CARD_SAVED_SUCCESS:
      const { cards } = state;
      const { card } = action;
      return {
        ...state,
        cards: [...cards, card]
      };
    case DELETE_CARD:
      return deleteCard(state, action.cardId);
    case DELETE_ACCOUNT:
      return deleteAccount(state, action.accountId);
    case DELETE_ACCOUNT_REQUEST:
    case DELETE_CARD_REQUEST:
      return { ...state, isDeleting: true };
    case LOAD_BANKS:
      const { banks } = action;
      return { ...state, banks: filterNigerianBanks(banks) };
    case WALLET_TOPUP:
      return { ...state, isPaying: true };
    case WALLET_TOPUP_SUCCESS:
    case WALLET_TOPUP_CANCELLED:
    case WALLET_TOPUP_FAILED:
      return { ...state, isPaying: false, isProcessingPayment: false };
    case WALLET_TOPUP_FROM_SAVED_CARD:
    case WALLET_TOPUP_FROM_SAVED_ACC:
      return { ...state, isProcessingPayment: true };
    case UPDATE_USER_ACCOUNTS:
      const { account } = action;
      const { accounts } = state;
      return { ...state, accounts: [...accounts, account] };
    default:
      return { ...state };
  }
}

export default walletReducer;
