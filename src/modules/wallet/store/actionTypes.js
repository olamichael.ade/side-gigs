export const REQUEST = 'wallet/REQUEST';
export const REQUEST_FAILED = 'wallet/REQUEST_FAILED';
export const REQUEST_SUCCESS = 'wallet/REQUEST_SUCCESS';

export const LOAD_TRANSACTIONS = 'wallet/LOAD_TRANSACTIONS';

export const LOAD_USER_CARDS = 'wallet/LOAD_USER_CARDS';

export const SET_AMOUNT = 'wallet/SET_AMOUNT';

export const REFRESH = 'wallet/REFRESH';

export const WALLET_TOPUP = 'wallet/WALLET_TOPUP';
export const WALLET_TOPUP_CANCELLED = 'wallet/WALLET_TOPUP_CANCELLED';
export const WALLET_TOPUP_SUCCESS = 'wallet/WALLET_TOPUP_SUCCESS';
export const WALLET_TOPUP_VERIFY = 'wallet/WALLET_TOPUP_VERIFY';
export const WALLET_TOPUP_FAILED = 'wallet/WALLET_TOPUP_FAILED';
export const WALLET_TOPUP_FROM_SAVED_CARD =
  'wallet/WALLET_TOPUP_FROM_SAVED_CARD';
export const WALLET_TOPUP_FROM_SAVED_ACC = 'wallet/WALLET_TOPUP_FROM_SAVED_ACC';

export const CARD_SAVED_SUCCESS = 'wallet/CARD_SAVED_SUCCESS';
export const ACCOUNT_SAVED_SUCCESS = 'wallet/ACCOUNT_SAVED_SUCCESS';

export const DELETE_CARD_REQUEST = 'wallet/DELETE_CARD_REQUEST';
export const DELETE_CARD = 'wallet/DELETE_CARD';

export const SELECT_CARD = 'wallet/SELECT_CARD';
export const SELECT_ACCOUNT = 'wallet/SELECT_ACCOUNT';

export const LOAD_BANKS = 'wallet/LOAD_BANKS';

export const LOAD_USER_ACCOUNTS = 'wallet/LOAD_USER_ACCOUNTS';
export const UPDATE_USER_ACCOUNTS = 'wallet/UPDATE_USER_ACCOUNTS';

export const DELETE_ACCOUNT_REQUEST = 'wallet/DELETE_ACCOUNT_REQUEST';
export const DELETE_ACCOUNT = 'wallet/DELETE_ACCOUNT';
