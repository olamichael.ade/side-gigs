import { createSelector } from 'reselect';
import { notification } from 'antd';
import { find } from 'lodash';
import {
  LOAD_TRANSACTIONS,
  LOAD_USER_CARDS,
  REQUEST,
  SET_AMOUNT,
  REQUEST_SUCCESS,
  REQUEST_FAILED,
  REFRESH,
  WALLET_TOPUP_CANCELLED,
  WALLET_TOPUP_SUCCESS,
  WALLET_TOPUP_VERIFY,
  WALLET_TOPUP_FAILED,
  CARD_SAVED_SUCCESS,
  DELETE_CARD,
  DELETE_CARD_REQUEST,
  SELECT_CARD,
  LOAD_BANKS,
  SELECT_ACCOUNT,
  DELETE_ACCOUNT,
  DELETE_ACCOUNT_REQUEST,
  LOAD_USER_ACCOUNTS,
  WALLET_TOPUP,
  WALLET_TOPUP_FROM_SAVED_CARD,
  WALLET_TOPUP_FROM_SAVED_ACC,
  UPDATE_USER_ACCOUNTS
} from './actionTypes';
import { loadUser } from 'modules/auth/store/actions';
import { PBFKEY, SECKEY } from 'utils/constants';
import Api from 'utils/Api';

const getpaidSetup = window.getpaidSetup;

const selAuthUser = state => state.auth || {};
const selWallet = state => state.wallet || {};

const statusToTitle = {
  success: 'Success',
  error: 'Error'
};

const selSavedCardPayload = createSelector(
  [selWallet, selAuthUser],
  (wallet, auth) => {
    const { user } = auth;
    const selectedCard = find(wallet.cards, { _id: wallet.selectedCard });

    return {
      currency: user.currency || 'NGN',
      SECKEY,
      token: selectedCard.token,
      country: 'NG',
      amount: wallet.amount,
      email: user.email,
      firstname: user.name,
      txRef: `FFL-${Date.now()}`,
      narration: 'Wallet top up'
    };
  }
);

const selNewCardPayload = createSelector(
  [selWallet, selAuthUser],
  (wallet, auth) => {
    const { user } = auth;

    return {
      PBFPubKey: PBFKEY,
      customer_email: user.email,
      customer_firstname: user.name,
      custom_description: `Fund Wallet`,
      custom_logo: `https://res.cloudinary.com/dysm3stwj/image/upload/q_auto/v1535786205/ffl/soccer-ball.png`,
      custom_title: 'Fans Fantasy Ligue',
      amount: wallet.amount,
      customer_phone: '',
      country: 'NG',
      currency: 'NGN',
      payment_method: 'both',
      txref: `FFL-${Date.now()}`
    };
  }
);

const openNotificationWithIcon = (type, description) => {
  notification[type]({
    message: statusToTitle[type],
    description,
    style: { top: 36 }
  });
};

function updateCards(card) {
  return { type: CARD_SAVED_SUCCESS, card };
}

function loadTransactions() {
  return dispatch => {
    dispatch(loadUser());
    return Api.getAccountTransactions().then(transactions => {
      dispatch({ type: REQUEST_SUCCESS });
      return dispatch({
        type: LOAD_TRANSACTIONS,
        payload: {
          transactions,
          total: transactions.length
        }
      });
    });
  };
}

function loadCards() {
  return dispatch => {
    dispatch({ type: REQUEST });
    return Api.getCards().then(cards => {
      dispatch({ type: REQUEST_SUCCESS });
      return dispatch({
        type: LOAD_USER_CARDS,
        payload: {
          cards,
          total: cards.length
        }
      });
    });
  };
}

function setAmount(payload) {
  return dispatch => dispatch({ type: SET_AMOUNT, payload });
}

function selectCard(selectedCard) {
  return dispatch => dispatch({ type: SELECT_CARD, payload: { selectedCard } });
}

function selectAccount(selectedAccount) {
  return dispatch =>
    dispatch({ type: SELECT_ACCOUNT, payload: { selectedAccount } });
}

function load() {
  return dispatch => {
    dispatch({ type: REQUEST });

    return dispatch(loadTransactions()).catch(() =>
      dispatch({
        type: REQUEST_FAILED,
        payload: { message: 'Error loading account details' }
      })
    );
  };
}

function refresh() {
  return dispatch => dispatch({ type: REFRESH });
}

function saveCard(payload) {
  return async dispatch => {
    try {
      const res = await Api.saveCard(payload);
      dispatch(updateCards(payload));
      openNotificationWithIcon('success', res.message);
    } catch (e) {
      console.log('Error saving card: ', JSON.stringify(e));
      openNotificationWithIcon('error', e.message);
    }
  };
}

function fundWallet(creds, cb) {
  return (dispatch, getState) => {
    const state = getState();
    const payload = selNewCardPayload(state);
    const {
      auth: { user }
    } = state;

    dispatch({ type: WALLET_TOPUP });

    return getpaidSetup({
      ...payload,
      card: creds.number,
      expiry: creds.expiry,
      cvv: creds.cvv,
      onclose() {
        dispatch({ type: WALLET_TOPUP_CANCELLED });
      },
      callback: response => {
        const {
          tx: { chargeResponseCode, txRef, amount }
        } = response;

        if (chargeResponseCode === '00' || chargeResponseCode === '0') {
          dispatch({ type: WALLET_TOPUP_VERIFY });

          Api.fundWallet(user._id, {
            txref: txRef,
            currency: payload.currency,
            amount
          }).then(res => {
            const {
              card: {
                last4digits,
                type,
                card_tokens: [{ embedtoken }]
              }
            } = res;

            const cardPayload = {
              name: creds.name,
              number: last4digits,
              token: embedtoken,
              type
            };

            if (creds.rememberMe) {
              dispatch(saveCard(cardPayload));
            }
            dispatch({ type: WALLET_TOPUP_SUCCESS });
            openNotificationWithIcon('success', 'Successful wallet topup');
            dispatch(loadUser());
            cb();
          });
        } else {
          dispatch({ type: WALLET_TOPUP_FAILED });
        }
      }
    });
  };
}

function fundWalletFromCard(cb) {
  return (dispatch, getState) => {
    const state = getState();
    const payload = selSavedCardPayload(state);
    const {
      auth: { user }
    } = state;

    dispatch({ type: WALLET_TOPUP_FROM_SAVED_CARD });

    Api.chargeCard(payload)
      .then(res => {
        const { chargeResponseCode, txRef, amount } = res.data;
        if (chargeResponseCode === '00' || chargeResponseCode === '0') {
          dispatch({ type: WALLET_TOPUP_VERIFY });

          Api.fundWallet(user._id, {
            txref: txRef,
            currency: payload.currency,
            amount
          }).then(res => {
            dispatch({ type: WALLET_TOPUP_SUCCESS });
            openNotificationWithIcon('success', 'Successful wallet topup');
            dispatch(loadUser());
            cb();
          });
        } else {
          dispatch({ type: WALLET_TOPUP_FAILED });
        }
      })
      .catch(err => {
        const {
          body: { status, message }
        } = err;
        dispatch({ type: WALLET_TOPUP_FAILED });
        openNotificationWithIcon(status, message);
      });
  };
}

function deleteCard(id) {
  return (dispatch, getState) => {
    const { wallet } = getState();

    dispatch({ type: DELETE_CARD_REQUEST });

    Api.deleteCard(wallet.selectedCard)
      .then(res => {
        dispatch({ type: DELETE_CARD, cardId: id });
        openNotificationWithIcon('success', 'Card removed successfully');
      })
      .catch(err => {
        openNotificationWithIcon(
          'error',
          "Sorry, we're unable to delete your card"
        );
      });
  };
}

function deleteAccount(id) {
  return (dispatch, getState) => {
    const { wallet } = getState();

    dispatch({ type: DELETE_ACCOUNT_REQUEST });

    Api.deleteAccount(wallet.selectedAccount)
      .then(res => {
        dispatch({ type: DELETE_ACCOUNT, accountId: id });
        openNotificationWithIcon('success', 'Account deleted');
      })
      .catch(err => {
        openNotificationWithIcon(
          'error',
          "Sorry, we're unable to delete your saved account"
        );
      });
  };
}

function loadBanks() {
  return dispatch => {
    Api.getBanks()
      .then(({ data }) => {
        dispatch({ type: LOAD_BANKS, banks: data });
      })
      .catch(err => {
        console.log('Error getting banks: ', JSON.stringify(err));
      });
  };
}

function loadAccounts() {
  return dispatch => {
    dispatch({ type: REQUEST });
    return Api.getAccounts()
      .then(accounts => {
        dispatch({ type: REQUEST_SUCCESS });
        return dispatch({
          type: LOAD_USER_ACCOUNTS,
          payload: {
            accounts,
            total: accounts.length
          }
        });
      })
      .catch(err => {
        console.log('Error getting accounts: ', JSON.stringify(err));
      });
  };
}

function saveAccount(account) {
  return (dispatch, getState) => {
    const {
      wallet: { accounts = [] }
    } = getState();

    const existingAccount = find(accounts, {
      code: account.code,
      number: account.number
    });

    const isExisting = existingAccount ? existingAccount.length > 0 : false;

    if (isExisting) return;

    return Api.saveAccount(account)
      .then(() => {
        dispatch({ type: UPDATE_USER_ACCOUNTS, account });
        openNotificationWithIcon('success', 'Successfully added Account');
      })
      .catch(err => {
        openNotificationWithIcon('error', err.body.message);
      });
  };
}

function processPayout(account) {
  return (dispatch, getState) => {
    const {
      auth: {
        user: { _id }
      },
      wallet: { banks, amount }
    } = getState();

    const { rememberMe, code, accountName, accountNumber } = account;

    const payoutPayload = {
      accountNumber,
      accountName,
      code,
      amount
    };

    if (rememberMe) {
      const bank = find(banks, { code });
      dispatch(
        saveAccount({
          name: accountName,
          number: accountNumber,
          bank: bank.name,
          code: bank.code
        })
      );
    }

    return Api.payout(_id, payoutPayload)
      .then(res => {
        dispatch({ type: WALLET_TOPUP_SUCCESS });
        dispatch(loadUser()); // update user in the store
        dispatch(loadTransactions()); // update transaction table in the store
        openNotificationWithIcon('success', res.message); // show success notification
      })
      .catch(err => {
        dispatch({ type: WALLET_TOPUP_FAILED });
        console.log('Error:', JSON.stringify(err.body));
        openNotificationWithIcon('error', err.body.message);
      });
  };
}

function payToAccount(selectedAccount) {
  return (dispatch, getState) => {
    const {
      wallet: { accounts }
    } = getState();

    dispatch({ type: WALLET_TOPUP_FROM_SAVED_ACC });

    const account = find(accounts, { _id: selectedAccount });

    const payload = {
      code: account.code,
      accountName: account.name,
      accountNumber: account.number
    };

    dispatch(processPayout(payload));
  };
}

export {
  load,
  setAmount,
  loadCards,
  refresh,
  fundWalletFromCard,
  fundWallet,
  selectCard,
  deleteCard,
  loadAccounts,
  selectAccount,
  deleteAccount,
  loadBanks,
  processPayout,
  saveAccount,
  payToAccount
};
