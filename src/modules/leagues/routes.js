import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/leagues',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import(/* webpackChunkName: "leagues" */ './pages/Leagues'),
      loading: Loader,
      modules: ['leagues']
    })
  },
  {
    path: '/leagues/me',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "myLeagues" */ './pages/MyLeagues'),
      loading: Loader,
      modules: ['myLeagues']
    })
  },
  {
    path: '/leagues/create',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "createLeagues" */ './pages/Create'),
      loading: Loader,
      modules: ['createLeagues']
    })
  },
  {
    path: '/leagues/join',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "joinLeagues" */ './pages/Join'),
      loading: Loader,
      modules: ['joinLeagues']
    })
  },
  {
    path: '/leagues/invite',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "leagueInvite" */ './pages/Invite'),
      loading: Loader,
      modules: ['leagueInvite']
    })
  },
  {
    path: '/leagues/:leagueId/standings',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "leagueStandings" */ './pages/Standing'),
      loading: Loader,
      modules: ['leagueStandings']
    })
  },
  {
    path: '/leagues/hot-leagues',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "promoLeagues" */ './pages/PromoLeagues'),
      loading: Loader,
      modules: ['promoLeagues']
    })
  }
];

export default routes;
