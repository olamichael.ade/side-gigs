import React from 'react';
import './numberRowTab.scss';

const NumberTab = ({ changeIndex, selected }) => {
  const tab = number => {
    return (
      <div
        key={`tab${number}`}
        className={`number-box col-xs-4 ${
          selected === number ? 'selected' : ''
        }`}
        onClick={() => changeIndex(number)}
      >
        <span className="number">{number}</span>
      </div>
    );
  };
  return [1, 2, 3].map(item => {
    return tab(item);
  });
};

export default NumberTab;
