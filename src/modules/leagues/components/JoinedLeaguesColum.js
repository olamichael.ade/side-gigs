import React from 'react';
import { Text } from 'components';
import './Column.scss';

export default [
  {
    title: <Text classText="jc-col-header">Game Name</Text>,
    align: 'left',
    dataIndex: 'name',
    className: 'jc-col',
    render: (text, record) => (
      <Text classText="jc-title" color="black">
        {record.name}
      </Text>
    )
  },
  {
    title: <Text classText="jc-col-header">Game Duration</Text>,
    align: 'left',
    dataIndex: 'start',
    className: 'jc-col gw',
    render: (text, record) => (
      <span>
        <Text classText="jc-value-text gw">{`Gameweek ${record.start} - ${
          record.end
        }`}</Text>
        <Text classText="jc-value-text-mobile">{`GW ${record.start}-${
          record.end
        }`}</Text>
      </span>
    )
  },
  {
    title: <Text classText="jc-col-header">Type</Text>,
    align: 'right',
    dataIndex: 'type',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.type}</Text>
  },
  {
    title: <Text classText="jc-col-header">Fee</Text>,
    align: 'right',
    dataIndex: 'fee',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.fee || 0}</Text>
  }
];
