import React from 'react';
import { Row, Col } from 'antd';
import { Topbar, Text, LinkedText } from 'components';
import './Header.scss';

const Header = props => {
  const { title } = props;
  return (
    <div className="league-header-container">
      <Topbar>
        <Row type="flex" className="lh-header-row">
          <Col xs={{ span: 8 }} sm={{ span: 12 }}>
            <Text classText="lh-title" color="black">
              {title}
            </Text>
          </Col>
          <Col xs={{ span: 16 }} sm={{ span: 12 }}>
            <Row type="flex" gutter={10}>
              <Col
                xs={{ span: 6 }}
                sm={{ span: 0 }}
                md={{ span: 0 }}
                className="lh-links hot-leagues"
              >
                <LinkedText to="/leagues/hot-leagues">
                  <Text>Hot Leagues</Text>
                </LinkedText>
              </Col>
              <Col xs={{ span: 6 }} sm={{ span: 8 }} className="lh-links">
                <LinkedText to="/leagues/me">
                  <Text>My Leagues</Text>
                </LinkedText>
              </Col>
              <Col xs={{ span: 6 }} sm={{ span: 8 }} className="lh-links">
                <LinkedText to="/leagues/create">
                  <Text>Create League</Text>
                </LinkedText>
              </Col>
              <Col xs={{ span: 6 }} sm={{ span: 8 }} className="lh-links">
                <LinkedText to="/leagues/join">
                  <Text classText="lh-links-desktop">Private Leagues</Text>
                </LinkedText>
              </Col>
            </Row>
          </Col>
        </Row>
      </Topbar>
    </div>
  );
};

export default Header;
