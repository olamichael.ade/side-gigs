import React, { PureComponent } from 'react';
import { Tabs, Table } from 'antd';
import Column from './MyChallengeColumn';
import JoinedLeaguesColumn from './JoinedLeaguesColum';
import './Tabs.scss';

const TabPane = Tabs.TabPane;

class WrappedTabs extends PureComponent {
  gotoDetails = record => {
    this.props.history.push(`/leagues/${record._id}/standings`);
  };

  render() {
    const { leagues, joinedLeagues } = this.props;

    return (
      <Tabs
        className="league-tab-container"
        defaultActiveKey="1"
        onChange={this.callback}
      >
        <TabPane tab="Joined Leagues" key="1">
          <div>
            <Table
              onRow={record => {
                return {
                  onClick: () => this.gotoDetails(record)
                };
              }}
              rowKey={record => record._id}
              dataSource={joinedLeagues}
              columns={JoinedLeaguesColumn}
              locale={{ emptyText: 'You have not joined any League' }}
            />
          </div>
        </TabPane>
        <TabPane tab="My Leagues" key="2">
          <div>
            <Table
              onRow={record => {
                return {
                  onClick: () => this.gotoDetails(record)
                };
              }}
              rowKey={record => record._id}
              dataSource={leagues}
              columns={Column}
              locale={{ emptyText: 'You have not created any League' }}
            />
          </div>
        </TabPane>
      </Tabs>
    );
  }
}

export default WrappedTabs;
