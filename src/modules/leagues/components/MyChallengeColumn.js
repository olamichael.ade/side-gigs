import React from 'react';
// import { Link } from 'react-router-dom';
import { Text, Icon } from 'components';
import { capitalize } from 'utils/utils';
import './Column.scss';

const { ICON_LOCK, ICON_UNLOCK } = Icon;

export default [
  {
    title: <Text classText="jc-col-header">Game Name</Text>,
    align: 'left',
    dataIndex: 'name',
    className: 'jc-col',
    render: (text, record) => (
      <Text classText="jc-title" color="black">
        {record.name}
      </Text>
    )
  },
  {
    title: <Text classText="jc-col-header">Game Duration</Text>,
    align: 'left',
    dataIndex: 'start',
    className: 'jc-col gw',
    render: (text, record) => (
      <span>
        <Text classText="jc-value-text gw">{`Gameweek ${record.start} - ${
          record.end
        }`}</Text>
        <Text classText="jc-value-text-mobile">{`GW ${record.start}-${
          record.end
        }`}</Text>
      </span>
    )
  },
  {
    title: <Text classText="jc-col-header">Fee</Text>,
    align: 'right',
    dataIndex: 'fee',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.fee || 0}</Text>
  },
  {
    title: <Text classText="jc-col-header">Status</Text>,
    align: 'right',
    dataIndex: 'status',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.state}</Text>
  },
  {
    title: <Text classText="jc-col-header">Type</Text>,
    align: 'center',
    dataIndex: 'type',
    className: 'jc-col gw',
    render: (text, record) => (
      <span>
        <Text classText="jc-value-text gw">{`${capitalize(record.type)}`}</Text>
        <Text classText="jc-value-text-mobile">
          <Icon
            iconType={record.type === 'private' ? ICON_LOCK : ICON_UNLOCK}
          />
        </Text>
      </span>
    )
  }
  // {
  //   title: '',
  //   align: 'right',
  //   dataIndex: 'control',
  //   className: 'jc-col',
  //   render: (t, r) => {
  //     return (
  //       <div className="ctrl-container">
  //         <Link to={`/`}>
  //           <Icon iconType={ICON_SETTINGS} />
  //         </Link>
  //         <Link to={`/`}>
  //           <Icon iconType={ICON_DELETE} />
  //         </Link>
  //       </div>
  //     );
  //   }
  // }
];
