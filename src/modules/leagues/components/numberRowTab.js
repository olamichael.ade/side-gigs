import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'components';
import './numberRowTab.scss';

const NumberRowTab = props => {
  const { title, children } = props;

  return (
    <div className="number-topbar-container">
      <Text classText="top-title" weight="med2" color="black">
        {title}
      </Text>
      <div>{children}</div>
    </div>
  );
};

NumberRowTab.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node
};

export default NumberRowTab;
