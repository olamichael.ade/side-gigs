import React from 'react';
import { Link } from 'react-router-dom';
import './topNav.scss';

const TopNavBar = ({ leagueName, showTabs }) => {
  return (
    <div className="top-nav-wrapper container">
      <div className="vertical-align">
        <div className="col-sm-6 col-xs-12 title">{leagueName}</div>
        {showTabs && (
          <div className="col-sm-4 col-xs-12 nav-item-tabs">
            <Link to="/leagues/create">
              <div className="col-xs-6 nav-links">Create League</div>
            </Link>
            <Link to="/leagues/join">
              <div className="col-xs-6 nav-links">Join League</div>
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};

TopNavBar.defaultProps = {
  leagueName: '',
  showTabs: false
};
export default TopNavBar;
