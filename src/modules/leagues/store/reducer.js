import {
  REFRESH,
  LOAD_LEAGUES,
  LOAD_LEAGUES_SUCCESS,
  LOAD_LEAGUES_FAIL,
  CREATE_LEAGUE_STEP,
  CREATE_LEAGUE,
  CREATE_LEAGUE_SUCCESS,
  CREATE_LEAGUE_FAILURE,
  RESET_IS_CREATED,
  SEARCH_LEAGUE,
  SEARCH_LEAGUE_SUCCESS,
  SEARCH_LEAGUE_FAILURE,
  FETCH_LEAGUE,
  FETCH_LEAGUE_SUCCESS,
  FETCH_LEAGUE_FAILURE,
  RESET_LEAGUE,
  UPDATE_LEAGUE_DETAILS,
  JOIN_LEAGUE,
  JOIN_LEAGUE_SUCCESS,
  JOIN_LEAGUE_FAILURE,
  CLEAR_JOIN_ERROR,
  FETCH_GAME_WEEK,
  DISMISS_MODAL,
  LOAD_JOINED_LEAGUES_SUCCESS,
  IS_REQESTING,
  IS_REQUEST_SUCCESS,
  IS_REQUEST_FAILED,
  LOAD_LEAGUE_STANDING,
  REFRESH_LEAGUE_STANDING,
  REFRESH_TRENDING_LEAGUES,
  LOAD_TRENDING_LEAGUES
} from './actionTypes';

const initialState = {
  loading: false,
  loaded: false,
  leagues: [],
  joinedLeagues: [],
  trendingLeagues: [],
  standings: [],
  league: {},
  error: false,
  success: false,
  errorMessage: null,
  step: 1,
  creating: false,
  isCreatedSucess: false,
  joining: false,
  gameWeeksDetails: {
    all: [],
    currentGameWeek: {},
    nextGameWeek: {},
    previousGameWeek: {}
  },
  joinleagueDetails: {
    step: null
  },
  showModal: true,
  isAjaxReq: false,
  isReqComplete: false
};

function createLeagueSuccess(state, data) {
  return {
    ...state,
    league: data.newLeague,
    creating: false,
    isCreatedSucess: true
  };
}

function updateLeagueDetails(state, data) {
  const newObject = Object.assign({}, state.joinleagueDetails, data);
  return {
    ...state,
    joinleagueDetails: newObject
  };
}

function setGameWeek(state, weeks) {
  const currentGameWeek = weeks.find(e => e.is_current) || {};
  const nextGameWeek = weeks.find(e => e.is_next) || {};
  const previousGameWeek = weeks.find(e => e.is_previous) || {};
  return {
    ...state,
    gameWeeksDetails: {
      all: weeks,
      currentGameWeek,
      nextGameWeek,
      previousGameWeek
    }
  };
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case REFRESH: {
      return { ...state, loading: false, loaded: false };
    }

    case IS_REQESTING: {
      return {
        ...state,
        isAjaxReq: true,
        isReqComplete: false,
        success: false
      };
    }

    case IS_REQUEST_SUCCESS: {
      return { ...state, isAjaxReq: false, isReqComplete: true, success: true };
    }

    case IS_REQUEST_FAILED: {
      return { ...state, isAjaxReq: false, isReqComplete: true, error: true };
    }

    case LOAD_LEAGUES: {
      return { ...state, loading: true };
    }

    case LOAD_LEAGUES_SUCCESS: {
      const { leagues } = action;
      return {
        ...state,
        loading: false,
        loaded: true,
        success: true,
        leagues
      };
    }

    case LOAD_JOINED_LEAGUES_SUCCESS: {
      const { joinedLeagues } = action;
      return {
        ...state,
        joinedLeagues
      };
    }

    case LOAD_LEAGUE_STANDING: {
      const { standings } = action;
      return { ...state, standings };
    }

    case LOAD_TRENDING_LEAGUES: {
      const { trendingLeagues } = action;
      return { ...state, trendingLeagues };
    }

    case LOAD_LEAGUES_FAIL: {
      const { errorMessage } = action;
      return {
        ...state,
        loading: false,
        loaded: false,
        error: true,
        errorMessage
      };
    }

    case FETCH_LEAGUE_SUCCESS: {
      const { data } = action;
      return {
        ...state,
        loading: false,
        loaded: true,
        success: true,
        league: data
      };
    }
    case FETCH_LEAGUE:
      return { ...state, loading: true };

    case FETCH_LEAGUE_FAILURE: {
      return {
        ...state,
        error: true,
        errorMessage: action.message
      };
    }
    case CREATE_LEAGUE_STEP: {
      return { ...state, step: action.step };
    }

    case CREATE_LEAGUE: {
      return { ...state, creating: true };
    }

    case CREATE_LEAGUE_SUCCESS: {
      return createLeagueSuccess(state, action.data);
    }

    case CREATE_LEAGUE_FAILURE: {
      return {
        ...state,
        error: true,
        errorMessage: action.message,
        creating: false
      };
    }

    case RESET_IS_CREATED:
      return { ...state, isCreatedSucess: false };

    case SEARCH_LEAGUE:
      return { ...state, isSearching: true };

    case SEARCH_LEAGUE_SUCCESS:
      return { ...state, league: action.data, isSearching: false };

    case SEARCH_LEAGUE_FAILURE:
      return {
        ...state,
        error: true,
        errorMessage: action.message,
        isSearching: false
      };

    case RESET_LEAGUE:
      return { ...state, league: {} };

    case UPDATE_LEAGUE_DETAILS:
      return updateLeagueDetails(state, action.data);

    case JOIN_LEAGUE:
      return { ...state, joining: true };

    case JOIN_LEAGUE_SUCCESS:
      return {
        ...state,
        joining: false
      };
    case JOIN_LEAGUE_FAILURE:
      return {
        ...state,
        joining: false,
        errorMessage: action.errorMessage,
        error: true
      };
    case CLEAR_JOIN_ERROR:
      return {
        ...state,
        error: false,
        errorMessage: null
      };
    case FETCH_GAME_WEEK:
      return setGameWeek(state, action.gameWeeks);
    case DISMISS_MODAL:
      return { ...state, showModal: false };
    case REFRESH_TRENDING_LEAGUES:
    case REFRESH_LEAGUE_STANDING: {
      return {
        ...state,
        isAjaxReq: false,
        isReqComplete: false,
        standings: [],
        trendingLeagues: []
      };
    }
    default:
      return state;
  }
}

export default reducer;
