import Api from 'utils/Api';
import moment from 'moment';
import {
  REFRESH,
  LOAD_LEAGUES,
  LOAD_LEAGUES_SUCCESS,
  LOAD_LEAGUES_FAIL,
  CREATE_LEAGUE_STEP,
  CREATE_LEAGUE,
  CREATE_LEAGUE_SUCCESS,
  CREATE_LEAGUE_FAILURE,
  RESET_IS_CREATED,
  SEARCH_LEAGUE,
  SEARCH_LEAGUE_SUCCESS,
  SEARCH_LEAGUE_FAILURE,
  FETCH_LEAGUE_SUCCESS,
  FETCH_LEAGUE_FAILURE,
  FETCH_LEAGUE,
  RESET_LEAGUE,
  UPDATE_LEAGUE_DETAILS,
  JOIN_LEAGUE,
  JOIN_LEAGUE_FAILURE,
  JOIN_LEAGUE_SUCCESS,
  CLEAR_JOIN_ERROR,
  FETCH_GAME_WEEK,
  DISMISS_MODAL,
  LOAD_JOINED_LEAGUES_SUCCESS,
  IS_REQESTING,
  IS_REQUEST_SUCCESS,
  IS_REQUEST_FAILED,
  LOAD_LEAGUE_STANDING,
  REFRESH_LEAGUE_STANDING,
  LOAD_TRENDING_LEAGUES,
  REFRESH_TRENDING_LEAGUES
} from './actionTypes';
import { loadUser } from 'modules/auth/store/actions';

function refresh() {
  return { type: REFRESH };
}

function getLeaguePayload(params) {
  const close = moment(params.lastGameEntry).format('MM-DD-YYYY hh:mm:ss');
  return {
    name: params.leagueName,
    min: params.minimumParticipants,
    limit: params.maximumParticipants,
    fee: parseInt(params.fee, 10),
    type: params.leagueType,
    start: params.leagueStart,
    end: params.leagueEnd,
    close,
    rewards: params.reward,
    prize: 0
  };
}

function loadUserLeagues(userId) {
  return dispatch => {
    dispatch({ type: LOAD_LEAGUES });

    return Api.getUsersLeagues(userId)
      .then(response => {
        dispatch({ type: LOAD_LEAGUES_SUCCESS, leagues: response });
      })
      .catch(() => {
        dispatch({
          type: LOAD_LEAGUES_FAIL,
          errorMessage: 'Could not retrieve leagues for user.'
        });
      });
  };
}
function createLeague(creds) {
  return dispatch => {
    const payload = getLeaguePayload(creds);
    dispatch({ type: CREATE_LEAGUE });
    return Api.createLeague(payload)
      .then(response => {
        dispatch({
          type: CREATE_LEAGUE_SUCCESS,
          data: response
        });
        return { statusOk: true, message: response };
      })
      .catch(err => {
        dispatch({
          type: CREATE_LEAGUE_FAILURE,
          message:
            (err && err.body && err.body.message) || 'Error creating league'
        });
        return { statusOk: false, message: err };
      });
  };
}

function createLeagueStepToggle(step) {
  return dispatch => {
    dispatch({ type: CREATE_LEAGUE_STEP, data: step });
  };
}

function resetIsCreated() {
  return dispatch => {
    dispatch({ type: RESET_IS_CREATED });
  };
}

function searchLeague(code) {
  return dispatch => {
    dispatch({ type: SEARCH_LEAGUE });
    return Api.getOneLeague(code)
      .then(response => {
        if (!response[0]._id) {
          return dispatch({
            type: SEARCH_LEAGUE_FAILURE,
            message: 'Not Found'
          });
        }
        dispatch({ type: SEARCH_LEAGUE_SUCCESS, data: response[0] });
        return { statusOk: true, league: response };
      })
      .catch(error => {
        dispatch({
          type: SEARCH_LEAGUE_FAILURE,
          message: (error && error.body && error.body.message) || 'Not Found'
        });
      });
  };
}

function gameWeeks() {
  return dispatch => {
    return Api.getGameWeeks()
      .then(resp => {
        dispatch({ type: FETCH_GAME_WEEK, gameWeeks: resp });
      })
      .catch(err => {
        console.log('Could not get game weeks:', err);
      });
  };
}
function findLeagueById(id) {
  return dispatch => {
    dispatch({ type: FETCH_LEAGUE });
    return Api.findLeagueById(id)
      .then(response => {
        dispatch({ type: FETCH_LEAGUE_SUCCESS, data: response });
        return response;
      })
      .catch(error => {
        dispatch({ type: FETCH_LEAGUE_FAILURE, data: error });
        console.log(error);
      });
  };
}
function resetLeague() {
  return dispatch => dispatch({ type: RESET_LEAGUE });
}
function updateJoinLeague(data) {
  return dispatch => dispatch({ type: UPDATE_LEAGUE_DETAILS, data });
}

function joinLeague(id) {
  return dispatch => {
    dispatch({ type: JOIN_LEAGUE });
    return Api.joinLeague(id)
      .then(response => {
        dispatch({ type: JOIN_LEAGUE_SUCCESS, data: response });
        dispatch(loadUser());
        return response;
      })
      .catch(err => {
        dispatch({
          type: JOIN_LEAGUE_FAILURE,
          errorMessage: err.body.message
        });
      });
  };
}

function dismissModal() {
  return dispatch => dispatch({ type: DISMISS_MODAL });
}

function loadJoinedLeagues() {
  return dispatch => {
    return Api.loadJoinedLeagues()
      .then(({ leagues }) => {
        dispatch({ type: LOAD_JOINED_LEAGUES_SUCCESS, joinedLeagues: leagues });
      })
      .catch(err => {
        dispatch({
          type: LOAD_LEAGUES_FAIL,
          errorMessage: 'Could not retrieve joined leagues for user.'
        });
      });
  };
}

function loadLeagues() {
  return (dispatch, getState) => {
    const {
      auth: { user }
    } = getState();
    const { _id } = user || {};

    dispatch(loadUserLeagues(_id));
    dispatch(loadJoinedLeagues());
  };
}

function loadLeagueStanding(leagueId) {
  return dispatch => {
    dispatch({ type: IS_REQESTING });
    dispatch(findLeagueById(leagueId));
    return Api.loadLeagueStanding(leagueId)
      .then(({ standing }) => {
        dispatch({ type: IS_REQUEST_SUCCESS });
        dispatch({ type: LOAD_LEAGUE_STANDING, standings: standing });
      })
      .catch(err => {
        dispatch({ type: IS_REQUEST_FAILED });
        console.log('Could not retrieve league standings', JSON.stringify(err));
      });
  };
}

function refreshStanding() {
  return dispatch => dispatch({ type: REFRESH_LEAGUE_STANDING });
}

function refreshTrending() {
  return dispatch => dispatch({ type: REFRESH_TRENDING_LEAGUES });
}

function loadTrendingLeagues() {
  return dispatch => {
    dispatch({ type: IS_REQESTING });
    return Api.loadTrendingLeagues()
      .then(res => {
        dispatch({ type: IS_REQUEST_SUCCESS });
        dispatch({ type: LOAD_TRENDING_LEAGUES, trendingLeagues: res.leagues });
      })
      .catch(err => {
        dispatch({ type: IS_REQUEST_FAILED });
        console.log('Could not retrieve trending leagues', JSON.stringify(err));
      });
  };
}

function clearError() {
  return dispatch => dispatch({ type: CLEAR_JOIN_ERROR });
}

export {
  loadUserLeagues,
  refresh,
  createLeagueStepToggle,
  createLeague,
  resetIsCreated,
  searchLeague,
  gameWeeks,
  findLeagueById,
  resetLeague,
  updateJoinLeague,
  joinLeague,
  dismissModal,
  loadJoinedLeagues,
  loadLeagues,
  loadLeagueStanding,
  refreshStanding,
  loadTrendingLeagues,
  refreshTrending,
  clearError
};
