export const LOAD_LEAGUES = 'leagues/LOAD_LEAGUES';
export const LOAD_LEAGUES_SUCCESS = 'leagues/LOAD_LEAGUES_SUCCESS';
export const LOAD_JOINED_LEAGUES_SUCCESS =
  'leagues/LOAD_JOINED_LEAGUES_SUCCESS';
export const LOAD_LEAGUES_FAIL = 'leagues/LOAD_LEAGUES_FAIL';
export const REFRESH = 'leagues/REFRESH';
export const CREATE_LEAGUE = 'leagues/CREATE_LEAGUE';
export const CREATE_LEAGUE_SUCCESS = 'leagues/CREATE_LEAGUE_SUCCESS';
export const CREATE_LEAGUE_FAILURE = 'leagues/CREATE_LEAGUE_FAILURE';
export const SEARCH_LEAGUE = 'leagues/SEARCH_LEAGUE';
export const SEARCH_LEAGUE_SUCCESS = 'leagues/SEARCH_LEAGUE_SUCCESS';
export const SEARCH_LEAGUE_FAILURE = 'leagues/SEARCH_LEAGUE_FAILURE';
export const FETCH_LEAGUE = '/leagues/FETCH_LEAGUE';
export const FETCH_LEAGUE_SUCCESS = '/leagues/FETCH_LEAGUE_SUCCESS';
export const FETCH_LEAGUE_FAILURE = '/leagues/FETCH_LEAGUE_FAILURE';
export const JOIN_LEAGUE = 'leagues/JOIN_LEAGUE';
export const JOIN_LEAGUE_SUCCESS = 'leagues/JOIN_LEAGUE_SUCCESS';
export const JOIN_LEAGUE_FAILURE = 'leagues/JOIN_LEAGUE_FAILURE';
export const FETCH_GAME_WEEK = 'leagues/FETCH_GAME_WEEK';
export const IS_REQESTING = 'leagues/IS_REQESTING';
export const IS_REQUEST_SUCCESS = 'leagues/IS_REQUEST_SUCCESS';
export const IS_REQUEST_FAILED = 'leagues/IS_REQUEST_FAILED';
export const LOAD_LEAGUE_STANDING = 'leagues/LOAD_LEAGUE_STANDING';
export const LOAD_TRENDING_LEAGUES = 'leagues/LOAD_TRENDING_LEAGUE';
export const REFRESH_LEAGUE_STANDING = 'league/REFRESH_LEAGUE_STANDING';
export const REFRESH_TRENDING_LEAGUES = 'league/REFRESH_TRENDING_LEAGUES';
export const CLEAR_JOIN_ERROR = 'league/CLEAR_JOIN_ERROR';

// Non-server actions
export const CREATE_LEAGUE_STEP = 'leagues/CREATE_LEAGUE_STEP';
export const RESET_IS_CREATED = 'leagues/RESET_IS_CREATED';
export const RESET_LEAGUE = 'RESET_LEAGUE';
export const UPDATE_LEAGUE_DETAILS = 'UPDATE_LEAGUE_DETAILS';
export const DISMISS_MODAL = 'DISMISS_MODAL';
