import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { LoadComponent } from 'HOCS';
import {
  getPromoLeagues,
  refreshPromoLeagues
} from 'modules/global/store/actions';

import LeaguesTable from './components/LeaguesTable';

const mapStateToProps = state => {
  const { globals, leagues, auth } = state;
  return {
    user: auth.user,
    loading: globals.loading,
    loaded: globals.loaded,
    promoLeagues: globals.promoLeagues,
    joinedLeagues: leagues.joinedLeagues
  };
};

export default compose(
  connect(
    mapStateToProps,
    {
      load: getPromoLeagues,
      refresh: refreshPromoLeagues
    }
  ),
  LoadComponent,
  withRouter
)(LeaguesTable);
