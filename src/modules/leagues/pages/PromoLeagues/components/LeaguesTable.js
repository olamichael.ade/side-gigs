import React from 'react';
import { Table, Tabs } from 'antd';
import moment from 'moment';
import { Text } from 'components';
import LeaguesTableColumn from './LeaguesTableColumn';
import JoinedLeaguesColumn from './JoinedLeaguesColumn';

import './LeaguesTable.scss';

const { TabPane } = Tabs;

const PromoLeagues = props => {
  const { history, promoLeagues, joinedLeagues, user = {} } = props;
  const { leagues = [] } = user ? user.team : {};

  const handleClick = record => {
    const isOpen = moment(record.close) > moment();
    const hasJoinedLeague = leagues.includes(record._id);

    if (record.state === 'open' && !hasJoinedLeague && isOpen)
      return history.push(`/leagues/join?code=${record.code}`);

    return history.push(`/leagues/${record._id}/standings`);
  };

  const gotoDetails = record =>
    history.push(`/leagues/${record._id}/standings`);

  const getLeaguesGrp = filterKey =>
    promoLeagues.filter(pl => pl.state === filterKey);

  return (
    <div>
      <section className="ch2h-container">
        <div className="form-title-container">
          <Text classText="ch2h-form-title" weight="med">
            Leagues
          </Text>
        </div>
        <div>
          <Tabs className="league-tab-container" defaultActiveKey="1">
            <TabPane tab="Open Leagues" key="1">
              <Table
                onRow={record => {
                  return {
                    onClick: () => handleClick(record)
                  };
                }}
                rowKey={record => record._id}
                dataSource={getLeaguesGrp('open')}
                columns={LeaguesTableColumn}
                locale={{ emptyText: 'No Open Leagues' }}
              />
            </TabPane>
            <TabPane tab="Ended Leagues" key="2">
              <Table
                onRow={record => {
                  return {
                    onClick: () => handleClick(record)
                  };
                }}
                rowKey={record => record._id}
                dataSource={getLeaguesGrp('ended')}
                columns={LeaguesTableColumn}
                locale={{ emptyText: 'No Ended Leagues' }}
              />
            </TabPane>
            <TabPane tab="Joined Leagues" key="3">
              <Table
                onRow={record => {
                  return {
                    onClick: () => gotoDetails(record)
                  };
                }}
                rowKey={record => record._id}
                dataSource={joinedLeagues}
                columns={JoinedLeaguesColumn}
                locale={{ emptyText: 'You have not joined any League' }}
              />
            </TabPane>
          </Tabs>
        </div>
      </section>
    </div>
  );
};

export default PromoLeagues;
