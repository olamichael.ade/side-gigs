import React from 'react';
import { Tag } from 'antd';
import { Text } from 'components';
import moment from 'moment';

import './LeaguesTable.scss';

const PromoColumn = [
  {
    title: <Text classText="jc-col-header">League Name</Text>,
    align: 'left',
    dataIndex: 'title',
    className: 'jc-col',
    render: (text, record) => (
      <Text classText="jc-title" color="black">
        {record.name}
      </Text>
    )
  },
  {
    title: <Text classText="jc-col-header">Game Duration</Text>,
    align: 'left',
    dataIndex: 'start',
    className: 'jc-col gw',
    render: (text, record) => (
      <span>
        <Text classText="jc-value-text gw">{`Gameweek ${record.start} - ${
          record.end
        }`}</Text>
        <Text classText="jc-value-text-mobile">{`GW ${record.start}-${
          record.end
        }`}</Text>
      </span>
    )
  },
  {
    title: <Text classText="jc-col-header">Fee</Text>,
    align: 'right',
    dataIndex: 'fee',
    className: 'jc-col',
    render: (t, r) => (
      <Text classText="jc-value-text">{`N${(
        r.fee || 0
      ).toLocaleString()}`}</Text>
    )
  },
  {
    title: <Text classText="jc-col-header">Prize</Text>,
    align: 'center',
    dataIndex: 'prize',
    className: 'jc-col',
    render: (t, r) => (
      <Text classText="jc-value-text">{`N${r.prize.toLocaleString()}`}</Text>
    )
  },
  {
    title: <Text classText="jc-col-header">Status</Text>,
    align: 'center',
    dataIndex: 'state',
    className: 'jc-col',
    render: (t, r) => {
      const isClosed = moment() > moment(r.close);
      const state = r.state === 'open' && isClosed ? 'closed' : r.state;

      return (
        <Text classText="jc-value-text">
          <Tag color="green">{state}</Tag>
        </Text>
      );
    }
  }
];

export default PromoColumn;
