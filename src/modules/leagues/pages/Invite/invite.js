import React, { Component, Fragment } from 'react';
import { Button, Text } from 'components';
import { parse } from 'qs';
import { Link } from 'react-router-dom';
import { Icon, Tooltip } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import './invite.scss';

class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copied: false
    };
  }
  componentWillMount() {
    const { location } = this.props;

    const query = parse(location.search.substr(1));
    if (query.code) {
      this.props.searchLeague(query.code);
    }
  }
  componentWillUnmount() {
    if (this.props.isCreatedSuccess) this.props.resetIsCreated();
  }

  handleDone = () => {
    const { history } = this.props;
    history.push('/leagues/me');
  };

  onCopy = () => {
    this.setState({ copied: true }, () => {
      setTimeout(() => {
        this.setState({ copied: false });
      }, 500);
    });
  };

  render() {
    const shareText =
      'Join%20me%20on%20Fantasy%20Fan%20League.%20And%20you%20can%20be%20next%20next%20big%20Fantasy%20Manager';
    const { isCreatedSuccess, league } = this.props;
    var base_url = window.location.origin;
    return (
      <div className="invite-body">
        {isCreatedSuccess && (
          <div className="success-text">
            Your league has been successfully created
          </div>
        )}
        {isCreatedSuccess && <hr />}
        {league.code ? (
          <Fragment>
            <div
              className={`league-info ${!isCreatedSuccess ? 'space-item' : ''}`}
            >
              <p className="rc-head-title">Invite Friends</p>
              <p className="rc-head-subtitle">
                Send invites to your friends via{' '}
              </p>
              <h1 className="rc-code">{league.code}</h1>
              <div className="social-icons">
                <div className="facebook-icon">
                  {/* <a class="twitter-share-button"
                      href={`https://www.facebook.com/sharer/sharer.php?u=${shareText} ${base_url}/league/join?code=${league.code}`}
                      data-size="large"> */}
                  <Icon
                    type="facebook"
                    style={{ fontSize: 20, color: '#fff' }}
                  />
                  {/* </a> */}
                </div>
                <div className="twitter-icon">
                  <a
                    className="twitter-share-button"
                    href={`https://twitter.com/intent/tweet?text=${shareText} ${base_url}/league/join?code=${
                      league.code
                    }`}
                    data-size="large"
                  >
                    <Icon
                      type="twitter"
                      style={{ fontSize: 20, color: '#fff' }}
                    />
                  </a>
                </div>
                <div className="email-icon">
                  <Icon type="mail" style={{ fontSize: 20, color: '#fff' }} />
                </div>
                <div className="share-icon">
                  <Icon
                    type="share-alt"
                    style={{ fontSize: 20, color: '#fff' }}
                  />
                </div>
              </div>
              <div className="copy-field">
                <div className="url">
                  <Tooltip
                    placement="top"
                    title={this.state.copied ? 'Copied' : 'Copy to Clipboard'}
                  >
                    <CopyToClipboard
                      onCopy={this.onCopy}
                      text={`${base_url}/league/join?code=${league.code}`}
                    >
                      <span className="url">{`${base_url}/league/join?code=${
                        league.code
                      }`}</span>
                    </CopyToClipboard>
                  </Tooltip>
                </div>
                <div className="copy-icon">
                  <Tooltip
                    placement="top"
                    title={this.state.copied ? 'Copied' : 'Copy to Clipboard'}
                  >
                    <CopyToClipboard
                      onCopy={this.onCopy}
                      options={{ message: 'Copied!' }}
                      text={league.code}
                    >
                      <Icon
                        type="copy"
                        style={{ fontSize: 20, color: '#002157' }}
                        onClick={this.onClick}
                      />
                    </CopyToClipboard>
                  </Tooltip>
                </div>
              </div>
              <div className="lg-settings">
                <Link to="#">view league settings</Link>
              </div>
            </div>
            <div className="btn-container">
              <Button
                color="purple"
                className="auth-form-button"
                htmlType="submit"
                size="large"
                onClick={this.handleDone}
              >
                <Text color="white" classText="btn-text">
                  DONE
                </Text>
              </Button>
            </div>
          </Fragment>
        ) : (
          <div> No league selected</div>
        )}
      </div>
    );
  }
}
Invite.defaultProps = {
  code: ''
};
export default Invite;
