import { compose } from 'redux';
import { connect } from 'react-redux';
import { resetIsCreated, searchLeague } from '../../store/actions';
// import { LoadComponent, RefreshComponent } from 'HOCS';

import Invite from './invite';

const mapStateToProps = state => {
  const { auth, leagues } = state;
  return {
    user: auth.user,
    isCreatedSuccess: leagues.isCreatedSucess,
    league: leagues.league
  };
};

export default compose(
  connect(
    mapStateToProps,
    { resetIsCreated, searchLeague }
  )
)(Invite);
