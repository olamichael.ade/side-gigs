import { compose } from 'redux';
import { connect } from 'react-redux';
import { gameWeeks } from '../../store/actions';
import { RefreshComponent } from 'HOCS';
import {
  refresh,
  createLeague,
  createLeagueStepToggle
} from '../../store/actions';
import Create from './create';

const mapStateToProps = state => {
  const { auth, leagues } = state;
  return {
    user: auth.user,
    step: leagues.step,
    creating: leagues.creating,
    error: leagues.error,
    errorMessage: leagues.errorMessage,
    gameWeeksDetails: leagues.gameWeeksDetails,
    currentWeek: leagues.gameWeeksDetails.currentGameWeek.id
  };
};

export default compose(
  connect(
    mapStateToProps,
    { createLeague, refresh, createLeagueStepToggle, gameWeeks }
  ),
  // LoadComponent,
  RefreshComponent
)(Create);
