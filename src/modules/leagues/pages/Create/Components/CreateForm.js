import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { DatePicker } from 'antd';
import moment from 'moment';
import { Button, Text } from 'components';
import Input from 'components/Input';
import Select from 'components/Select';

import { errors } from 'utils/validators';

import './create-league.scss';

const FormInput = Input.form;
const FormSelect = Select.form;
const Option = Select.Option;
const dateFormat = 'YYYY/MM/DD';
const FORM_NAME = 'CREATE_LEAGUE';

function disabledDate(current) {
  // Can not select days before today and today
  return current && current < Date.now();
}

function renderDatePicker(props) {
  const {
    input: { onChange, value }
  } = props;
  const defaultValue = moment(value).isValid() ? value : moment();
  return (
    <div className="form-input input-field" style={{ width: '100%' }}>
      <DatePicker
        onChange={onChange}
        disabledDate={disabledDate}
        defaultValue={defaultValue}
        format={dateFormat}
      />
    </div>
  );
}

let WrappedCreateForm = props => {
  const {
    invalid,
    handleSubmit,
    onSubmit,
    gameWeeksDetails,
    currentWeek
  } = props;
  const inputLayout = {
    marginBottom: 20
  };

  const renderSelectField = ({ input: { onChange, name, value } }) => {
    return (
      <Select
        name={name}
        id={name}
        className="form-component-wrapper"
        placeholder="Select a Gameweek"
        onChange={onChange}
        defaultValue={value}
      >
        <Option value="">Select Gameweek</Option>
        {gameWeeksDetails.all.map(week => {
          let isDisabled = week.id < currentWeek;

          return (
            <Option
              key={`league-end-key-${week.id}`}
              disabled={isDisabled}
              value={week.id}
            >
              {week.name}
            </Option>
          );
        })}
      </Select>
    );
  };

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="login-form container create-league-form"
    >
      <div className="wrapper row form-row">
        <label className="text-label ">
          <Text color="black">League Name</Text>
        </label>
        <Field
          className="input-field"
          name="leagueName"
          placeholder="Enter the name of your league"
          type="text"
          component={FormInput}
          formLayout={inputLayout}
          validate={[errors.leagueNameRequired]}
        />
      </div>
      <div className=" wrapper row form-row">
        <label className="text-label">
          <Text color="black">League type</Text>
        </label>
        <Field
          className="form-input input-field"
          name="leagueType"
          placeholder="League Type"
          type="text"
          component={FormSelect}
          formLayout={inputLayout}
          validate={[errors.isRequired]}
        >
          <Select.Option value="private">Private League</Select.Option>
          <Select.Option value="public">Public League</Select.Option>
        </Field>
      </div>
      <div className="wrapper dropdown-item row form-row">
        <div className="row form-row">
          <label className="text-label league-start">
            <Text color="black">League Starts</Text>
          </label>
          <Field
            className="form-component-wrapper"
            name="leagueStart"
            required
            placeholder="input start date"
            component={renderSelectField}
            formLayout={inputLayout}
          />
        </div>
        <div className="row form-row">
          <label className="text-label">
            <Text color="black">League Ends</Text>
          </label>
          <Field
            className="form-component-wrapper"
            name="leagueEnd"
            required
            component={renderSelectField}
            formLayout={inputLayout}
          />
        </div>
      </div>

      <div className="wrapper row form-row">
        <div className="row form-row col-xs-12">
          <label className="text-label" htmlFor="lastGameEntry">
            <Text color="black">Closing Week</Text>
          </label>
          <div>
            <Field
              className="form-component-wrapper"
              name="lastGameEntry"
              placeholder="Enter the name of your league"
              component={renderDatePicker}
              formLayout={inputLayout}
            />
            <div className="input-hint">
              <Text size="small2">
                * Managers will no longer be able to join the league after this
                date
              </Text>
            </div>
          </div>
        </div>
        <div className="row form-row col-xs-12 max-praticipant-item">
          <label className="max-item-label text-label">
            <Text color="black">
              Maximum Participants <span className="subtext">(optional)</span>
            </Text>
          </label>
          <Field
            className="form-input input-field"
            name="maximumParticipants"
            placeholder="Max participants"
            type="number"
            component={FormInput}
            formLayout={inputLayout}
          />
        </div>
      </div>
      <div className="auth-btn-container">
        <Button
          color="purple"
          className="form-btn"
          disabled={invalid}
          htmlType="submit"
          onClick={handleSubmit}
          size="large"
        >
          <Text classText="btn-text" color="white">
            PROCEED
          </Text>
        </Button>
      </div>
    </form>
  );
};

WrappedCreateForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string
};

WrappedCreateForm = connect((state, x) => {
  const { form } = state;
  const { leagueStart } = form[FORM_NAME].values;

  return {
    leagueStart
  };
})(WrappedCreateForm);

export const CreateForm = reduxForm({
  form: FORM_NAME
})(WrappedCreateForm);
