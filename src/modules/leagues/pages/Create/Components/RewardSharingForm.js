import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Text } from 'components';
// import Input from 'components/Input';
import Select from 'components/Select';

import './create-league.scss';

// const FormInput = Input.form;
const FormSelect = Select.form;
const WrappedRewardSharingForm = props => {
  const { invalid, onSubmit, handleSubmit } = props;

  const inputLayout = {
    marginBottom: 20
  };
  // const handleCheck = type => {
  //   props.toggleRadio(type);
  // };
  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="login-form container reward-sharing-form"
    >
      {/* <p className="subtext display-mobile">
        <span><a href="/terms">Terms and condition</a></span> apply
      </p> */}
      <div className="row">
        <div className="radio-btn" />
        <div className="col-xs-12 col-sm-11 row rs-system-ratio">
          <div className="col-xs-12 col-sm-2 tag-box">
            <Text color="black">System Ratio</Text>
          </div>
          <div className="col-sm-10 col-xs-12">
            <Field
              className="form-input form-row"
              name="rewardType"
              placeholder="reward ratio"
              type="text"
              component={FormSelect}
              formLayout={inputLayout}
              disabled={!props.isSystem}
            >
              <Select.Option value="">Select Reward</Select.Option>
              <Select.Option value="type-1">50% : 30% : 20% </Select.Option>
              <Select.Option value="type-2">50% : 35% : 15% </Select.Option>
              <Select.Option value="type-3">50% : 40% : 10% </Select.Option>
              <Select.Option value="type-4">50% : 45% : 5% </Select.Option>
              <Select.Option value="type-5">55% : 45% : 0% </Select.Option>
              <Select.Option value="type-6">60% : 20% : 20% </Select.Option>
              <Select.Option value="type-7">60% : 25% : 15% </Select.Option>
              <Select.Option value="type-8">60% : 30% : 10% </Select.Option>
              <Select.Option value="type-9">60% : 40% : 0% </Select.Option>
              <Select.Option value="type-10">70% : 15% : 15% </Select.Option>
              <Select.Option value="type-11">70% : 20% : 10% </Select.Option>
              <Select.Option value="type-12">70% : 30% : 0% </Select.Option>
              <Select.Option value="type-13">80% : 15% : 5% </Select.Option>
              <Select.Option value="type-14">80% : 20% : 0% </Select.Option>
              <Select.Option value="type-15">90% : 10% : 0% </Select.Option>
              <Select.Option value="type-16">
                100% : 0% : 0% (Winner takes all)
              </Select.Option>
            </Field>
          </div>
        </div>
      </div>
      {/* <div className="row custom-wrapper">
        <div className="radio-btn">
          <input
            type="radio"
            name="itemtoggled"
            value="custom-type"
            onChange={() => handleCheck('custom-type')}
            className="col-xs-10 col-sm-1"
          />
        </div>
        <div className="col-sm-11 col-xs-10 row">
          <div className="col-xs-12 col-sm-2 tag-box mobile">
            <Text color="black">Custom Ratio</Text>
          </div>
          <div className="col-sm-4 col-xs-12 row custom-input">
            <div className="col-sm-5 col-xs-12 rs-label">
              <Text color="black">1st position</Text>
            </div>
            <div className="col-sm-7 col-xs-12 rs-field ">
              <Field
                className="form-input"
                name="firstPositionPrice"
                placeholder="percentile"
                type="number"
                component={FormInput}
                formLayout={inputLayout}
                disabled={props.isSystem}
              />
            </div>
          </div>
          <div className="col-sm-4 col-xs-12 row custom-input">
            <div className="col-sm-5 col-xs-12 rs-label">
              <Text color="black">2nd position</Text>
            </div>
            <div className="col-sm-7 col-xs-12 rs-field">
              <Field
                className="form-input"
                name="secondPositionPrice"
                placeholder="percentile"
                type="number"
                component={FormInput}
                formLayout={inputLayout}
                disabled={props.isSystem}
              />
            </div>
          </div>
          <div className="col-sm-4 col-xs-12 row custom-input">
            <div className="col-sm-5 col-xs-12 rs-label">
              <Text color="black">3rd position</Text>
            </div>
            <div className="col-sm-7 col-xs-12 rs-field">
              <Field
                className="form-input"
                name="thirdPositionPrice"
                placeholder="percentile"
                type="number"
                component={FormInput}
                formLayout={inputLayout}
                disabled={props.isSystem}
              />
            </div>
          </div>
        </div>
      </div> */}
      <p className="subtext">
        <span>
          <a href="/terms">Terms and condition</a>
        </span>{' '}
        apply
      </p>
      {/* <div className="row form-row man-of-the-month">
        <label className="col-xs-12 col-sm-3 text-label">
          <Text color="black">
            Manager of the month <span>(optional)</span>
          </Text>
        </label>
        <Field
          className="col-xs-12 col-sm-8"
          name="managerOfTheMonthReward"
          placeholder="Enter the name of your league"
          type="text"
          component={FormSelect}
          formLayout={inputLayout}
        />
      </div> */}

      <div className="auth-btn-container">
        <Button
          color="purple"
          className="form-btn"
          disabled={invalid}
          htmlType="submit"
          onClick={handleSubmit}
          size="large"
        >
          <Text classText="btn-text" color="white">
            PROCEED
          </Text>
        </Button>
      </div>
    </form>
  );
};

WrappedRewardSharingForm.defaultProps = {
  isSystem: true
};

WrappedRewardSharingForm.propTypes = {
  onSubmit: PropTypes.func,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string,
  toggleRadio: PropTypes.func
};

export const RewardSharingForm = reduxForm({
  form: 'REWARD_FORM'
})(WrappedRewardSharingForm);
