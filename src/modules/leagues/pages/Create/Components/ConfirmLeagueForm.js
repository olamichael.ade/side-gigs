import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { Text } from 'components';
import { errors } from 'utils/validators';
import { CREATE_LEAGUE_FORM } from 'modules/leagues/constants';
import { CardForm } from 'modules/leagues/components/CardForm';
import './create-league.scss';

const WrappedConfirmLeagueForm = props => {
  const { onSubmit, newerror, errorMessage, user, creating } = props;
  const wallet = (user && user.wallet) || 0;

  const minWalletValidator = errors.minWithdrawalAmount(wallet);

  return (
    <div className="login-form container confirm-league-form">
      {newerror && (
        <div className="league-message-box error">{errorMessage}</div>
      )}
      <div className="row form-row">
        <label className="col-xs-12 text-label participation-text">
          <Text color="black">Participation Fee</Text>
        </label>
        <CardForm
          className="form-input col-xs-12"
          validator={[minWalletValidator, errors.minAmount]}
          form="WITHDRAWAL_FORM"
          onSubmit={amount => {
            onSubmit(amount);
          }}
          creating={creating}
          buttonLabel={creating ? 'Creating...' : 'CREATE MY LEAGUE'}
        />
      </div>
    </div>
  );
};

WrappedConfirmLeagueForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string,
  newerror: PropTypes.bool,
  errorMessage: PropTypes.string
};

export const ConfirmLeagueForm = reduxForm({
  form: CREATE_LEAGUE_FORM,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(WrappedConfirmLeagueForm);
