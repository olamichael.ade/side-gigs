import React, { Component } from 'react';
import { Modal } from 'antd';
import { CreateForm, ConfirmLeagueForm, RewardSharingForm } from './Components';
import NumberTab from '../../components/NumberTab';
import { getRewardObject, computeReward } from 'utils/getReward';
import NumberRowTab from '../../components/numberRowTab';

const confirm = Modal.confirm;

class Create extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      league: {},
      isSystem: true
    };
  }

  handleChangeIndex = number => {
    this.setState({ step: number });
  };

  componentDidMount() {
    this.props.gameWeeks();
  }

  handleProceed = () => {
    this.setState({ step: this.state.step + 1 });
  };

  handleSubmit = creds => {
    const { league, step } = this.state;
    if (this.state.isSystem && creds.rewardType) {
      const rewardRatio = getRewardObject(creds.rewardType);
      creds.reward = rewardRatio;
    } else if (!this.state.isSystem) {
      const rewardRatio = computeReward(creds);
      creds.reward = rewardRatio;
    }
    const newLeague = { ...league, ...creds };
    this.setState({ league: newLeague });
    return step === 3 ? this.handleCreate(newLeague) : this.handleProceed();
  };

  handleToggleRadio = type => {
    this.setState({ isSystem: type === 'system-type' });
  };

  handleCreate = league => {
    const { history } = this.props;

    return this.props.createLeague(league).then(message => {
      if (message.statusOk) {
        return history.push(`invite`);
      }
    });
  };

  showConfirm = () => {
    const { history } = this.props;

    confirm({
      title: 'Insufficient Funds',
      content:
        'You have insufficient funds to create a league. Do you want to fund your wallet?',
      onOk() {
        history.push('/wallet');
      },
      onCancel() {
        console.log('Cancel fund wallet option');
      }
    });
  };

  render() {
    let Component, Title;
    switch (this.state.step) {
      case 1:
        Component = CreateForm;
        Title = 'Create League';
        break;
      case 2:
        Component = RewardSharingForm;
        Title = 'Reward Sharing';
        break;
      case 3:
        Component = ConfirmLeagueForm;
        Title = 'Confirm League';
        break;
      default:
        Component = CreateForm;
        Title = 'Create League';
        break;
    }
    const componentProps = {
      formInput: {},
      onSubmit: this.handleSubmit,
      isSystem: this.state.isSystem,
      toggleRadio: this.handleToggleRadio,
      reload: this.handleReload,
      ...this.props
    };

    if (this.state.step !== 2) {
      delete componentProps.isSystem;
      delete componentProps.toggleRadio;
    }

    return (
      <div className="main-body">
        <div className="create-league-wrapper">
          <NumberRowTab title={Title}>
            <NumberTab
              changeIndex={this.handleChangeIndex}
              selected={this.state.step}
            />
          </NumberRowTab>
          <Component
            {...componentProps}
            newerror={this.props.error}
            initialValues={this.state.league}
          />
        </div>
      </div>
    );
  }
}

export default Create;
