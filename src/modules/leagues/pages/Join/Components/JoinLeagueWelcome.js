import React, { PureComponent } from 'react';
import { FacebookIcon, TwitterIcon, WhatsappIcon } from 'react-share';
import { Button, Text } from 'components';
import PropTypes from 'prop-types';
import './joinleaguewelcome.scss';

class JoinLeagueDetails extends PureComponent {
  render() {
    const { league } = this.props;

    const baseUrl = window.location.origin;

    return (
      <div className="welcome-container">
        <div className="jl-heading">
          You have successfully joined {league.name}
        </div>
        <div className="btn-container">
          <Button
            color="purple"
            className="auth-form-button"
            htmlType="submit"
            size="large"
            onClick={this.props.handleDone}
          >
            <Text color="white" classText="btn-text">
              DONE
            </Text>
          </Button>
        </div>
        <div className="comms-container">
          <p className="comms-text">
            Help spread the word and join our community:
          </p>
          <div className="comms-icon-container">
            <a
              className="share-icon"
              href={`https://twitter.com/intent/tweet?text=Join ${
                league.name
              } now! ${baseUrl}/league/join?code=${league.code}`}
              target="__blank"
            >
              <TwitterIcon size={32} round />
            </a>
            <a
              className="share-icon"
              href="https://web.facebook.com/fansfantasyleague/"
              target="__blank"
            >
              <FacebookIcon size={32} round />
            </a>
            <a
              className="share-icon"
              href=" https://chat.whatsapp.com/EzODJJRu0IZFQeiikiiDjk"
              target="__blank"
            >
              <WhatsappIcon size={32} round />
            </a>
          </div>
        </div>
      </div>
    );
  }
}
JoinLeagueDetails.defaultProps = {
  league: {
    type: 'Open League',
    fee: '$1000'
  }
};

JoinLeagueDetails.Proptypes = {
  league: PropTypes.object
};
export default JoinLeagueDetails;
