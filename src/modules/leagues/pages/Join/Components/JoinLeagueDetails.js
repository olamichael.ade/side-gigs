import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Spin, Alert } from 'antd';
import { isEmpty } from 'lodash';
import { Button, Text } from 'components';
import { getDayMonth, currencyFormatter } from 'utils/utils';
import './joinleaguedetails.scss';

class JoinLeagueDetails extends PureComponent {
  noResult() {
    return (
      <div className="center">
        No result found.{' '}
        <span
          className="pull-right back-text"
          onClick={() => {
            this.props.history.goBack();
          }}
        >
          Back
        </span>
      </div>
    );
  }

  renderContent = () => {
    const { league, loaded, error, errorMessage, clearError } = this.props;
    const {
      type,
      fee,
      start,
      end,
      close,
      managers,
      limit,
      rewards,
      prize,
      isRegular,
      state
    } = league || {};

    const isEnded = state === 'ended';

    const [firstPosition, secondPosition, thirdPosition] = rewards || [];

    return loaded ? (
      <Fragment>
        {error && (
          <Alert
            className="error-box"
            message={errorMessage}
            type="error"
            closable
            onClose={clearError}
          />
        )}
        {isEnded && (
          <div>
            <div className="league-heading">
              Sorry, {league.name} has ended.
            </div>
          </div>
        )}
        {!isEnded && (
          <Fragment>
            <div className="league-heading">League Details</div>
            <hr />
            <div className="league-in">
              <div className="detail-item margin-bottom">
                <div className="ld-caption">League Type: </div>
                <div className="ld-text">{type}</div>
              </div>
              <div className="detail-item margin-bottom">
                <div className="ld-caption">Participation fee: </div>
                <div className="ld-text"> N {currencyFormatter(fee)}</div>
              </div>
              <div className="detail-item margin-bottom">
                <div className="ld-caption">Game Duration:</div>
                <div className="ld-text">
                  {' '}
                  Gameweek {start} - Gameweek {end}
                </div>
              </div>
              <div className="detail-item margin-bottom">
                <div className="ld-caption">Reg. Close:</div>
                <div className="ld-text">{getDayMonth(close)}</div>
              </div>
              <div className="detail-item margin-bottom">
                <div className="ld-caption">Total Joined:</div>
                <div className="ld-text">
                  {managers} Manager
                  {managers === 1 ? '' : 's'}
                </div>
              </div>
              <div className="detail-item margin-bottom">
                <div className="ld-caption">Max. Participants:</div>
                <div className="ld-text">{limit || 'N/A'} </div>
              </div>
              {!isRegular && (
                <div className="detail-item margin-bottom">
                  <div className="ld-caption">Prize:</div>
                  <div className="ld-text">N {currencyFormatter(prize)}</div>
                </div>
              )}
              {isEmpty(rewards) ? (
                <Fragment>
                  <div className="detail-item">
                    <div className="ld-caption">Reward Sharing:</div>
                    <div className="ld-text">winner takes all</div>
                  </div>
                </Fragment>
              ) : (
                <Fragment>
                  <div className="detail-item">
                    <div className="ld-caption">Reward Sharing:</div>
                    <div className="ld-text">
                      {firstPosition ? firstPosition.reward : 0}% Winner
                    </div>
                  </div>
                  <div className="detail-item">
                    <div className="ld-caption" />
                    <div className="ld-text">
                      {secondPosition ? secondPosition.reward : 0}% 1st Runner
                      up
                    </div>
                  </div>
                  <div className="detail-item">
                    <div className="ld-caption" />
                    <div className="ld-text">
                      {thirdPosition ? thirdPosition.reward : 0}% 2nd Runner up
                    </div>
                  </div>
                </Fragment>
              )}
            </div>
            <div className="btn-container">
              <Button
                color="purple"
                className="auth-form-button"
                htmlType="submit"
                size="large"
                onClick={this.props.handleDone}
              >
                <Text color="white" classText="btn-text">
                  JOIN LEAGUE
                </Text>
              </Button>
            </div>
          </Fragment>
        )}
      </Fragment>
    ) : (
      this.noResult()
    );
  };

  render() {
    const { loading } = this.props;

    return (
      <div className="invite-body">
        {loading ? <Spin size="large" /> : this.renderContent()}
      </div>
    );
  }
}
JoinLeagueDetails.defaultProps = {
  league: {
    type: 'Open League',
    fee: '1000'
  }
};

JoinLeagueDetails.Proptypes = {
  league: PropTypes.object
};
export default JoinLeagueDetails;
