import React, { Component } from 'react';
import { Button, Text } from 'components';
import { Field, reduxForm } from 'redux-form';
import Input from 'components/Input';
import { SEARCH_LEAGUE_FORM } from 'modules/leagues/constants';
import { errors } from 'utils/validators';
import './joinleagueform.scss';

const FormInput = Input.form;

class WrappedJoinLeagueForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copied: false
    };
  }

  render() {
    const { onSubmit, handleSubmit } = this.props;
    const inputLayout = {
      marginBottom: 20
    };

    return (
      <div className="join-league-container">
        <form
          onSubmit={() => handleSubmit(onSubmit)}
          className="login-form container join-league-form"
        >
          <div className="row form-row">
            <label className="col-xs-12 text-label">
              <Text color="black">League Code</Text>
            </label>
            <Field
              className="form-input col-xs-12"
              name="code"
              placeholder="Enter league code here"
              type="text"
              component={FormInput}
              formLayout={inputLayout}
              validate={[errors.isRequired]}
            />
          </div>

          <div className="auth-btn-container">
            <Button
              color="purple"
              className="auth-form-button"
              disabled={false}
              htmlType="submit"
              size="large"
            >
              <Text color="white" classText="btn-text">
                {this.props.creating ? 'Searching...' : 'SEARCH'}
              </Text>
            </Button>
          </div>
        </form>
      </div>
    );
  }
}
WrappedJoinLeagueForm.defaultProps = {
  // code:
};

const Join = reduxForm({
  form: SEARCH_LEAGUE_FORM,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(WrappedJoinLeagueForm);

export default Join;
