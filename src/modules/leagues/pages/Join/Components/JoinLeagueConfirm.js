import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'antd';
import { Button, Text, LinkedText } from 'components';
import { currencyFormatter } from 'utils/utils';
import './joinleagueconfirm.scss';

class JoinLeagueForm extends PureComponent {
  render() {
    const {
      league: { fee },
      user,
      handlePay,
      joining,
      errorMessage,
      error
    } = this.props;

    const showAlert = error || user.wallet < fee;
    const message =
      user.wallet < fee ? (
        <Text>
          You don't have sufficient credit to join league. Click{' '}
          <LinkedText to="/wallet">here</LinkedText> to fund wallet.
        </Text>
      ) : (
        errorMessage
      );

    return (
      <div className="join-league-container">
        {showAlert && (
          <div className="error-container">
            <Alert message={message} type="error" showIcon />
          </div>
        )}
        <div className="ld-confirmation-container">
          <div className="row form-row">
            <label className="col-xs-12 text-label">
              <Text color="black">
                Participation Fee: NGN {currencyFormatter(fee)}
              </Text>
            </label>
          </div>
          <div className="auth-btn-container">
            <Button
              color="purple"
              className="auth-form-button"
              disabled={user.wallet < fee}
              htmlType="submit"
              size="large"
              onClick={handlePay}
            >
              <Text color="white" classText="btn-text">
                {joining ? 'Processing...' : 'PAY'}
              </Text>
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
JoinLeagueForm.defaultProps = {
  user: {
    wallet: 0
  }
};

JoinLeagueForm.propTypes = {
  fee: PropTypes.number,
  user: PropTypes.object
};

export default JoinLeagueForm;
