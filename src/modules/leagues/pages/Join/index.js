import { compose } from 'redux';
import { connect } from 'react-redux';
import {
  searchLeague,
  findLeagueById,
  resetLeague,
  updateJoinLeague,
  joinLeague,
  clearError
} from '../../store/actions';

import Join from './join';

const mapStateToProps = state => {
  const { auth, leagues } = state;
  return {
    user: auth.user,
    leagueDetails: leagues.joinleagueDetails,
    league: leagues.league,
    joining: leagues.joining,
    error: leagues.error,
    errorMessage: leagues.errorMessage
  };
};

export default compose(
  connect(
    mapStateToProps,
    {
      searchLeague,
      findLeagueById,
      resetLeague,
      updateJoinLeague,
      joinLeague,
      clearError
    }
  )
)(Join);
