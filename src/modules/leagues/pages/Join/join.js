import React, { Component } from 'react';
import { parse } from 'qs';
import { Modal } from 'antd';
import TopNavBar from '../../components/topNav';
import JoinLeagueForm from './Components/JoinLeagueForm';
import JoinLeagueDetails from './Components/JoinLeagueDetails';
import JoinLeagueConfirm from './Components/JoinLeagueConfirm';
import JoinLeagueWelcome from './Components/JoinLeagueWelcome';
// import './join.scss';

const confirm = Modal.confirm;

class Join extends Component {
  state = {
    loading: false,
    loaded: false
  };

  componentDidMount() {
    this.getLeague();
  }

  updateProfile() {
    confirm({
      title: 'Your phone number is missing!',
      content:
        'You have not updated your profile with your phone number. Kindly take a moment to update it. We need your phone number to contact you incase you win.',
      onOk: () => {
        return this.props.history.push('/profile');
      },
      okText: 'Update Profile',
      onCancel() {}
    });
  }

  getLeague = () => {
    const { location } = this.props;
    const query = parse(location.search.substr(1));

    this.setState({ loading: true, loaded: false });
    if (query.code) {
      this.props
        .searchLeague(query.code)
        .then(response => {
          this.setState({ loading: false, loaded: true });
          this.props.updateJoinLeague({ step: 2 });
        })
        .catch(err => {
          this.setState({ loading: false, loaded: true });
          this.props.updateJoinLeague({ step: 1 });
        });
    } else {
      this.setState({ loading: false, loaded: true });
      this.props.updateJoinLeague({ step: 1 });
    }
  };

  componentWillUnmount() {
    this.props.resetLeague();
  }

  onSubmit = payload => {
    this.props.searchLeague(payload);
  };

  handleDone = () => {
    const { league, joinLeague, updateJoinLeague, user } = this.props;

    if (!user.phone) return this.updateProfile();

    if (league.fee === 0) {
      return joinLeague(league._id)
        .then(res => {
          if (res.message) {
            updateJoinLeague({ step: 4 });
          }
        })
        .catch(err => {});
    }

    return updateJoinLeague({ step: 3 });
  };

  handleComplete = () => {
    this.props.history.push('/leagues/me');
  };

  handlePay = () => {
    const { joinLeague, league, updateJoinLeague } = this.props;

    return joinLeague(league._id).then(response => {
      if (response.message) {
        return updateJoinLeague({ step: 4 });
      }
    });
  };

  render() {
    const { leagueDetails, league } = this.props;
    return (
      <div className="join-league-container">
        {leagueDetails.step !== 4 && (
          <TopNavBar
            showTabs={false}
            leagueName={league.name ? league.name : 'Join League'}
          />
        )}
        {leagueDetails.step === 1 && (
          <JoinLeagueForm onSubmit={this.onSubmit} {...this.props} />
        )}
        {leagueDetails.step === 2 && (
          <JoinLeagueDetails
            handleDone={this.handleDone}
            loading={this.state.loading}
            loaded={this.state.loaded}
            {...this.props}
          />
        )}
        {leagueDetails.step === 3 && (
          <JoinLeagueConfirm handlePay={this.handlePay} {...this.props} />
        )}
        {leagueDetails.step === 4 && (
          <JoinLeagueWelcome handleDone={this.handleComplete} {...this.props} />
        )}
      </div>
    );
  }
}

export default Join;
