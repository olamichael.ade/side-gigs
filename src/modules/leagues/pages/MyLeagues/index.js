import { connect } from 'react-redux';
import { compose } from 'redux';
import { LoadComponent } from 'HOCS';
import { dismissModal, loadLeagues } from '../../store/actions';

import Dashboard from './Dashboard';

const mapStateToProps = state => {
  const {
    leagues,
    auth: { user }
  } = state;
  const { teamName = '' } = user || {};
  return {
    loading: leagues.isAjaxReq,
    loaded: leagues.isReqComplete,
    leagues: leagues.leagues,
    joinedLeagues: leagues.joinedLeagues,
    error: leagues.error,
    success: leagues.success,
    errorMessage: leagues.errorMessage,
    showModal: leagues.showModal,
    teamName: teamName
  };
};

export default compose(
  connect(
    mapStateToProps,
    { load: loadLeagues, dismissModal }
  ),
  LoadComponent
)(Dashboard);
