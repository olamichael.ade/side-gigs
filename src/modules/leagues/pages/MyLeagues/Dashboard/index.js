import React, { Component } from 'react';
import { Steps, Icon } from 'antd';
import { Text, Button } from 'components';
import TopNavBar from 'modules/leagues/components/topNav';
import LeagueTables from '../../../components/LeagueTables';
import './Dashboard.scss';

const Step = Steps.Step;

class Dashboard extends Component {
  renderContent() {
    const { teamName } = this.props;
    return (
      <div>
        <TopNavBar showTabs leagueName={teamName} />
        <LeagueTables {...this.props} />
      </div>
    );
  }

  renderWelcome() {
    return (
      <div className="welcome-body">
        <div className="intermediate-title-container">
          <Text classText="intermediate-title" weight="med2">
            Welcome!
          </Text>
        </div>
        <div className="intermediate-title-container">
          <Text classText="intermediate-subtitle">
            Before you begin, review the following steps to make this as smooth
            as possible
          </Text>
        </div>
        <Steps direction="vertical" className="steps">
          <Step
            status="finish"
            icon={<Icon type="wallet" />}
            title="Fund your wallet"
            description="You'll need to fund your wallet before using this platform optimally."
          />
          <Step
            status="finish"
            icon={<Icon type="usergroup-add" />}
            title="Create League"
            description="Create the league you want others to join. You can set the fee for joining the league you created."
          />
          <Step
            status="finish"
            icon={<Icon type="search" />}
            title="Join League"
            description="You can join a league using an invite code or going through our open leagues and join and show them you're the best manager"
          />
        </Steps>
        <div className="welcome-btn-container">
          <Button color="purple" onClick={this.goToRoute}>
            <Text classText="btn-text" color="white">
              GET STARTED
            </Text>
          </Button>
        </div>
      </div>
    );
  }

  render() {
    return <div className="main-body">{this.renderContent()}</div>;
  }
}

export default Dashboard;
