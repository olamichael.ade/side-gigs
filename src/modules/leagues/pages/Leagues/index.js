import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { LoadComponent, RefreshComponent } from 'HOCS';
import { loadTrendingLeagues, refreshTrending } from '../../store/actions';

import Dashboard from './components/Dashboard';

const mapStateToProps = state => {
  const {
    leagues,
    auth: { user }
  } = state;
  return {
    loading: leagues.isAjaxReq,
    loaded: leagues.isReqComplete,
    trendingLeagues: leagues.trendingLeagues,
    error: leagues.error,
    success: leagues.success,
    errorMessage: leagues.errorMessage,
    user
  };
};

export default compose(
  connect(
    mapStateToProps,
    { load: loadTrendingLeagues, refresh: refreshTrending }
  ),
  LoadComponent,
  RefreshComponent,
  withRouter
)(Dashboard);
