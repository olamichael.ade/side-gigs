import React from 'react';
import { Link } from 'react-router-dom';
import { Tag } from 'antd';
import { Text } from 'components';

import './AvailableLeagues.scss';

const getColumns = userId => {
  return [
    {
      title: <Text classText="jc-col-header">League Name</Text>,
      align: 'left',
      dataIndex: 'title',
      className: 'jc-col',
      render: (text, record) => (
        <Text classText="jc-title" color="black">
          {record.name}
        </Text>
      )
    },
    {
      title: <Text classText="jc-col-header">Game Duration</Text>,
      align: 'left',
      dataIndex: 'start',
      className: 'jc-col gw',
      render: (text, record) => (
        <span>
          <Text classText="jc-value-text gw">{`Gameweek ${record.start} - ${
            record.end
          }`}</Text>
          <Text classText="jc-value-text-mobile">{`GW ${record.start}-${
            record.end
          }`}</Text>
        </span>
      )
    },
    {
      title: <Text classText="jc-col-header">Fee</Text>,
      align: 'right',
      dataIndex: 'fee',
      className: 'jc-col',
      render: (t, r) => <Text classText="jc-value-text">{r.fee || 0}</Text>
    },
    {
      title: <Text classText="jc-col-header">Managers</Text>,
      align: 'center',
      dataIndex: 'managers',
      className: 'jc-col',
      render: (t, r) => <Text classText="jc-value-text">{r.managers}</Text>
    },
    {
      title: '',
      align: 'right',
      dataIndex: 'accept',
      className: 'jc-col',
      render: (t, r) =>
        r.owner === userId ? (
          <Tag color="green">Owner</Tag>
        ) : (
          <Link to={`/leagues/join?code=${r.code}`}>
            <Text classText="accept-text" color="black">
              view
            </Text>
          </Link>
        )
    }
  ];
};

export { getColumns };
