import React from 'react';
import Header from 'modules/leagues/components/Header';
import AvailableLeagues from './AvailableLeagues';
import './Dashboard.scss';

const Dashboard = props => {
  const { teamName } = props.user || {};
  return (
    <div className="main-body">
      <Header title={teamName} />
      <AvailableLeagues {...props} />
    </div>
  );
};

export default Dashboard;
