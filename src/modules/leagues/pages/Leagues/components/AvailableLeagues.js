import React from 'react';
import { Table } from 'antd';
import { Text } from 'components';
import { getColumns } from './AvailableLeaguesColumn';

import './AvailableLeagues.scss';

const AvailableLeagues = props => {
  const { trendingLeagues, user } = props;
  const { _id } = user || {};

  const columns = getColumns(_id);
  return (
    <div>
      <section className="ch2h-container">
        <div className="form-title-container">
          <Text classText="ch2h-form-title" weight="med">
            Trending Leagues
          </Text>
        </div>
        <div>
          <Table
            rowKey={record => record._id}
            dataSource={trendingLeagues}
            columns={columns}
            locale={{ emptyText: 'No Trending Leagues' }}
          />
        </div>
      </section>
    </div>
  );
};

export default AvailableLeagues;
