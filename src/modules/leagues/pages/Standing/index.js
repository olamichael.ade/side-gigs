import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { LoadComponent, RefreshComponent } from 'HOCS';
import { loadLeagueStanding, refreshStanding } from '../../store/actions';
import Standing from './Components/StandingTable';

const mapStateToProps = (state, router) => {
  const {
    leagues,
    auth: { user }
  } = state;
  const { params } = router.match;
  const { teamName = '' } = user || {};
  return {
    loading: leagues.isAjaxReq,
    loaded: leagues.isReqComplete,
    loadParam: params.leagueId,
    standings: leagues.standings,
    league: leagues.league,
    teamName
  };
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { load: loadLeagueStanding, refresh: refreshStanding }
  ),
  LoadComponent,
  RefreshComponent
)(Standing);
