import React from 'react';
import { Collapse, Table, Spin, Row, Col } from 'antd';
import { isEmpty, sortBy, head, last } from 'lodash';
import TopNavBar from '../../../components/topNav';
import LeagueDetail from './LeagueDetail';
import { Text } from 'components';

import StandingColumn from './StandingColumn';
import './StandingTable.scss';

const Panel = Collapse.Panel;

const calculateCurrentPeriod = standings => {
  return standings.map((ls, idx) => {
    const { gameweeks } = ls || {};

    let currentDuration;

    if (gameweeks.length === 1) {
      const currentGameWeek = gameweeks[0] || {};
      currentDuration = `GW ${currentGameWeek.event}` || '-';
    }

    if (gameweeks.length > 1) {
      const start = head(gameweeks);
      const end = last(gameweeks);

      const startWeek = start && start.points ? start.event : '';
      const endWeek = end && end.points ? end.event : '';

      currentDuration = `GW ${startWeek}- GW ${endWeek}`;
    }

    return { ...ls, currentDuration };
  });
};

const StandingTable = props => {
  const { standings, loading, teamName, league } = props;

  const enhancedData = calculateCurrentPeriod(standings);

  const leagueStanding = sortBy(enhancedData, [ls => -ls.totalScore]).map(
    (standing, idx) => ({ ...standing, key: idx + 1 })
  );

  const { currentDuration = '' } = leagueStanding[0] || {};

  const renderContent = isEmpty(standings) ? (
    <div className="c-loading-container">
      <Text>{`League standings is not yet available`}</Text>
    </div>
  ) : (
    <Row type="flex" gutter={16}>
      <Col
        xs={{ span: 24, order: 2 }}
        sm={{ span: 24, order: 2 }}
        md={{ span: 14, order: 0 }}
      >
        <div className="league-standing-table">
          <Collapse bordered defaultActiveKey={['1']}>
            <Panel
              className="ls-panel"
              header={
                <Text classText="c-panel-title" weight="med">
                  {`Overall Standing (${currentDuration})`}
                </Text>
              }
              key={1}
            >
              <Table
                className="c-challenge-list"
                rowKey={record => record.userId}
                columns={StandingColumn}
                dataSource={leagueStanding}
                size="middle"
              />
            </Panel>
          </Collapse>
        </div>
      </Col>
      <Col
        xs={{ span: 24, order: 1 }}
        sm={{ span: 24, order: 1 }}
        md={{ span: 10, order: 0 }}
      >
        <LeagueDetail league={league} />
      </Col>
    </Row>
  );

  return (
    <div>
      <TopNavBar leagueName={teamName} />
      {loading ? (
        <div className="league-standing-container loader">
          <Spin />
        </div>
      ) : (
        <div className="league-standing-container">{renderContent}</div>
      )}
    </div>
  );
};

export default StandingTable;
