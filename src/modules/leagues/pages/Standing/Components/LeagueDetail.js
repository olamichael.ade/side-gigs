import React from 'react';
import { Text } from 'components';
import { getDayMonth, currencyFormatter } from 'utils/utils';
import './LeagueDetail.scss';

const LeagueDetail = ({ league }) => {
  const { type, fee, start, end, managers, close, name } = league;
  return (
    <div className="league-detail-container">
      <div className="ld-header">
        <Text classText="ld-title" weight="med">
          League Details
        </Text>
      </div>
      <div className="ld-body">
        <div className="league-item">
          <Text classText="label-text">League Name: </Text>
          <Text className="item-value">{name}</Text>
        </div>
        <div className="league-item">
          <Text classText="label-text">League Type: </Text>
          <Text className="item-value">{type}</Text>
        </div>
        <div className="league-item">
          <Text classText="label-text">Participation Fee: </Text>
          <Text className="item-value">{`NGN ${currencyFormatter(fee)}`}</Text>
        </div>
        <div className="league-item">
          <Text classText="label-text">Game Duration: </Text>
          <Text className="item-value">
            GW {start} - GW {end}
          </Text>
        </div>
        <div className="league-item">
          <Text classText="label-text">Reg. Close: </Text>
          <Text className="item-value">{getDayMonth(close)}</Text>
        </div>
        <div className="league-item">
          <Text classText="label-text">Total Joined: </Text>
          <Text className="item-value">
            {managers} Manager
            {managers === 1 ? '' : 's'}
          </Text>
        </div>
      </div>
    </div>
  );
};

export default LeagueDetail;
