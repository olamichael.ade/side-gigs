import React from 'react';
import { Text } from 'components';

import './Column.scss';

// const { ICON_LOCK, ICON_UNLOCK } = Icon;

const getGameweek = (gameWeeks = []) => {
  return gameWeeks.reduce((memo, gw) => {
    const { points, event } = gw;

    return points && event ? gw : memo;
  }, {});
};

export default [
  {
    title: 'S/N',
    align: 'left',
    dataIndex: 'sn',
    render: (text, record) => <Text color="black">{record.key}</Text>
  },
  {
    title: 'Teams',
    align: 'left',
    dataIndex: 'name',
    render: (text, record) => (
      <Text classText="c-col-title" color="black">
        {record.name}
      </Text>
    )
  },
  {
    title: 'Current GW Pt.',
    align: 'center',
    dataIndex: 'currentGameweek',
    render: (t, r) => {
      const gameWeek = getGameweek(r.gameweeks);

      return <Text classText="c-col-text">{gameWeek.points || 0}</Text>;
    }
  },
  {
    title: 'Total Score',
    align: 'center',
    dataIndex: 'totalScore',
    render: (t, r) => <Text classText="c-col-text">{r.totalScore || 0}</Text>
  }
];
