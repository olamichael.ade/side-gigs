import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/',
    exact: true,
    auth: false, // Check if its an authenticated route
    component: Loadable({
      loader: () => import(/* webpackChunkName: "home" */ './components/index'),
      loading: Loader,
      modules: ['home']
    })
  }
];

export default routes;
