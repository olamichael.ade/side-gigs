import React, { Component, Fragment } from 'react';
import { HashLink as Link } from 'HOCS/LinkComponent';
import { Text, Icon, Button, LinkedText } from 'components';
import { parseClosingDate } from 'utils/utils';
import Number from './number';
import WhyItem from './WhyItem';
import Baller from '../assets/ffl-baller.png';
import './index.scss';

const {
  ICON_DOWN_CIRCLE,
  MONEY_ICON,
  HANDSHAKE_ICON,
  BADGE_ICON,
  CREATE_ICON,
  CREATE_GAME_ICON,
  JOIN_ICON,
  FUND_ICON
} = Icon;

class Home extends Component {
  handleClick = () => {
    this.props.history.push('/signup');
  };

  render() {
    const { isAuthenticated, featuredLeague } = this.props;
    const { prize = 0, fee = 0, name, start, end, code, close, state } =
      featuredLeague || {};

    const isEnded = state === 'ended';

    return (
      <div className="home-container">
        <section className="landing-container">
          <div className="landing-content-container">
            <div className="landing-title-container">
              <p className="page-title-container">
                <Text color="white" classText="page-title">
                  Fans Fantasy League, the best money fantasy football game.
                </Text>
              </p>
              <p className="page-subtitle-container text-container">
                <Text classText="sub-title">
                  Make money and enjoy the game with your friends.
                </Text>
              </p>
              {featuredLeague &&
                !isEnded && (
                  <Fragment>
                    <p className="page-info-container main-title">
                      <span>Win big!!!</span> With{' '}
                      <span className="txt-highlight">
                        {fee.toLocaleString() === '0'
                          ? 'zero registration fee'
                          : 'N' + fee.toLocaleString()}
                      </span>
                      , you can win{' '}
                      <span className="txt-highlight">
                        N{prize.toLocaleString()}
                      </span>
                      .
                    </p>
                    <p className="page-info">
                      Join{' '}
                      <LinkedText
                        className="txt-highlight link"
                        to={`/leagues/join?code=${code}`}
                      >
                        {name}
                      </LinkedText>{' '}
                      league now. Top team/manager wins!
                    </p>
                    <p className="page-info sub-title">
                      League starts Gameweek {start} and ends Gameweek {end}.
                    </p>
                    <p className="page-info sub-title">
                      Get your line ups ready! League closes{' '}
                      {parseClosingDate(close)}
                    </p>
                    <div className="link-container">
                      <LinkedText to={`/leagues/join?code=${code}`}>
                        <Button className="auth-form-button sm-btn">
                          <Text color="white" weight="med">
                            Join
                          </Text>
                        </Button>
                      </LinkedText>
                    </div>
                  </Fragment>
                )}
              {!isAuthenticated && (
                <div className="page-button-cta">
                  <LinkedText to="/signup">
                    <Button className="auth-form-button">
                      <Text color="white">Get Started</Text>
                    </Button>
                  </LinkedText>
                  <div className="cta-option-text">
                    <Text color="white">OR</Text>
                  </div>
                  <div>
                    <LinkedText to="/login">
                      <Button color="purple" onClick={this.handleClick}>
                        <Text color="white">SIGN IN</Text>
                      </Button>
                    </LinkedText>
                  </div>
                </div>
              )}
            </div>
            <img className="baller-img" src={Baller} alt="ffl-baller" />
          </div>
          <div className="scroll-view-container">
            <Link smooth to="#why-fan-ligue">
              <Icon iconClass="ctrl-icon" iconType={ICON_DOWN_CIRCLE} />
            </Link>
          </div>
        </section>
        <section
          id="why-fan-ligue"
          ref="whyContainer"
          className="why-container"
        >
          <div className="why-container-title">
            <Text classText="wi-title" color="black" weight="med">
              Why Fans Fantasy League?
            </Text>
          </div>
          <div className="why-items-container">
            <WhyItem
              title="Make Money"
              subTitle="Use from alot of our available Open leagues, challenge others
                and make good money."
              iconType={MONEY_ICON}
              iconClass="money-icon"
            />
            <WhyItem
              title="One vs One Challenges"
              subTitle="Create Head to Head Challenge with your friends or invite them
                to join your open leagues. Compete and enjoy the game."
              iconType={HANDSHAKE_ICON}
            />
            <WhyItem
              title="Best Manager"
              subTitle="Prove to your friends that you are the best manager in the
                world. Based on your official FPL points."
              iconType={BADGE_ICON}
              iconClass="badge-icon"
            />
          </div>
        </section>
        <section id="how-to-play" className="how-to-container">
          <div className="ht-container-title">
            <Text classText="wi-title" color="black" weight="med">
              How Fans Fantasy League Works
            </Text>
          </div>
          <div className="ht-subtitle">
            <Text>
              In 4 easy steps, start making money, while having fun with your
              friends.
            </Text>
          </div>
          <div className="steps-container">
            <div className="steps">
              <div className="hiw-image">
                <Icon iconType={CREATE_ICON} iconClass="wi-icon-2" />
              </div>
              <div className="step-grp-container">
                <Number number="1" />
                <Text classText="wi-title" color="black" weight="med">
                  Create an Account
                </Text>
                <div className="hiw-dec-container">
                  <Text>
                    Sign up with Fans Fantasy League with your email. Link with
                    your FPL account.
                  </Text>
                </div>
              </div>
            </div>
            <div className="steps reverse">
              <div className="hiw-image">
                <Icon iconType={FUND_ICON} iconClass="wi-icon-2" />
              </div>
              <div className="step-grp-container">
                <Number number="2" />
                <div>
                  <Text classText="wi-title" color="black" weight="med">
                    Fund your Wallet
                  </Text>
                </div>
                <div className="hiw-dec-container">
                  <Text>
                    Using your ATM Card/Debit Card, you will be required to fund
                    your wallet to be able to use this platform optimaly.
                  </Text>
                </div>
              </div>
            </div>
            <div className="steps">
              <div className="hiw-image">
                <Icon iconType={CREATE_GAME_ICON} iconClass="wi-icon-2" />
              </div>
              <div className="step-grp-container">
                <Number number="3" />
                <div>
                  <Text classText="wi-title" color="black" weight="med">
                    Create a game
                  </Text>
                </div>
                <div className="hiw-dec-container">
                  <Text>
                    Create your own league or Head to Head Challenge by your
                    rules, set the fee to join. Invite your friends and others
                    to join.
                  </Text>
                </div>
              </div>
            </div>
            <div className="steps reverse">
              <div className="hiw-image">
                <Icon iconType={JOIN_ICON} iconClass="wi-icon-2" />
              </div>
              <div className="step-grp-container">
                <Number number="4" />
                <div>
                  <Text classText="wi-title" color="black" weight="med">
                    Join league, have fun
                  </Text>
                </div>
                <div className="hiw-dec-container">
                  <Text>
                    Using a unique invitation code you can join an open or
                    private league and get to prove you are the best manager.
                    Cheers!
                  </Text>
                </div>
              </div>
            </div>
            {!isAuthenticated && (
              <div className="l-page-cta">
                <div className="btn-container">
                  <Button color="purple" onClick={this.handleClick}>
                    <Text color="white">SIGN UP</Text>
                  </Button>
                </div>
                <div className="sign-in-box">
                  <Text color="black">
                    I have an account,{' '}
                    <LinkedText className="signup-link" to="/login">
                      Sign in
                    </LinkedText>
                  </Text>
                </div>
              </div>
            )}
          </div>
        </section>
      </div>
    );
  }
}
export default Home;
