import React from 'react';
import './index.scss';
const Number = ({ number }) => {
  return <div className="ht-number">{number}</div>;
};
export default Number;
