import { connect } from 'react-redux';
import { find } from 'lodash';
import Home from './Home';

const selFeatured = leagues => find(leagues, l => l.featured);

const mapStateToProps = state => {
  const {
    auth,
    globals: { promoLeagues }
  } = state;
  return {
    isAuthenticated: auth.isAuthenticated,
    featuredLeague: selFeatured(promoLeagues)
  };
};

export default connect(mapStateToProps)(Home);
