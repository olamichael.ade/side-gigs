import React from 'react';
import { Icon, Text } from 'components';

import './index.scss';

const WhyItem = ({ title, subTitle, iconType, iconClass }) => {
  return (
    <div className="why-item">
      <div className="wi-icon-container">
        <Icon iconType={iconType} iconClass={`wi-icon ${iconClass}`} />
      </div>
      <div className="wi-title-container">
        <Text classText="wi-title" color="black" weight="med">
          {title}
        </Text>
      </div>
      <div className="wi-subtitle-container">
        <Text classText="wi-subtitle">{subTitle}</Text>
      </div>
    </div>
  );
};

export default WhyItem;
