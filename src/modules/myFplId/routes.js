import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/get-fpl-id',
    exact: true,
    auth: false,
    component: Loadable({
      loader: () => import(/* webpackChunkName: "getFplId" */ './index'),
      loading: Loader,
      modules: ['getFplId']
    })
  }
];

export default routes;
