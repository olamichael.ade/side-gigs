import React from 'react';
import { Image, Video } from 'cloudinary-react';
import { Text } from 'components';
import { CLOUD_NAME } from 'utils/constants';
import './index.scss';

const MyId = () => {
  return (
    <div className="fpl-id-container">
      <div className="box">
        <h2 className="title">How to get FPL ID</h2>
        <Image publicId="ffl/how-to" cloudName={CLOUD_NAME} width="100%" />
        <h3>
          <Text classText="">Or watch video</Text>
        </h3>
        <Video
          cloudName={CLOUD_NAME}
          publicId="ffl/how_to_vid"
          width="100%"
          controls
        />
      </div>
    </div>
  );
};

export default MyId;
