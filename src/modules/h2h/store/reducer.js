import {
  IS_REQUEST,
  IS_REQUEST_FAILED,
  IS_REQUEST_SUCCESS,
  LOAD_CURRENT_GAMEWEEK,
  LOAD_H2H,
  CLEAR_ERROR,
  REFRESH_H2H,
  LOAD_CURRENT_H2H,
  UPDATE_CURRENT_H2H
} from './actionTypes';

const initialState = {
  loading: false,
  loaded: false,
  error: false,
  errorMessage: null,
  h2h: [],
  currentH2h: {},
  isAjaxReq: false,
  gameWeek: null
};

function h2hReducer(state = initialState, action) {
  switch (action.type) {
    case IS_REQUEST:
      return { ...state, isAjaxReq: true, error: false };
    case IS_REQUEST_SUCCESS:
      return { ...state, isAjaxReq: false, error: false };
    case IS_REQUEST_FAILED:
      const { errorMessage } = action;
      return { ...state, isAjaxReq: false, error: true, errorMessage };
    case LOAD_CURRENT_GAMEWEEK:
      const { gameWeek } = action;
      return { ...state, gameWeek };
    case LOAD_H2H:
      const { h2h } = action;
      return { ...state, h2h: [...h2h] };
    case CLEAR_ERROR:
      return { ...state, error: false, errorMessage: null };
    case REFRESH_H2H:
      return { ...initialState };
    case UPDATE_CURRENT_H2H:
    case LOAD_CURRENT_H2H:
      const { currentH2h } = action;
      return { ...state, currentH2h };
    default:
      return { ...state };
  }
}

export default h2hReducer;
