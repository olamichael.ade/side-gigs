import Api from 'utils/Api';
import {
  IS_REQUEST,
  IS_REQUEST_FAILED,
  IS_REQUEST_SUCCESS,
  LOAD_CURRENT_GAMEWEEK,
  LOAD_H2H,
  CLEAR_ERROR,
  REFRESH_H2H,
  LOAD_CURRENT_H2H,
  UPDATE_CURRENT_H2H
} from './actionTypes';

function loadCurrentGameweek() {
  return dispatch => {
    Api.getCurrentWeek().then(res =>
      dispatch({ type: LOAD_CURRENT_GAMEWEEK, gameWeek: res.week || 0 })
    );
  };
}

function createChallenge(data, cb) {
  return dispatch => {
    dispatch({ type: IS_REQUEST });
    return Api.createChallenge(data)
      .then(res => {
        dispatch({ type: IS_REQUEST_SUCCESS });
        dispatch({ type: LOAD_H2H, h2h: res.h2h });
        cb();
      })
      .catch(err => {
        const {
          body: { message }
        } = err;
        dispatch({ type: IS_REQUEST_FAILED, errorMessage: message });
      });
  };
}

const updateH2h = (data, cb) => {
  return dispatch => {
    dispatch({ type: IS_REQUEST });
    Api.updateH2h(data)
      .then(res => {
        dispatch({ type: IS_REQUEST_SUCCESS });
        dispatch({ type: UPDATE_CURRENT_H2H, currentH2h: res.data });
        return cb();
      })
      .catch(err => {
        return dispatch({
          type: IS_REQUEST_FAILED,
          errorMessage: 'Error updating H2h'
        });
      });
  };
};

function clearError() {
  return dispatch => dispatch({ type: CLEAR_ERROR });
}

const searchUser = name => Api.searchUser(name);

const loadOpenH2h = () => {
  return dispatch => {
    dispatch({ type: IS_REQUEST });
    Api.getOpenChallenge()
      .then(res => {
        dispatch({ type: IS_REQUEST_SUCCESS });
        dispatch({ type: LOAD_H2H, h2h: res.h2h });
      })
      .catch(e => {
        console.log('Could not load challenges: ', e);
        dispatch({ type: IS_REQUEST_FAILED, errorMessage: null });
      });
  };
};

const fetchChallengeType = ct => Api.fetchChallengeType(ct);

const loadH2h = id => {
  return dispatch => {
    dispatch({ type: IS_REQUEST });
    Api.getH2h(id)
      .then(res => {
        dispatch({ type: IS_REQUEST_SUCCESS });
        dispatch({ type: LOAD_CURRENT_H2H, currentH2h: res.data });
      })
      .catch(err => dispatch({ type: IS_REQUEST_FAILED }));
  };
};

const loadH2hAndCurrentWeek = id => {
  return dispatch => {
    dispatch({ type: IS_REQUEST });

    dispatch(loadCurrentGameweek());
    return Api.getH2h(id)
      .then(res => {
        dispatch({ type: IS_REQUEST_SUCCESS });
        dispatch({ type: LOAD_CURRENT_H2H, currentH2h: res.data });
      })
      .catch(err => dispatch({ type: IS_REQUEST_FAILED }));
  };
};

const h2hCta = (id, cb, ctaType) => {
  return dispatch => {
    dispatch({ type: IS_REQUEST });
    const apiRequest =
      ctaType === 'accept' ? Api.acceptChallenge : Api.rejectChallenge;
    return apiRequest(id)
      .then(res => {
        dispatch({ type: IS_REQUEST_SUCCESS });
        return cb();
      })
      .catch(err => {
        const ERR_MSG = 'Oh snap! There was an error processing your request.';
        const { message } = err.body;
        dispatch({ type: IS_REQUEST_FAILED, errorMessage: message || ERR_MSG });
      });
  };
};

const refresh = () => {
  return dispatch => dispatch({ type: REFRESH_H2H });
};

export {
  loadCurrentGameweek,
  loadH2hAndCurrentWeek,
  createChallenge,
  clearError,
  searchUser,
  loadOpenH2h,
  fetchChallengeType,
  h2hCta,
  loadH2h,
  refresh,
  updateH2h
};
