import Loadable from 'react-loadable';

import { Loader } from 'components';

const routes = [
  {
    path: '/h2h',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "h2hDashboard" */ './pages/Dashboard'),
      loading: Loader,
      modules: ['h2hDashboard']
    })
  },
  {
    path: '/h2h/create',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "createH2h" */ './pages/Create'),
      loading: Loader,
      modules: ['createH2h']
    })
  },
  {
    path: '/h2h/:h2h_id/edit',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import(/* webpackChunkName: "editH2H" */ './pages/Edit'),
      loading: Loader,
      modules: ['editH2h']
    })
  },
  {
    path: '/h2h/join',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import(/* webpackChunkName: "joinH2h" */ './pages/Join'),
      loading: Loader,
      modules: ['joinH2h']
    })
  },
  {
    path: '/h2h/completed',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "completeH2h" */ './pages/Complete'),
      loading: Loader,
      modules: ['completeH2h']
    })
  },
  {
    path: '/h2h/:h2h_id/accept',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "acceptH2h" */ './pages/Accept'),
      loading: Loader,
      modules: ['acceptH2h']
    })
  },
  {
    path: '/h2h/:h2h_id',
    exact: true,
    auth: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "h2hDetails" */ './pages/Details'),
      loading: Loader,
      modules: ['h2hDetails']
    })
  }
];

export default routes;
