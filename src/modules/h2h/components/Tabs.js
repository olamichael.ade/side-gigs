import React, { Component } from 'react';
import { Tabs, Table } from 'antd';
import ChallengeList from './ChallengeList';
import { fetchChallengeType } from '../store/action';
import Column from './MyChallengeColumn';
import AllChallengeColumn from './AllColumn';
import './Tabs.scss';

const TabPane = Tabs.TabPane;

const keyToTab = {
  '1': 'all',
  '2': '',
  '3': 'ongoing',
  '4': 'upcoming',
  '5': 'previous'
};
class WrappedTabs extends Component {
  state = {
    challengeType: 'all',
    challenges: {}
  };

  componentDidMount() {
    this.fetchChallengeType();
  }

  gotoDetails = record => {
    this.props.history.push(`/h2h/${record._id}`);
  };

  callback = key => {
    this.setState({ challengeType: keyToTab[key], loading: false }, () =>
      this.fetchChallengeType()
    );
  };

  fetchChallengeType = () => {
    const { challengeType, challenges } = this.state;

    this.setState({ loading: true, loaded: false });

    fetchChallengeType(challengeType)
      .then(res => {
        const chType = !challengeType ? 'myH2h' : challengeType;
        this.setState({
          loading: false,
          loaded: true,
          challenges: { ...challenges, [chType]: res.h2h }
        });
      })
      .catch(e => {
        console.log('Cannot get h2h for challenge type: ', challengeType, e);
        this.setState({ loading: false, loaded: false });
      });
  };

  render() {
    const { challengeType, loading, challenges } = this.state;

    return (
      <Tabs
        className="h2h-tab-container"
        defaultActiveKey="1"
        onChange={this.callback}
      >
        <TabPane tab="All H2H" key="1">
          <div>
            <Table
              onRow={record => {
                return {
                  onClick: () => this.gotoDetails(record)
                };
              }}
              rowKey={record => record._id}
              dataSource={challenges.myH2h}
              columns={AllChallengeColumn}
              locale={{ emptyText: 'No H2h Challenges' }}
            />
          </div>
        </TabPane>
        <TabPane tab="My H2H" key="2">
          <div>
            <Table
              rowKey={record => record._id}
              dataSource={challenges.myH2h}
              columns={Column}
              locale={{ emptyText: 'No Open Challenges' }}
            />
          </div>
        </TabPane>
        <TabPane tab="Ongoing H2H" key="3">
          <ChallengeList
            loading={loading}
            challengeType={challengeType}
            challenges={challenges.ongoing || []}
          />
        </TabPane>
        <TabPane tab="Upcoming H2H" key="4">
          <ChallengeList
            loading={loading}
            challengeType={challengeType}
            challenges={challenges.upcoming || []}
          />
        </TabPane>
        <TabPane tab="Previous H2H" key="5">
          <ChallengeList
            loading={loading}
            challengeType={challengeType}
            challenges={challenges.previous || []}
          />
        </TabPane>
      </Tabs>
    );
  }
}

export default WrappedTabs;
