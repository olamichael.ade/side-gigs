import React from 'react';
import { Link } from 'react-router-dom';
import { Text, Icon } from 'components';
import './Column.scss';

const { ICON_SETTINGS, ICON_LOCK } = Icon;

export default [
  {
    title: <Text classText="jc-col-header">Game Title</Text>,
    align: 'left',
    dataIndex: 'title',
    className: 'jc-col',
    render: (text, record) => (
      <Text classText="jc-title" color="black">
        {record.title}
      </Text>
    )
  },
  {
    title: <Text classText="jc-col-header">Game Duration</Text>,
    align: 'left',
    dataIndex: 'start',
    className: 'jc-col gw',
    render: (text, record) => (
      <span>
        <Text classText="jc-value-text gw">{`Gameweek ${record.start} - ${
          record.end
        }`}</Text>
        <Text classText="jc-value-text-mobile">{`GW ${record.start} - ${
          record.end
        }`}</Text>
      </span>
    )
  },
  {
    title: <Text classText="jc-col-header">Fee</Text>,
    align: 'right',
    dataIndex: 'fee',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.fee || 0}</Text>
  },
  {
    title: <Text classText="jc-col-header">Status</Text>,
    align: 'right',
    dataIndex: 'status',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.status}</Text>
  },
  {
    title: <Text classText="jc-col-header">Action</Text>,
    align: 'center',
    dataIndex: 'control',
    className: 'jc-col',
    render: (t, r) => {
      const canEditAndDelete = ['expired', 'open'].includes(r.status);

      return canEditAndDelete ? (
        <div className="ctrl-container">
          <Link to={`/h2h/${r._id}/edit`}>
            <Icon iconType={ICON_SETTINGS} />
          </Link>
        </div>
      ) : (
        <div className="ctrl-container">
          <Icon iconType={ICON_LOCK} />
        </div>
      );
    }
  }
];
