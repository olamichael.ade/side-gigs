import React from 'react';
import { Collapse } from 'antd';

const Panel = Collapse.Panel;

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden'
};

const ChallengeTable = props => {
  const { header, key } = props;

  return (
    <Panel header={header} key={key} style={customPanelStyle}>
      <p>{text}</p>
    </Panel>
  );
};

export default ChallengeTable;
