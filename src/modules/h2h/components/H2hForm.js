import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Select, Modal } from 'antd';
import { isEmpty } from 'lodash';
import { Button, Text } from 'components';
import Input from 'components/Input';
import { errors } from 'utils/validators';
import './H2hForm.scss';

const FormInput = Input.form;
const Option = Select.Option;

let WrappedCreateH2HForm = props => {
  const {
    minValue,
    currentWeek,
    onSubmit,
    loading,
    handleSubmit,
    invalid,
    challenger,
    minAmountValidator,
    isEdit,
    callback,
    canEditDetails
  } = props;

  const isDisabled = isEdit ? !canEditDetails : false;

  const inputLayout = {
    marginBottom: 20
  };

  const validateFee = value => {
    if (value > minValue) confirm();
  };

  const confirm = () => {
    return Modal.confirm({
      title: 'Insufficient wallet balance',
      content: 'Do you want to fund your wallet?',
      onOk: () => callback(),
      onCancel: () => {}
    });
  };

  const renderSelectField = ({ input: { onChange, name, value } }) => {
    const gameweeks = Array.from({ length: 38 }, (v, k) => k + 1);

    return (
      <Select
        name={name}
        id={name}
        disabled={isDisabled}
        className="form-select"
        placeholder="Select a Gameweek"
        onChange={onChange}
        defaultValue={value}
      >
        <Option value="">Select Gameweek</Option>
        {gameweeks.map(gameweek => (
          <Option
            disabled={gameweek < currentWeek}
            key={`week-${gameweek}`}
            value={gameweek}
          >
            <Text>{`Gameweek ${gameweek}`}</Text>
          </Option>
        ))}
      </Select>
    );
  };

  const renderFeeInput = inputProps => {
    return (
      <FormInput
        {...inputProps}
        onChange={e => {
          inputProps.input.onChange(e);
          validateFee(e.target.value);
        }}
      />
    );
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="h2h-form">
      {!isEmpty(challenger) && (
        <div className="invited-challenger">
          <p className="invited-user">
            <Text classText="invited-user-label">You invited:</Text>
            <Text color="black">{challenger.label}</Text>
          </p>
        </div>
      )}
      <div className="form-input-container">
        <label>
          <Text color="black">Challenge Title</Text>
        </label>
        <Field
          className="form-input"
          name="title"
          disabled={isDisabled}
          placeholder="Enter Challenge Title"
          component={FormInput}
          formLayout={inputLayout}
          validate={errors.challengeRequired}
        />
      </div>
      <div className="composite-select">
        <div className="form-input-container">
          <label>
            <Text color="black">Challenge starts</Text>
          </label>
          <Field
            className="form-input"
            name="start"
            required
            component={renderSelectField}
            formLayout={inputLayout}
          />
        </div>
        <div className="form-input-container">
          <label className="center-label">
            <Text color="black">Challenge Ends</Text>
          </label>
          <Field
            className="form-input"
            name="end"
            required
            component={renderSelectField}
            formLayout={inputLayout}
          />
        </div>
      </div>
      <div className="form-input-container">
        <label>
          <Text color="black">Participation Fee</Text>
        </label>
        <Field
          className="form-input"
          name="fee"
          type="number"
          disabled={isEdit}
          placeholder="Enter stake amount"
          component={renderFeeInput}
          formLayout={inputLayout}
          validate={[errors.feeRequired, minAmountValidator]}
        />
      </div>
      <div>
        <Text classText="form-foot-note" color="black">
          * 15% platform fee will be deducted
        </Text>
      </div>
      <div className="h2h-btn-container">
        <Button
          color="purple"
          loading={loading}
          className="auth-form-button"
          disabled={invalid}
          htmlType="submit"
        >
          {isEdit ? (
            <Text color="white">{loading ? 'Updating...' : 'Update'}</Text>
          ) : (
            <Text color="white">{loading ? 'Creating...' : 'Create'}</Text>
          )}
        </Button>
      </div>
    </form>
  );
};

WrappedCreateH2HForm.defaultProps = {
  canEditDetails: false,
  isEdit: false
};

WrappedCreateH2HForm.propTypes = {
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  form: PropTypes.string,
  isEdit: PropTypes.bool,
  canEditDetails: PropTypes.bool
};

WrappedCreateH2HForm = reduxForm({
  form: 'H2H_CREATE_FORM',
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(WrappedCreateH2HForm);

export default WrappedCreateH2HForm;
