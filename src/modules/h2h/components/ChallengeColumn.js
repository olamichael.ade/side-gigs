import React from 'react';
import { Text, Icon } from 'components';

import './Column.scss';

const { ICON_CARET_UP, ICON_CARET_DOWN } = Icon;

export default [
  {
    title: '',
    align: 'left',
    dataIndex: 'name',
    render: (text, record) => (
      <Text classText="c-col-title" color="black">
        {record.name}
      </Text>
    )
  },
  {
    title: '',
    align: 'center',
    dataIndex: 'ranking',
    render: (text, record) => {
      switch (record.ranking) {
        case 'loser':
          return <Icon iconClass="loser-icon" iconType={ICON_CARET_DOWN} />;
        case 'winner':
          return <Icon iconClass="winner-icon" iconType={ICON_CARET_UP} />;
        default:
          return <div className="draw-icon" />;
      }
    }
  },
  {
    title: 'GW',
    align: 'center',
    dataIndex: 'currentGameweek',
    render: (t, r) => <Text classText="c-col-text">{r.currentGameweek}</Text>
  },
  {
    title: 'TP',
    align: 'center',
    dataIndex: 'totalScore',
    render: (t, r) => <Text classText="c-col-text">{r.totalScore}</Text>
  }
];
