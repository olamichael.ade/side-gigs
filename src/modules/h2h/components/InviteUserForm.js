import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Select, Spin } from 'antd';
import debounce from 'lodash/debounce';
import isEmpty from 'lodash/isEmpty';
import { Button, Text } from 'components';
import { searchUser } from '../store/action';

const Option = Select.Option;

class InviteUserForm extends Component {
  constructor(props) {
    super(props);

    this.fetchUser = debounce(this.fetchUser, 800);
  }

  state = {
    data: [],
    value: [],
    fetching: false
  };

  fetchUser = name => {
    this.setState({ data: [], fetching: true });

    searchUser(name).then(({ users }) => {
      const data = users.map(user => ({
        text: `${user.name} - ${user.teamName}`,
        value: `${user._id}`
      }));

      this.setState({ data, fetching: false });
    });
  };

  handleChange = value => {
    this.setState({
      value,
      data: [],
      fetching: false
    });
  };

  inviteUser = () => {
    const { value } = this.state;
    this.props.inviteUser(value);
  };

  render() {
    const { fetching, data, value } = this.state;
    return (
      <div className="invite-form-container">
        <Select
          name="user-invite"
          showSearch
          labelInValue
          value={value}
          placeholder="Select users"
          notFoundContent={fetching ? <Spin size="small" /> : null}
          filterOption={false}
          onSearch={this.fetchUser}
          onChange={this.handleChange}
          style={{ width: '100%' }}
        >
          {data.map(d => <Option key={d.value}>{d.text}</Option>)}
        </Select>
        <div className="h2h-invite-btn-container">
          <Button
            color="purple"
            className="h2h-form-button"
            disable={isEmpty(value).toString()}
            onClick={this.inviteUser}
          >
            <Text color="white">CONFIRM</Text>
          </Button>
        </div>
      </div>
    );
  }
}

InviteUserForm.propTypes = {
  onSubmit: PropTypes.func,
  inviteUser: PropTypes.func.isRequired
};

export default InviteUserForm;
