import React from 'react';
import { Text } from 'components';
import './Column.scss';

export default [
  {
    title: <Text classText="jc-col-header">Game Title</Text>,
    align: 'left',
    dataIndex: 'title',
    className: 'jc-col',
    render: (text, record) => (
      <Text classText="jc-title" color="black">
        {record.title}
      </Text>
    )
  },
  {
    title: <Text classText="jc-col-header">Game Duration</Text>,
    align: 'left',
    dataIndex: 'start',
    className: 'jc-col gw',
    render: (text, record) => (
      <span>
        <Text classText="jc-value-text gw">{`Gameweek ${record.start} - ${
          record.end
        }`}</Text>
        <Text classText="jc-value-text-mobile">{`GW ${record.start} - ${
          record.end
        }`}</Text>
      </span>
    )
  },
  {
    title: <Text classText="jc-col-header">Fee</Text>,
    align: 'right',
    dataIndex: 'fee',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.fee || 0}</Text>
  },
  {
    title: <Text classText="jc-col-header">Status</Text>,
    align: 'right',
    dataIndex: 'status',
    className: 'jc-col',
    render: (t, r) => <Text classText="jc-value-text">{r.status}</Text>
  }
];
