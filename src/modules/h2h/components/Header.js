import React from 'react';
import { Text } from 'components';
import Topbar from './Topbar';

import './Header.scss';

const Header = ({ title }) => {
  return (
    <Topbar>
      <Text classText="header-title" weight="med2" color="black">
        {title}
      </Text>
    </Topbar>
  );
};

export default Header;
