import React from 'react';
import { Collapse, Table, Spin } from 'antd';
import { isEmpty } from 'lodash';
import { Text } from 'components';
import ChallengeColmun from './ChallengeColumn';

import './ChallengeList.scss';

const Panel = Collapse.Panel;

const getWinner = data => {
  const [team1, team2] = data;

  return [
    {
      ...team1,
      ranking:
        team1.totalScore > team2.totalScore
          ? 'winner'
          : team1.totalScore === team2.totalScore
            ? 'draw'
            : 'loser'
    },
    {
      ...team2,
      ranking:
        team2.totalScore > team1.totalScore
          ? 'winner'
          : team2.totalScore === team1.totalScore
            ? 'draw'
            : 'loser'
    }
  ];
};

const tabToLabel = {
  ongoing: 'Ongoing',
  upcoming: 'Upcoming',
  previous: 'Previous'
};

const ChallengeList = props => {
  const { challenges, challengeType, loading } = props;
  // const defaultActiveKey = Object.keys(challenges);

  const getCurrentGameweek = (gameWeeks = []) => {
    return gameWeeks.reduce((memo, gw) => {
      const { event, points } = gw;

      return points ? event : memo;
    }, null);
  };

  const normalizeResult = (result = []) => {
    const nResult = result.map(r => {
      const { gameweeks, ...rest } = r;

      return {
        ...rest,
        currentGameweek: getCurrentGameweek(gameweeks)
      };
    });

    return getWinner(nResult);
  };

  const renderPanels = challenges.map((challenge, i) => {
    const data = normalizeResult(challenge.result);

    return (
      <Panel
        className="challenge-item"
        header={
          <Text classText="c-panel-title" weight="med">
            {challenge.title}
          </Text>
        }
        key={`${i + 1}`}
      >
        <Table
          className="c-challenge-list"
          rowKey={record => record.userId}
          columns={ChallengeColmun}
          dataSource={data}
          size="middle"
          pagination={false}
        />
      </Panel>
    );
  });

  const renderContent = isEmpty(challenges) ? (
    <div className="c-loading-container">
      <Text>{`You have no ${tabToLabel[challengeType]} challenge(s)`}</Text>
    </div>
  ) : (
    <Collapse bordered={false} defaultActiveKey={['1']}>
      {renderPanels}
    </Collapse>
  );

  return (
    <div>
      {loading ? (
        <div className="c-loading-container">
          <Spin />
        </div>
      ) : (
        renderContent
      )}
    </div>
  );
};

export default ChallengeList;
