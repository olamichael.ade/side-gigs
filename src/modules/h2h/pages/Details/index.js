import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { LoadComponent } from 'HOCS';
import { loadH2h, refresh, h2hCta } from '../../store/action';
import Details from './Details';

const mapStateToProps = (state, router) => {
  const { h2h } = state;
  const { params } = router.match;

  return {
    loading: h2h.loading,
    loaded: h2h.loaded,
    loadParam: params.h2h_id,
    currentH2h: h2h.currentH2h,
    isAjaxReq: h2h.isAjaxReq
  };
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { load: loadH2h, refresh, h2hCta }
  ),
  LoadComponent
)(Details);
