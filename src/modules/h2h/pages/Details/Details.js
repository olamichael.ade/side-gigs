import React from 'react';
import { Link } from 'react-router-dom';
import { Topbar } from '../../components';
import { Text, Button } from 'components';
import { capitalize } from 'utils/utils';
import './index.scss';

const Details = props => {
  const {
    currentH2h: { title, fee, _id, status, start, end, prize, type },
    h2hCta,
    isAjaxReq,
    history
  } = props;

  const isPrivate = type === 'private';

  const pageTitle = isPrivate ? 'H2H Invitation' : capitalize(title);

  const rejectChallenge = () => {
    const cb = () =>
      history.push({
        pathname: '/h2h/completed',
        state: {
          successMessage: `You have rejected ${title}.`
        }
      });

    return h2hCta(_id, cb, 'reject');
  };

  return (
    <div className="details-container">
      <Topbar>
        <Text classText="top-title" color="black" weight="med">
          {pageTitle}
        </Text>
      </Topbar>
      <div className="details-body">
        <div className="details-top">
          <Text classText="details-text-title" weight="med">
            Confirm Payment
          </Text>
        </div>
        <hr />
        <div className="details">
          {isPrivate && (
            <div className="labelled-text-container">
              <div className="label-container">
                <Text classText="label-text">Challenge Title: </Text>
              </div>
              <div className="value-container">
                <Text classText="value-text">{capitalize(title)}</Text>
              </div>
            </div>
          )}
          <div className="labelled-text-container">
            <div className="label-container">
              <Text classText="label-text">Challenge Type: </Text>
            </div>
            <div className="value-container">
              <Text classText="value-text">{capitalize(status)}</Text>
            </div>
          </div>
          <div className="labelled-text-container">
            <div className="label-container">
              <Text classText="label-text">Participation Fee: </Text>
            </div>
            <div className="value-container">
              <Text classText="value-text">{`NGN ${fee}`}</Text>
            </div>
          </div>
          <div className="labelled-text-container">
            <div className="label-container">
              <Text classText="label-text">Game Duration: </Text>
            </div>
            <div className="value-container">
              <div className="value-desktop">
                <Text classText="value-text">{`Gameweek ${start}`}</Text>{' '}
                -&nbsp;
                <Text classText="value-text">{`Gameweek ${end}`}</Text>
              </div>
              <div className="value-mobile">
                <Text classText="value-text">{`GW ${start}`}</Text> -&nbsp;
                <Text classText="value-text">{`GW ${end}`}</Text>
              </div>
            </div>
          </div>
          <div className="labelled-text-container">
            <div className="label-container">
              <Text classText="label-text">Prize: </Text>
            </div>
            <div className="value-container">
              <Text classText="value-text">{`NGN ${prize}`}</Text>
            </div>
          </div>
          <div className="labelled-text-container d-btn">
            <Link to={`/h2h/${_id}/accept`}>
              <Button color="purple">
                <Text color="white">Accept</Text>
              </Button>
            </Link>
            <Button loading={isAjaxReq} color="red" onClick={rejectChallenge}>
              <Text color="white">Reject</Text>
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Details;
