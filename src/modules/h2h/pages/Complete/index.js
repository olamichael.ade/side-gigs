import React from 'react';
import { Icon, Text, Button } from 'components';
import { withRouter } from 'react-router';
import './index.scss';

const { OK_ICON } = Icon;

const Completed = props => {
  const {
    history,
    location: { state }
  } = props;

  const gotoDashboard = () => history.push('/h2h');

  const { successMessage = '' } = state;

  return (
    <div className="completed-container">
      <div className="container-body">
        <div className="icon-container">
          <Icon iconType={OK_ICON} iconClass="ok-icon" />
        </div>
        <div className="completed-text-container">
          <Text color="black" classText="success-text">
            {successMessage}
          </Text>
          <Button
            color="purple"
            className="completed-btn"
            onClick={gotoDashboard}
          >
            <Text color="white">Done</Text>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Completed);
