import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { LoadComponent, RefreshComponent } from 'HOCS';
import { loadH2h, refresh, h2hCta, clearError } from '../../store/action';
import Accept from './Accept';

const mapStateToProps = (state, router) => {
  const { h2h } = state;
  const { params } = router.match;

  return {
    loading: h2h.loading,
    loaded: h2h.loaded,
    loadParam: params.h2h_id,
    currentH2h: h2h.currentH2h,
    error: h2h.error,
    errorMessage: h2h.errorMessage
  };
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { load: loadH2h, refresh, h2hCta, clearError }
  ),
  LoadComponent,
  RefreshComponent
)(Accept);
