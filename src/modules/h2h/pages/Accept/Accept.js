import React from 'react';
import { Alert } from 'antd';
import { Text, Button } from 'components';

import './index.scss';

const Accept = props => {
  const {
    currentH2h: { title, fee, _id },
    h2hCta,
    error,
    clearError,
    errorMessage,
    history
  } = props;

  const acceptChallenge = () => {
    const cb = () =>
      history.push({
        pathname: '/h2h/completed',
        state: {
          successMessage: `You have sucessfully accepted ${title}`
        }
      });

    return h2hCta(_id, cb, 'accept');
  };

  return (
    <div className="accept-container">
      <div className="top-container">
        <Text classText="top-title" color="black" weight="med">
          {title}
        </Text>
      </div>
      <div className="confirm-container">
        <div className="confirm-top">
          <Text classText="confirm-text-title">Confirm Payment</Text>
        </div>
        <hr />
        <div className="confirm-body">
          {error && (
            <div className="h2h-err-container">
              <Alert
                description={errorMessage}
                type="error"
                closable
                onClose={() => clearError()}
              />
            </div>
          )}
          <div className="confirm-body-text">
            <Text>
              {`NGN ${fee} willl be deducted from your wallet as participation fee.`}
            </Text>
          </div>
          <Button color="purple" onClick={acceptChallenge}>
            <Text color="white">CONFIRM PAYMENT</Text>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Accept;
