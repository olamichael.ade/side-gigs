import React from 'react';
import { Link } from 'react-router-dom';
import { Topbar, Tabs } from '../../components';
import { Button, Text } from 'components';

import './index.scss';

const H2H = () => {
  return (
    <div>
      <Topbar>
        <div className="cta-container">
          <Link className="btn-link" to="h2h/create">
            <Button>
              <Text color="black">Create Challenge</Text>
            </Button>
          </Link>
          <Link className="btn-link" to="h2h/join">
            <Button>
              <Text color="black">Join Challenge</Text>
            </Button>
          </Link>
        </div>
      </Topbar>
      <Tabs />
    </div>
  );
};

export default H2H;
