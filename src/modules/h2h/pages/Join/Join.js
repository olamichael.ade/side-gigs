import React from 'react';
import { Text } from 'components';
import { Table } from 'antd';
import { Header } from '../../components';
import { getColumns } from './Column';

import './index.scss';

const JoinH2H = props => {
  const { h2h, userId } = props;

  const columns = getColumns(userId);

  return (
    <div className="h2h-container">
      <Header title="Open Challenges" />
      <section className="ch2h-container">
        <div className="form-title-container">
          <Text classText="ch2h-form-title" weight="med">
            Available H2H Challenges
          </Text>
        </div>
        <div>
          <Table
            rowKey={record => record._id}
            dataSource={h2h}
            columns={columns}
            locale={{ emptyText: 'No Open Challenges' }}
          />
        </div>
      </section>
    </div>
  );
};

export default JoinH2H;
