import { compose } from 'redux';
import { connect } from 'react-redux';
import { LoadComponent } from 'HOCS';
import { loadOpenH2h } from '../../store/action';
import Join from './Join';

const mapStateToProps = state => {
  const {
    h2h: { loading, loaded, error, errorMessage, isAjaxReq, h2h },
    auth: { user }
  } = state;

  const { _id } = user || {};

  return {
    loading,
    loaded,
    error,
    h2h,
    errorMessage,
    isAjaxReq,
    userId: _id
  };
};

export default compose(
  connect(
    mapStateToProps,
    { load: loadOpenH2h }
  ),
  LoadComponent
)(Join);
