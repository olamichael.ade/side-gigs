import React from 'react';
import { Link } from 'react-router-dom';
import { Tag } from 'antd';
import { Text, Button, Icon } from 'components';

import './index.scss';

const { ICON_PLUS } = Icon;

const getColumns = userId => {
  return [
    {
      title: '',
      align: 'left',
      dataIndex: 'title',
      className: 'jc-col',
      render: (text, record) => (
        <Text classText="jc-title" color="black">
          {record.title}
        </Text>
      )
    },
    {
      title: <Text classText="jc-col-header">Game Duration</Text>,
      align: 'left',
      dataIndex: 'start',
      className: 'jc-col gw',
      render: (text, record) => (
        <span>
          <Text classText="jc-value-text gw">{`Gameweek ${record.start} - ${
            record.end
          }`}</Text>
          <Text classText="jc-value-text-mobile">{`GW ${record.start} - ${
            record.end
          }`}</Text>
        </span>
      )
    },
    {
      title: <Text classText="jc-col-header">Fee</Text>,
      align: 'right',
      dataIndex: 'fee',
      className: 'jc-col',
      render: (t, r) => <Text classText="jc-value-text">{r.fee || 0}</Text>
    },
    {
      title: <Text classText="jc-col-header">Prize</Text>,
      align: 'right',
      dataIndex: 'prize',
      className: 'jc-col',
      render: (t, r) => <Text classText="jc-value-text">{r.prize}</Text>
    },
    {
      title: '',
      align: 'right',
      dataIndex: 'accept',
      className: 'jc-col',
      render: (t, r) =>
        r.challenger === userId ? (
          <Tag color="green">Owner</Tag>
        ) : (
          <Link to={`/h2h/${r._id}`}>
            <Button className="col-btn">
              <Text classText="accept-text" color="black">
                Accept
              </Text>
              <Icon iconType={ICON_PLUS} />
            </Button>
          </Link>
        )
    }
  ];
};

export { getColumns };
