import { compose } from 'redux';
import { connect } from 'react-redux';
import { LoadComponent } from 'HOCS';
import {
  loadCurrentGameweek,
  createChallenge,
  clearError
} from '../../store/action';
import Create from './Create';

const mapStateToProps = state => {
  const {
    h2h: { loading, loaded, error, errorMessage, gameWeek, isAjaxReq },
    auth: { user, _id }
  } = state;
  const { wallet = 0 } = user || {};

  return {
    loading,
    loaded,
    error,
    errorMessage,
    gameWeek,
    isAjaxReq,
    wallet,
    challenger: _id
  };
};

export default compose(
  connect(
    mapStateToProps,
    { load: loadCurrentGameweek, createChallenge, clearError }
  ),
  LoadComponent
)(Create);
