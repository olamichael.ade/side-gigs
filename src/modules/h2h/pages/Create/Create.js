import React, { Component } from 'react';
import { Radio, Alert, Modal } from 'antd';
import { withRouter } from 'react-router';
import isEmpty from 'lodash/isEmpty';
import { Text, Button } from 'components';
import { Header } from '../../components';
import CreateForm from '../../components/H2hForm';
import InviteForm from '../../components/InviteUserForm';

import { errors } from 'utils/validators';

import './index.scss';

const RadioGroup = Radio.Group;

class CreateH2H extends Component {
  state = {
    type: 'public',
    isVisible: false,
    isErrorVisible: false,
    isSearching: false,
    requestedChallenger: {}
  };

  onChange = () => {
    this.setState({ type: 'public', requestedChallenger: {} });
  };

  showModal = () => {
    this.setState({ type: 'private', isVisible: true });
  };

  goToWallet = () => {
    this.props.history.push('/wallet');
  };

  createChallenge = data => {
    const { history, createChallenge, wallet } = this.props;
    const {
      type,
      requestedChallenger: { key }
    } = this.state;

    const cb = () =>
      history.push({
        pathname: '/h2h/completed',
        state: {
          successMessage: `You have sucessfully created ${data.title}`
        }
      });

    if (data.fee > wallet) {
      return this.showConfirm();
    }
    createChallenge({ ...data, type, invitedContender: key }, cb);
  };

  toggleModal = () => {
    const { isVisible } = this.state;

    this.setState({ isVisible: !isVisible });
  };

  verifyUser = user => {
    this.setState({ requestedChallenger: user, isVisible: false });
  };

  renderInviteUser = () => {
    const { isVisible } = this.state;

    return (
      <Modal
        title={
          <Text classText="h2h-modal-title" weight="med2">
            Invite Friend
          </Text>
        }
        className="h2h-modal"
        visible={isVisible}
        onOk={this.inviteUser}
        onCancel={this.toggleModal}
        footer={null}
      >
        <p className="h2h-subtitle">
          Enter user’s name or team name of the user you want to invite.
        </p>
        <InviteForm inviteUser={data => this.verifyUser(data)} />
      </Modal>
    );
  };

  render() {
    const {
      isAjaxReq,
      gameWeek,
      error,
      errorMessage,
      clearError,
      wallet
    } = this.props;
    const { requestedChallenger } = this.state;

    const validator = errors.minWithdrawalAmount(wallet);

    return (
      <div className="h2h-container">
        <Header title="Create Challenge" />
        <section className="ch2h-container">
          <div className="form-title-container">
            <Text classText="ch2h-form-title" weight="med2">
              Challenge Information
            </Text>
          </div>
          <div className="form-controller">
            <Button
              className="ch2h-btn"
              onClick={this.showModal}
              disabled={!isEmpty(requestedChallenger)}
            >
              <Text color="black">Invite Friend</Text>
            </Button>
            <RadioGroup onChange={this.onChange} value={this.state.type}>
              <Radio className="ch2h-radio" value="public">
                <span className="ch2h-text-container">
                  <Text classText="radio-label">Or Keep Invitation Open</Text>
                  <p className="radio-subtitle">
                    When an invitation is open, H2H will be made public, anyone
                    can accept request.
                  </p>
                </span>
              </Radio>
            </RadioGroup>
          </div>
          {error && (
            <div className="h2h-err-container">
              <Alert
                description={errorMessage}
                type="error"
                closable
                onClose={() => clearError()}
              />
            </div>
          )}
          <CreateForm
            challenger={requestedChallenger}
            currentWeek={gameWeek}
            loading={isAjaxReq}
            onSubmit={this.createChallenge}
            minAmountValidator={validator}
            minValue={wallet}
            callback={this.goToWallet}
          />
        </section>
        <section>{this.renderInviteUser()}</section>
      </div>
    );
  }
}

export default withRouter(CreateH2H);
