import React from 'react';
import { withRouter } from 'react-router';
import { Navbar } from '../index';
import { HOME, LOGIN, SIGNUP } from 'utils/constants';
import './Auth.scss';

const AuthWrapper = props => {
  const { pathname } = props.location;
  let routeClass;
  let wrapperClass;
  let showLogo;

  switch (pathname) {
    case HOME:
      routeClass = 'home-route';
      wrapperClass = 'home-wrapper';
      showLogo = true;
      break;
    case LOGIN:
      routeClass = 'login-route';
      wrapperClass = 'login-wrapper';
      showLogo = false;
      break;
    case SIGNUP:
      routeClass = 'signup-route';
      wrapperClass = 'signup-wrapper';
      showLogo = false;
      break;
    default:
      routeClass = '';
      wrapperClass = '';
      showLogo = true;
      break;
  }

  return (
    <div className={`auth-page-wrapper ${wrapperClass}`}>
      <Navbar
        showLogo={showLogo}
        isAuthenticated={props.isAuthenticated}
        user={props.user}
      />
      <div className={`auth-wrapper ${routeClass}`}>{props.children}</div>
    </div>
  );
};

export const Auth = withRouter(AuthWrapper);
