import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Layout } from 'antd';
import { AppError, ErrorBoundary } from 'components';
import { Auth, Navbar } from '../index';
import { loadUser, logoutUser } from 'modules/auth/store/actions';
import { getPromoLeagues } from 'modules/global/store/actions';
import { loadBanks } from 'modules/wallet/store/actions';

const { Content } = Layout;
class AppContainer extends Component {
  componentWillMount() {
    const { loadUser, userLoaded, isAuthenticated } = this.props;
    if (isAuthenticated && !userLoaded) {
      loadUser();
    }
  }

  componentDidMount() {
    // Do something when user is authenticated
    this.props.loadBanks();
    this.props.getPromoLeagues();
  }

  renderContent() {
    const { children, isAuthenticated, user } = this.props;
    return (
      <div>
        <Navbar
          isAuthenticated={isAuthenticated}
          user={user}
          handleLogOut={this.props.logoutUser}
        />
        <Content>{children}</Content>
      </div>
    );
  }

  renderAuthContent() {
    return <Auth {...this.props} />;
  }

  render() {
    const { isAuthenticated } = this.props;

    return (
      <ErrorBoundary fallbackComponent={AppError}>
        {!isAuthenticated ? this.renderAuthContent() : this.renderContent()}
      </ErrorBoundary>
    );
  }
}

const mapStateToProps = state => ({
  userLoading: state.auth.loading,
  userLoaded: state.auth.loaded,
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user
});

export const App = compose(
  withRouter,
  connect(
    mapStateToProps,
    { loadUser, logoutUser, loadBanks, getPromoLeagues }
  )
)(AppContainer);
