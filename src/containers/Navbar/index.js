import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { HashLink as Link } from 'HOCS/LinkComponent';
import { Logo, Text } from 'components';
import { Avatar, Icon, Dropdown, Menu, Modal } from 'antd';
import MenuDropdown from './menuDropdown';
import { getGravatar } from 'utils/gravatar';
import './Navbar.scss';

const confirm = Modal.confirm;
const modalStyle = {
  top: 40
};
const displayName = 'Navbar';

const propTypes = {
  type: PropTypes.string,
  avatarUrl: PropTypes.string,
  handleLogOut: PropTypes.func
};

const unAuthenticatedNavList = [
  { display: 'How to play', url: '/#how-to-play' },
  { display: 'Support', url: '/contact-us' }
];

const authenticatedNavList = [
  {
    display: 'Leagues',
    url: '/leagues/hot-leagues',
    icon: (
      <Icon
        type="fire"
        theme="outlined"
        style={{ color: 'red', marginRight: 5 }}
      />
    )
  },
  // { display: 'Leagues', url: '/leagues' },
  // { display: 'H2H', url: '/h2h' },
  { display: 'Wallet', url: '/wallet' },
  // { display: 'Notification', url: '/notification' },
  { display: 'Help', url: '/contact-us' }
];
const dropDownMenuItems = [
  { display: 'Profile', url: '/profile' },
  { display: 'Logout', url: '#' }
];

class NavbarComponent extends Component {
  state = {
    showDropdown: false,
    isMobile: false
  };
  componentWillMount = () => {
    window.addEventListener('resize', this.handleWindowSizeChange);
  };

  componentDidMount() {
    this.handleWindowSizeChange();
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  };

  handleWindowSizeChange = () => {
    const width = window.innerWidth;
    this.setState({ isMobile: width <= 700 });
  };

  togglehamburger = () => {
    this.setState({ showDropdown: !this.state.showDropdown });
  };

  logoClick = () => {
    const { history } = this.props;
    history.push('/');
  };

  menu = () => (
    <Menu>
      <Menu.Item key="0" className="dropmenu-item">
        <a href="/profile">Profile</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item
        key="2"
        className="dropmenu-item danger"
        onClick={this.handleLogOut}
      >
        Logout
      </Menu.Item>
    </Menu>
  );

  handleLogOut = () => {
    const self = this;
    confirm({
      title: `Do you want to logout?`,
      style: { modalStyle },
      confirmLoading: true,
      onOk() {
        self.props.handleLogOut();
      },
      okText: 'Logout',
      onCancel() {}
    });
  };

  renderAuthNavbar = () => {
    const {
      location: { pathname },
      isAuthenticated
    } = this.props;

    const canAuthenticateRoutes = ['/contact-us', '/'].includes(pathname);
    return (
      <div className="navbar-main-container">
        <div className="nav">
          <div className="nav-header">
            <div className="nav-title" onClick={this.logoClick}>
              <Logo />
            </div>
          </div>
          <div className="nav-links">
            {canAuthenticateRoutes &&
              !isAuthenticated && (
                <Link
                  className={`${
                    pathname.startsWith('/signup') ? 'auth-active' : ''
                  } link-item desktop-item`}
                  key="link-item-1"
                  to="/signup"
                >
                  Sign up
                </Link>
              )}
            {canAuthenticateRoutes &&
              !isAuthenticated && (
                <Link
                  className={`${
                    pathname.startsWith('/login') ? 'auth-active' : ''
                  } link-item desktop-item`}
                  key="link-item-2"
                  to="/login"
                >
                  Login
                </Link>
              )}
            <div onClick={this.togglehamburger} className="pull-right">
              <Icon
                className="hamburger"
                type={!this.state.showDropdown ? 'menu-fold' : 'close'}
              />
            </div>
          </div>
          {this.state.showDropdown && (
            <MenuDropdown
              closeHamburger={this.togglehamburger}
              linkList={unAuthenticatedNavList}
              history={this.props.history}
            />
          )}
        </div>
      </div>
    );
  };

  renderMainNavbar = () => {
    const {
      user = {},
      location: { pathname }
    } = this.props;
    const email = user ? user.email : '';

    const linkItem = (item, index) => {
      return (
        <Link
          className={`link-item ${
            pathname.startsWith(item.url) ? 'active' : ''
          }`}
          key={`link-item-${index}`}
          to={item.url}
        >
          {item.icon && item.icon}
          <Text>{item.display}</Text>
        </Link>
      );
    };

    return (
      <div className="navbar-main-container">
        <div className="nav nav-contrast">
          <div className="nav-header">
            <div className="nav-title" onClick={this.logoClick}>
              <Logo dark className="logo" />
            </div>
          </div>
          {this.state.isMobile ? (
            <div className="nav-links">
              <div onClick={this.togglehamburger}>
                <Icon
                  className="hamburger dark-ham"
                  type={!this.state.showDropdown ? 'menu-fold' : 'close'}
                />
              </div>
            </div>
          ) : (
            <div className="nav-links">
              {authenticatedNavList.map((item, index) => {
                return linkItem(item, index);
              })}
              <Avatar
                className="avatar"
                size="large"
                src={getGravatar(email)}
              />

              <Dropdown
                overlay={this.menu()}
                trigger={['click']}
                placement="bottomRight"
              >
                <a className="ant-dropdown-link">
                  <Icon type="caret-down" />
                </a>
              </Dropdown>
            </div>
          )}
          {this.state.showDropdown &&
            this.state.isMobile && (
              <MenuDropdown
                linkList={authenticatedNavList.concat(dropDownMenuItems)}
                handleLogOut={this.handleLogOut}
                history={this.props.history}
                closeHamburger={this.togglehamburger}
              />
            )}
        </div>
      </div>
    );
  };
  render() {
    return (
      <div>
        {this.props.isAuthenticated
          ? this.renderMainNavbar()
          : this.renderAuthNavbar()}
      </div>
    );
  }
}

NavbarComponent.displayName = displayName;
NavbarComponent.propTypes = propTypes;
NavbarComponent.defaultProps = {
  isAuthenticated: false,
  avatarUrl: '',
  handleLogOut: () => {},
  showLogo: true
};

export const Navbar = withRouter(NavbarComponent);
