import React from 'react';
import PropTypes from 'prop-types';
import { HashLink as Link } from 'HOCS/LinkComponent';

import './Navbar.scss';

const MenuDropdown = ({ linkList, handleLogOut, history, closeHamburger }) => {
  const goToRoute = (e, path) => {
    e.preventDefault();
    if (closeHamburger) closeHamburger();

    return history.push(path);
  };

  const linkItem = (item, index) => {
    if (item.display === 'Logout') {
      return (
        <a key={`item-${index}`} onClick={handleLogOut}>
          {item.display}
        </a>
      );
    }
    return (
      <Link
        smooth
        key={`item-${index}`}
        to={item.url}
        onClick={e => goToRoute(e, item.url)}
      >
        {item.display}
      </Link>
    );
  };
  return (
    <div className="menuItem">
      <div className="menu-item-dropdown">
        {linkList.map((item, index) => {
          return linkItem(item, index);
        })}
      </div>
    </div>
  );
};

MenuDropdown.propTypes = {
  linkList: PropTypes.array,
  handleLogOut: PropTypes.func
};

MenuDropdown.defaultProps = {
  linkList: []
};

export default MenuDropdown;
